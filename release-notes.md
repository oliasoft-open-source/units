# Units Release Notes

# 4.3.1

- Fixed publishing pipelines

# 4.3.0

- Updated dependencies([OW-20243](https://oliasoft.atlassian.net/browse/OW-20243))

# 4.2.5

- Added new unit `psi/C`(Psi per Celsius) to the pressure per temperature category ([OW-20060](https://oliasoft.atlassian.net/browse/OW-20060))

# 4.2.4

- revert handling of `+` in `cleanNumStr()` ([OW-19695](https://oliasoft.atlassian.net/browse/OW-19695)) since it interferes
with typing values in inputs
- keep better handling of `+` in `unum()` and conversion functions

# 4.2.3

- improve handling of `+` in `unum()` and `cleanNumStr()` ([OW-19695](https://oliasoft.atlassian.net/browse/OW-19695))

# 4.2.2

Improve numerical parsing of strings containing `/` slash characters

- Fixes `isDeepCloseTo` to correctly return true for strings containing `/` ([OW-19280](https://oliasoft.atlassian.net/browse/OW-19280))
- Fixes `toNum()`, `isValidNum()`, `isNumeric()`, and `isNonNumerical()` to now handle strings containing  `/` correctly
- Simplifies `isNonNumerical()` so it just returns `!isNumeric()`
- Now `isNonNumerical('')` returns `false` (previously returned `true`)
- Exports new `isFraction()` function
- `toNum('1/0')` now returns `Infinity` not `1/0` (more correct)
- Adds more edge-case tests for weird situations like `1 1/2` and `1/2e+4`
- Whitespace trimming now handles extra spaces in the middle of strings
- Slightly stricter Regex for detecting scientific notation strings

# 4.2.1

- Added more unit conversion for distance per time ([OW-15577](https://oliasoft.atlassian.net/browse/OW-15577))

# 4.2.0

- Upgrade base image to Node.js v22([OW-18528](https://oliasoft.atlassian.net/browse/OW-18528))

# 4.1.0

- Added more precision for conversions ([OW-17705](https://oliasoft.atlassian.net/browse/OW-17705))

# 4.0.0

- Compile schema to schema validator function at build time.
- Helps to resolve CSP security issues (unsafe-eval)

# 3.15.0

- New `convertAndGetValueStrict()` function (preserves empty values and types) (([OW-18285](https://oliasoft.atlassian.net/browse/OW-18285))

# 3.14.0

- New `toString` function (converts numbers to strings without coalescing to scientific notation)

# 3.13.0

- New `stripLeadingZeros()` function

# 3.12.4

- Fix bug where roundToPrecision('0.0000002') previously returned scientific notation ([OW-17645](https://oliasoft.atlassian.net/browse/OW-17645))

# 3.12.3

- Added "lbf/100ft2" to pressure

# 3.12.2

- Add validators in CI on version and corresponding release-notes entry ([MR !124](https://gitlab.com/oliasoft-open-source/units/-/merge_requests/124))

# 3.12.1

- Upgrade devDependencies => remove all alerts from `yarn audit` ([OW-16370](https://oliasoft.atlassian.net/browse/OW-16370) / [MR !124](https://gitlab.com/oliasoft-open-source/units/-/merge_requests/124))

# 3.12.0

- Added new `isScientificStringNum()` function

# 3.11.10

- Added missed descriptions([OW-16777](https://oliasoft.atlassian.net/browse/OW-16777))

# 3.11.9

- Add better unit tests asserting return types for numeric functions

# 3.11.8

- Add support for missing ftUS (us Feet) location unit (WELLDESIGN-3ZR Sentry). Added ftUS as alias to usft

# 3.11.7

- Add support for missing ftSe (British foot (Sears 1922)) location unit ([OW-15238](https://oliasoft.atlassian.net/browse/OW-15238))

# 3.11.6

- Add more conversions for weight([OW-15190](https://oliasoft.atlassian.net/browse/OW-15190))

# 3.11.5

- Fixed some numerical (roundings) and dislay functions so that they can handle bad inputs like undefined ([OW-15047](https://oliasoft.atlassian.net/browse/OW-15047))

# 3.11.4

- Added conversions and units for entalphy ([OW-14844](https://oliasoft.atlassian.net/browse/OW-14844))

# 3.11.3

- Added conversions for mixing requirements ([OW-14894](https://oliasoft.atlassian.net/browse/OW-14894))

# 3.11.2

- Fix mattermost url and changed channel name to Release bots [OW-14780](https://oliasoft.atlassian.net/browse/OW-14780)

# 3.11.1

- Added `cm` unit to `length` quantity ([OW-14463](https://oliasoft.atlassian.net/browse/OW-14463))

# 3.11.0

- Added new `time/stand` unit ([OW-13880](https://oliasoft.atlassian.net/browse/OW-13880))

# 3.10.3

- updated method `withUnit()` to not always try to split the value provided in argument

# 3.10.2

- improved logic of the withUnit function to handle undefined and other special cases correctly

# 3.10.1

- Allowed continue pipelines when betta fails([OW-13420](https://oliasoft.atlassian.net/browse/OW-13420))

# 3.10.0

- Improved support for precision(trailing decimal zeros) when rounding and displaying numbers ([OW-12964](https://oliasoft.atlassian.net/browse/OW-12964))
  - New functions which preserve trailing decimal zeros for string inputs
    - `roundByMagnitudeToFixed()`
    - `displayNumberToFixed()`

#  3.9.0

- Added Beta release label and added rules to pipelines to run beta release when added

## 3.8.0

- Introduce Location (GIS) unit and units conversions

Included units: 

- Link
- Clark`s foot
- Clark`s link
- British yard (Sears 1922)
- British chain (Sears 1922)
- British chain (Sears 1922 Truncated)
- Gold Coast foot
- Indian yard

## 3.7.2

- improve handling of `+/-Infinity` and `NaN` in `convertAndGetValue()` / `unum()`, `numFraction()`, `toNum()`, and
`isValidNum()` [OW-12543](https://oliasoft.atlassian.net/browse/OW-12543)

## 3.7.1

- patch `roundToFixed` (has to return string type since number type can't represent trailing zeros)

## 3.7.0

- add new rounding functions `roundToFixed` and `roundByRange` ([OW-12468](https://oliasoft.atlassian.net/browse/OW-12468))

## 3.6.0

- optionally support e-notation in `displayNumber` ([OW-12471](https://oliasoft.atlassian.net/browse/OW-12471))

## 3.5.1

- added .npmignore file([OW-12381](https://oliasoft.atlassian.net/browse/OW-12381))

## 3.5.0

- improvements to rounding functions, new `roundToDecimalPrecision` function ([OW-12423](https://oliasoft.atlassian.net/browse/OW-12423))

## 3.4.2

- bugfix a locale mistake in `displayNumber` and `roundNumberWithUnit` ([OW-12428](https://oliasoft.atlassian.net/browse/OW-12428))

## 3.4.1

- bugfix and simplify `roundByMagnitude` to ensure it never rounds the integer part ([OW-12391](https://oliasoft.atlassian.net/browse/OW-12391))

## 3.4.0

- improve `displayNumber()` to automatically decide between decimal and scientific formats by default ([OW-12287](https://oliasoft.atlassian.net/browse/OW-12287))
- modifies `roundToPrecision()` so it includes the integer part by default (still configurable)
- removes export of unused internationalization functions

## 3.3.0

- add new rounding functions `roundToPrecision()`, and `roundByMagnitude()` ([OW-11985](https://oliasoft.atlassian.net/browse/OW-11985))
- deprecate `getNumberOfDigitsToShow()`

## 3.2.0

- Support unit strings as inputs to `toNum()` and `isValidNum()` ([OW-11987](https://oliasoft.atlassian.net/browse/OW-11987))

## 3.1.2

- bugfix: Bad conversion logic between `m3/m` and `m3/s`

## 3.1.1

- bugfix `displayNumber` option `roundScientificCoefficientToNDecimals`


## 3.1.0

- improve `displayNumber` with new options `nonBreakingSpace` and `roundScientificCoefficientToNDecimals`

## 3.0.1

- refactor `roundNumberWithLabel` so it reuses standard `round` and `displayNumber` logic internally ([OW-12055](https://oliasoft.atlassian.net/browse/OW-12055))

## 3.0.0

- add new `displayNumber` function for text labels with thousand separators, scientific notation ([OW-11986](https://oliasoft.atlassian.net/browse/OW-11986))
- deprecate `formatNumber` which used wrong thousand separators (small breaking change to display formats)
- improve `cleanNumStr` and `toNum` to strip leading and trailing spaces, tabs, and newlines

## 2.6.2

- Package cleanup to fix ([OW-11780](https://oliasoft.atlassian.net/browse/OW-11780))
- upgrade and move AJV to `peerDependencies` (to avoid duplicate installs in parent apps)
- removed dead babel package
- moved ESLint config package to `devDependencies`

## v2.6.1

- Improve `isCloseTo` handling with absolute diffs ([OW-11765](https://oliasoft.atlassian.net/browse/OW-11765))

## v2.6.0

- Extend `isCloseTo` function ([OW-11742](https://oliasoft.atlassian.net/browse/OW-11742))

## v2.5.0

- Add `isDeepCloseTo` function ([OW-11689](https://oliasoft.atlassian.net/browse/OW-11689))

## v2.4.1

- Fix `valudateNumber` method and add more test cases.

## v2.4.0

- Add new two unit groups (volumeGradient, shearStress) ([OW-11629](https://oliasoft.atlassian.net/jira/software/c/projects/OW/issues/OW-11629))

## v2.3.2

- changed unum to return only number values

## v2.3.1

- Add `validateNumber` method to index.ts and export it.
## v2.3.0

- add `validateNumber()` function and implement Ajv schema validator.
([OW-11576](https://oliasoft.atlassian.net/browse/OW-11576))

## v2.2.1

- Release pipeline fix

## v2.2.0

- Add `isCloseTo()` `isCloseToOrGreaterThan()` `isCloseToOrLessThan()` functions to compare floating-point numbers.
([OW-10737](https://oliasoft.atlassian.net/jira/software/c/projects/OW/issues/OW-10737))

## v2.1.0

- move `isValidNum()` function from WellDesign (complements `toNum()` but for validation)
([OW-11064](https://oliasoft.atlassian.net/browse/OW-11064))

## v2.0.2

- fix and move internationalization functions from welldesign repo ([OW-10740](https://oliasoft.atlassian.net/browse/OW-10740))

## v2.0.1

- fix/improve type definitions for `round()` function ([OW-10966](https://oliasoft.atlassian.net/browse/OW-10966))

## v2.0.0

- new universal `round()` function implementation ([OW-10738](https://oliasoft.atlassian.net/browse/OW-10738))
  - new implementation preserves types and handles unit numbers
  - fixes rounding implementation to not rely on `toFixed`
- removes old `roundNumber()` implementation (breaking change, use `round()` instead)

## v1.7.2

- Added new unit `kNm` (kilo Newton meter) to the torque unit category

## v1.7.1

- Switch repo from `npm` to `yarn` (for consistency with our other open packages)
- Upgrades `fraction.js` to latest version

## v1.6.1

- Fix yarn/npm command mismatch in `gitlab-ci.yml` (this project still uses npm)

## v1.6.0

- Add optional CI pipeline that automates publishing of beta releases ([OW-10003](https://oliasoft.atlassian.net/browse/OW-10003))

## v1.5.1

- fixed incorrect unitkeys

## v1.5.0

1. added new unit category/quantity `Pressure per temperature`
2. added 3 new units for the Pressure per temperature category

- `Pa/C` - Pascal per celsius
- `Bar/C` - Bar per celsius
- `psi/F` - Psi per fahrenheit

3. Added conversions for the new units

## v1.4.3

- exposed `isNonNumerical` method in repository

## v1.4.0

1. added new unit category/quantity `Turbulent Skin`
2. added 2 new units for the Turbulent Skin category

- `1/m3/d` - inverse cubic meters per day
- `1/MMSCFD` - Inverse Million Standard Cubic Feet per day

3. Added conversions for the new units

## v1.3.0

- first publish of release notes
