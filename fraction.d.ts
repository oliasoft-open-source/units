declare module 'fraction.js' {
  export default class Fraction {
    constructor(numerator: number, denominator?: number);
    constructor(decimal: number);
    constructor(string: string);
    constructor(fraction: Fraction);

    s: number;
    n: number;
    d: number;

    abs(): Fraction;
    neg(): Fraction;
    add(fraction: Fraction | number | string): Fraction;
    sub(fraction: Fraction | number | string): Fraction;
    mul(fraction: Fraction | number | string): Fraction;
    div(fraction: Fraction | number | string): Fraction;
    pow(exponent: number): Fraction;
    inverse(): Fraction;
    simplify(eps?: number): Fraction;
    equals(fraction: Fraction | number | string): boolean;
    compare(fraction: Fraction | number | string): number;
    divisible(fraction: Fraction | number | string): boolean;
    valueOf(): number;
    toString(): string;
    toLatex(excludeWhole?: boolean): string;
    toFraction(excludeWhole?: boolean): string;
    toContinued(): number[];
    clone(): Fraction;

    static gcd(a: number, b: number): number;
    static lcm(a: number, b: number): number;
  }
}
