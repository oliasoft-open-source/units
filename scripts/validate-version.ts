import { spawnSync} from 'child_process';
import { readPackageJson } from './ts-script-utils/read-package-json';

const checkYarnInfoSync = (packageName: string) => {
  const result = spawnSync('yarn', [
    'info',
    packageName,
    'versions',
    '--json',
  ], {encoding: 'utf-8'});
  if (result.stderr) {
    console.error(result.stderr);
    throw new Error(`Something went wrong when trying to run 'yarn info ${packageName} versions --json'`)
  }
  return JSON.parse(result.stdout.toString().trim());
};

const hasCurrentVersion = (
  yarnInfo: { data: Array<string> } | any,
  current: string,
) => {
  if (!yarnInfo.data?.length) {
    console.warn('Yarn info did not have data property', yarnInfo);
    return false;
  }
  // TODO: consider checking for semantic mambojambo like ^ or ~ and stuff like that
  // For now we only check for strict equality:
  return yarnInfo.data.some((v: string) => v === current);
};

const main = async () => {
  const packageJson = await readPackageJson();
  const { name, version } = packageJson;

  const yarnInfoOutput = checkYarnInfoSync(name);

  if (hasCurrentVersion(yarnInfoOutput, version)) {
    console.error(
      `Package ${name}@${version} already exists! Please bump version in package.json before merge.`,
    );
    process.exit(1);
  } else {
    console.log(
      `Package ${name}@${version} does not already exist`,
    );
    process.exit(0);
  }
};

main();

