import {readFile} from 'fs/promises';
import {resolve} from 'path';

export type PackageJson = {
  name: string;
  version: string;
  devDependencies: { [k: string]: string};
  dependencies: { [k: string]: string};
}

export const readPackageJson = async (folder: string = './') => {
  const filePath = resolve(folder, 'package.json');
  const packageJson = await readFile(filePath, 'utf-8');
  return JSON.parse(packageJson) as PackageJson;
};

export const getVersion = async () => {
  const p = await readPackageJson();
  return p.version;
};
