/* eslint-disable import/no-extraneous-dependencies */
import { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';
import Ajv from 'ajv';
import path from 'node:path';
import fs from 'node:fs';
import standaloneCode from 'ajv/dist/standalone';
import ajvErrors from 'ajv-errors';
import ajvKeywords from 'ajv-keywords';
import * as schemas from '../src/validate/schemas';

// @ts-ignore
const __dirname = dirname(fileURLToPath(import.meta.url));

const ajv = new Ajv({
  allErrors: true,
  $data: true,
  code: {
    source: true,
    esm: true
  }
});

ajvErrors(ajv);
ajvKeywords(ajv, 'transform');

for (const [key, schema] of Object.entries(schemas)) {
  if (schema && typeof schema === 'object') {
    ajv.addSchema(schema, `${key}Validator`);
  }
}

let moduleCode = standaloneCode(ajv);

moduleCode = moduleCode.replace(
  `"use strict";`,
  `// @ts-nocheck\n"use strict";\n
export interface ValidatorWithErrors {
  (data: any, options?: { instancePath?: string, parentData?: any, parentDataProperty?: any, rootData?: any }): boolean;
  errors?: any;
};`
);

moduleCode = moduleCode.replace(
  /export const (\w+) = (validate\d+);/g,
  `export const $1 = $2 as ValidatorWithErrors;`
);

moduleCode = moduleCode.replace(
  /(function validate\d+\(.*?\) \{)/g,
  `$1\n  const validate = this as ValidatorWithErrors;\n  validate.errors = null;`
);

moduleCode = moduleCode.replace(
  /(validate\.errors = vErrors;)/g,
  `(validate as ValidatorWithErrors).errors = vErrors;`
);

moduleCode = moduleCode.replace(
  /const func2 = import\("ajv\/dist\/runtime\/ucs2length"\)\.default;/g,
  `import ucs2length from "ajv/dist/runtime/ucs2length";`
);

const outputFilePath = path.join(__dirname, '../src/validate/ajv-validators.ts');
fs.writeFileSync(outputFilePath, moduleCode);
console.log(`Generated validators: ${outputFilePath}`);