import { readFile } from 'fs/promises';
import { resolve } from 'path';

import {
  readPackageJson,
} from './ts-script-utils/read-package-json';

const readReleaseNotes = async (folder: string = './') => {
  const filePath = resolve(folder, 'release-notes.md');
  const releaseNotes = await readFile(filePath, 'utf-8');
  return releaseNotes;
};

const main = async () => {
  const packageJson = await readPackageJson();
  const { name, version } = packageJson;

  // Make sure this exists in the release notes as well:
  const releaseNotes = await readReleaseNotes();
  if (releaseNotes.includes(`# ${version}`)) {
    console.debug(
      `Release notes for '${name}' seems to include a description of version '${version}'. OK.`,
    );
  } else {
    console.error(
      `Release notes for '${name}' does not describe version '${version}' properly. Please add/fix.`,
    );
    throw new Error(
      'Release notes seems to be missing description on current version!',
    );
  }
};

main()
  .then(() => {
    console.info('OK');
    process.exit(0);
  })
  .catch((err: Error) => {
    console.error(err);
    process.exit(1);
  });
