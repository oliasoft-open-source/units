import {
  formatDecimalDisplayNumber,
  formatScientificDisplayNumber,
  displayNumber,
  displayNumberToFixed
} from './display-number.ts';
import { roundToPrecision } from '../rounding/rounding.ts';

describe('Format human-friendly numbers for display labels', () => {
  describe('formatDecimalDisplayNumber', () => {
    it('should convert numbers to human-friendly decimal display values', () => {
      expect(formatDecimalDisplayNumber(1234567)).toBe('1 234 567');
      expect(formatDecimalDisplayNumber('1234567')).toBe('1 234 567');
      expect(formatDecimalDisplayNumber(123456.789)).toBe('123 456.789');
      expect(formatDecimalDisplayNumber('123456.789')).toBe('123 456.789');
      expect(formatDecimalDisplayNumber(123e7)).toBe('1 230 000 000');
      expect(formatDecimalDisplayNumber(123e-7)).toBe('0.0000123');
      expect(formatDecimalDisplayNumber('123e7')).toBe('1 230 000 000');
      expect(formatDecimalDisplayNumber('1/2')).toBe('0.5');
      expect(formatDecimalDisplayNumber(Infinity)).toBe('∞');
      expect(formatDecimalDisplayNumber(-Infinity)).toBe('-∞');
      expect(formatDecimalDisplayNumber(NaN)).toBe('NaN');
      expect(formatDecimalDisplayNumber(null)).toBe('');
      expect(formatDecimalDisplayNumber(undefined)).toBe('');
      expect(formatDecimalDisplayNumber('1234.')).toBe('1234.');
      expect(formatDecimalDisplayNumber('1234,')).toBe('1234,');
      expect(formatDecimalDisplayNumber('123,456,789,')).toBe('123,456,789,');
      expect(formatDecimalDisplayNumber('123.456,789.')).toBe('123.456,789.');
      expect(formatDecimalDisplayNumber('')).toBe('');
      expect(formatDecimalDisplayNumber('    1\n23.4 5  6\t789')).toBe(
        '123.456789'
      );
      expect(formatDecimalDisplayNumber('        ')).toBe('');
      expect(formatDecimalDisplayNumber('\t')).toBe('');
      expect(formatDecimalDisplayNumber('\n')).toBe('');
      expect(formatDecimalDisplayNumber('badFooBar')).toBe('badFooBar');
    });
    it('should not display very small and large numbers in scientific notation', () => {
      expect(formatDecimalDisplayNumber(1234567123.12345)).toBe('1 234 567 123.12345');
      //eventually floating-point cannot store more precision (we are not rounding here, limitation of floating-point):
      expect(formatDecimalDisplayNumber(123456712345678.12345)).toBe('123 456 712 345 678.12');
      expect(formatDecimalDisplayNumber(1.23456e17)).toBe('123 456 000 000 000 000');
      expect(formatDecimalDisplayNumber(1_234_560_000_000_000_000_000)).toBe('1 234 560 000 000 000 000 000');
      expect(formatDecimalDisplayNumber(1.23456e21)).toBe('1 234 560 000 000 000 000 000');
      expect(formatDecimalDisplayNumber(0.0000000000012345)).toBe('0.0000000000012345');
      expect(formatDecimalDisplayNumber(1.2345e-12)).toBe('0.0000000000012345');
      expect(formatDecimalDisplayNumber(1.2345e-20)).toBe('0.00000000000000000001');
      expect(formatDecimalDisplayNumber(1.2345e-30)).toBe('0');
    });
    it('should gracefully handle localized input formats (even though we do not expect these in the data layer)', () => {
      expect(formatDecimalDisplayNumber('123,456.789')).toBe('123 456.789');
      expect(formatDecimalDisplayNumber('123,456,789.123')).toBe(
        '123 456 789.123'
      );
      expect(formatDecimalDisplayNumber('123456789,123')).toBe(
        '123 456 789.123'
      );
      expect(formatDecimalDisplayNumber('123.456.789,123')).toBe(
        '123 456 789.123'
      );
      expect(formatDecimalDisplayNumber('123 456 789.123')).toBe(
        '123 456 789.123'
      );
    });
    it('can be configured to use non-breaking spaces', () => {
      expect(
        formatDecimalDisplayNumber('123,456.789', { nonBreakingSpace: true })
      ).toBe('123 456.789');
    });
    it('can be configured to not use any separator spaces', () => {
      expect(
        formatDecimalDisplayNumber('123,456.789', { noThousandsSeparator: true })
      ).toBe('123456.789');
    });
    it('can be configured to preserve trailing zeros (for fixed precision string inputs)', () => {
      expect(formatDecimalDisplayNumber('0', {preserveTrailingZeros: true})).toBe('0');
      expect(formatDecimalDisplayNumber('1234.123', {preserveTrailingZeros: true})).toBe('1 234.123');
      expect(formatDecimalDisplayNumber('1234.1000', {preserveTrailingZeros: true})).toBe('1 234.1000');
      expect(formatDecimalDisplayNumber('1234.1100', {preserveTrailingZeros: true})).toBe('1 234.1100');
      expect(formatDecimalDisplayNumber('123000.0', {preserveTrailingZeros: true})).toBe('123 000.0');
      expect(formatDecimalDisplayNumber('0.000000001200', {preserveTrailingZeros: true})).toBe('0.000000001200');
      expect(formatDecimalDisplayNumber('123000000', {preserveTrailingZeros: true})).toBe('123 000 000');
      expect(formatDecimalDisplayNumber('123000000.0', {preserveTrailingZeros: true})).toBe('123 000 000.0');
    });
  });
  describe('formatScientificDisplayNumber', () => {
    it('should format scientific notation display numbers', () => {
      expect(formatScientificDisplayNumber(1000.5)).toBe('1.0005·10³');
      expect(formatScientificDisplayNumber('1000.5')).toBe('1.0005·10³');
      expect(formatScientificDisplayNumber(0.000002345)).toBe('2.345·10⁻⁶');
      expect(formatScientificDisplayNumber('0.000002345')).toBe('2.345·10⁻⁶');
      expect(formatScientificDisplayNumber('0.000000000000000235')).toBe(
        '2.35·10⁻¹⁶'
      );
      expect(
        formatScientificDisplayNumber('8900000000000000000000000000')
      ).toBe('8.9·10²⁷');
      expect(formatScientificDisplayNumber('1/1000')).toBe('1·10⁻³');
      expect(formatScientificDisplayNumber(2.34e4)).toBe('2.34·10⁴');
      expect(formatScientificDisplayNumber('2.34e4')).toBe('2.34·10⁴');
      expect(formatScientificDisplayNumber(Infinity)).toBe('Infinity');
      expect(formatScientificDisplayNumber(-Infinity)).toBe('-Infinity');
      expect(formatScientificDisplayNumber(NaN)).toBe('Invalid');
      expect(formatScientificDisplayNumber('')).toBe('');
      expect(formatScientificDisplayNumber('1234.')).toBe('1234.');
      expect(formatScientificDisplayNumber('1234,')).toBe('1234,');
      expect(formatScientificDisplayNumber('123,456,789,')).toBe(
        '123,456,789,'
      );
      expect(formatScientificDisplayNumber('123.456,789.')).toBe(
        '123.456,789.'
      );
      expect(formatScientificDisplayNumber('    1\n23.4 5  6\t789')).toBe(
        '1.23456789·10²'
      );
      expect(formatScientificDisplayNumber('        ')).toBe('');
      expect(formatScientificDisplayNumber('\t')).toBe('');
      expect(formatScientificDisplayNumber('\n')).toBe('');
      expect(formatScientificDisplayNumber('badFooBar')).toBe('badFooBar');
      expect(formatScientificDisplayNumber('0', {preserveTrailingZeros: true})).toBe('0');
      expect(formatScientificDisplayNumber('123000.0', {preserveTrailingZeros: true})).toBe('1.23·10⁵');
      expect(formatScientificDisplayNumber('0.000000001200', {preserveTrailingZeros: true})).toBe('1.2·10⁻⁹');
      expect(formatScientificDisplayNumber('123000000', {preserveTrailingZeros: true})).toBe('1.23·10⁸');
    });
    it('should not display 10⁰', () => {
      expect(formatScientificDisplayNumber(0)).toBe('0');
      expect(formatScientificDisplayNumber(9)).toBe('9');
      expect(formatScientificDisplayNumber(9e0)).toBe('9');
      expect(formatScientificDisplayNumber(9.0e-0)).toBe('9');
    });
    it('it should optionally support e-notation', () => {
      expect(formatScientificDisplayNumber(1000.5, {eNotation: true})).toBe('1.0005e3');
      expect(formatScientificDisplayNumber(2.34e-4, {eNotation: true})).toBe('2.34e-4');
    });
    it('should gracefully handle localized input formats (even though we do not expect these in the data layer)', () => {
      expect(formatScientificDisplayNumber('123,000.0')).toBe('1.23·10⁵');
      expect(formatScientificDisplayNumber('123,400,000.000')).toBe(
        '1.234·10⁸'
      );
      expect(formatScientificDisplayNumber('123400000.000')).toBe('1.234·10⁸');
      expect(formatScientificDisplayNumber('123.400.000,000')).toBe(
        '1.234·10⁸'
      );
      expect(formatScientificDisplayNumber('123 400 000.000')).toBe(
        '1.234·10⁸'
      );
    });
    it('can be configured to round the coefficient', () => {
      expect(formatScientificDisplayNumber('123,456,891.123')).toBe(
        '1.23456891123·10⁸'
      );
      expect(
        formatScientificDisplayNumber('123,456,891.123', {
          roundScientificCoefficient: 2,
        })
      ).toBe('1.23·10⁸');
      expect(
        formatScientificDisplayNumber('123,456,891.123', {
          roundScientificCoefficient: 6,
        })
      ).toBe('1.234569·10⁸');
      expect(
        formatScientificDisplayNumber('123,456,891.123', {
          roundScientificCoefficient: 0,
        })
      ).toBe('1·10⁸');
    });
    it('should handle null or undefined value', () => {
      expect(formatScientificDisplayNumber(null)).toBe('');
      expect(formatScientificDisplayNumber(undefined)).toBe('');
    });
  });
  describe('displayNumber', () => {
    it('should display numbers for human-friendly labels', () => {
      expect(displayNumber(1234.56)).toBe('1 234.56');
      expect(displayNumber('1234.56')).toBe('1 234.56');
      expect(displayNumber('1234.56|m')).toBe('1 234.56');
      expect(displayNumber(null)).toBe('');
      expect(displayNumber(undefined)).toBe('');
    });
    it('displays in scientific notation when appropriate', () => {
      // By default >1e7 or <1e-4 displays in scientific notation
      expect(displayNumber(9500.123)).toBe('9 500.123');
      expect(displayNumber(10500.123)).toBe('10 500.123');
      expect(displayNumber(10500.12345678)).toBe('10 500.12345678');
      expect(displayNumber(11_050_000.12345678)).toBe('1.105000012345678·10⁷');
      expect(displayNumber(0.0004)).toBe('0.0004');
      expect(displayNumber(0.00004)).toBe('4·10⁻⁵');
      expect(displayNumber(0.0000123456)).toBe('1.23456·10⁻⁵');

      //can round the coefficient by composing with round:
      expect(displayNumber(roundToPrecision(11_050_000.12345678))).toBe('1.105·10⁷');
      //or:
      expect(
        displayNumber(
          11_050_000.12345678,
          {scientific: true, roundScientificCoefficient: 4}
        )
      ).toBe('1.105·10⁷');

      //can configure the mix/max thresholds for showing scientific notation
      expect(
        displayNumber(1230000.123, {autoScientificBelow: 1e-7, autoScientificAbove: 1e7})
      ).toBe('1 230 000.123');
      expect(
        displayNumber(12300000.123, {autoScientificBelow: 1e-7, autoScientificAbove: 1e7})
      ).toBe('1.2300000123·10⁷');
      expect(
        displayNumber(1.124e-9, {autoScientificBelow: 1e7, autoScientificAbove: 1e7})
      ).toBe('1.124·10⁻⁹');

      //can configure to _only_ show (all values) in scientific notation
      expect(displayNumber('10500.12345678', { scientific: false })).toBe(
        '10 500.12345678'
      );

      //can configure to _only_ show (all values) in scientific notation
      expect(displayNumber('1234.56', { scientific: true })).toBe(
        '1.23456·10³'
      );
    });
    it('optionally supports e-notation', () => {
      expect(displayNumber(10500.12, {eNotation: true})).toBe('10 500.12');
      expect(displayNumber(11_050_000, {eNotation: true})).toBe('1.105e7');
      expect(displayNumber(0.00004, {eNotation: true})).toBe('4e-5');
    });
    it('should display formatted units when enabled and the input is a unit string', () => {
      expect(displayNumber('1234.56|m3/min', { withUnit: true })).toBe(
        '1 234.56 m³/min'
      );
      expect(displayNumber('|m3/min', { withUnit: true })).toBe('m³/min');
      expect(displayNumber('0|cm', { withUnit: true })).toBe('0 cm');
      //not when not enabled
      expect(displayNumber('1234.56|m3/min')).toBe('1 234.56');
      //not when no unit string
      expect(displayNumber('1234.56', { withUnit: true })).toBe('1 234.56');
    });
  });
  describe('displayNumberToFixed', () => {
    it('preserves trailing decimal zeros for string inputs, except for scientific notation outputs', () => {
      expect(displayNumberToFixed('1234.5600')).toBe('1 234.5600');
      expect(displayNumberToFixed('1234.0')).toBe('1 234.0');
      expect(displayNumberToFixed('1.2300e-2')).toBe('0.012300');
      expect(displayNumberToFixed('0.000000012300')).toBe('1.23·10⁻⁸');
      expect(displayNumberToFixed('1.2300e-8')).toBe('1.23·10⁻⁸');
      expect(displayNumberToFixed('1.2300e8')).toBe('1.23·10⁸');
      expect(displayNumberToFixed('123000000')).toBe('1.23·10⁸');
      expect(displayNumberToFixed('12300000.0')).toBe('1.23·10⁷');
      expect(displayNumberToFixed(null)).toBe('');
      expect(displayNumberToFixed(undefined)).toBe('');
    });
  });
});
