import { convertNumberToLocale } from '../internationalization/internationalization.ts';
import { parseNumber, countTrailingZeros } from '../parse/parse-number.ts';
import { toNum, isValidNum } from './numbers.ts';
import { round } from '../rounding/rounding.ts';
import { trim } from '../utils.ts';
import { LABELS } from '../constants.ts';

const DEFAULT_AUTO_SCIENTIFIC_BELOW = 1e-4;
const DEFAULT_AUTO_SCIENTIFIC_ABOVE = 1e7;

const superscriptSymbols: Record<string, string> = {
  '0': '⁰',
  '1': '¹',
  '2': '²',
  '3': '³',
  '4': '⁴',
  '5': '⁵',
  '6': '⁶',
  '7': '⁷',
  '8': '⁸',
  '9': '⁹',
  '+': '⁺',
  '-': '⁻',
};

type NumberDisplayOptions = {
  scientific?: 'auto' | boolean;
  eNotation?: boolean,
  autoScientificBelow?: number,
  autoScientificAbove?: number,
  roundScientificCoefficient?: number,
  withUnit?: boolean;
  noThousandsSeparator?: boolean;
  nonBreakingSpace?: boolean;
};

type PrivateNumberDisplayOptions = NumberDisplayOptions & {
  preserveTrailingZeros?: boolean;
};

const appendTrailingZeros = (value: string, numberOfZeros: number): string => {
  const zeros = '0'.repeat(numberOfZeros);
  return numberOfZeros > 0 && value !== '0'
    ? !value.includes('.')
      ? `${value}.${zeros}`
      : `${value}${zeros}`
    : value;
};

export const formatDecimal = (
  value: number | string,
  thousandSeparator: string,
  preserveTrailingZeros = false
): string => {
  const convertedValue = convertNumberToLocale(toNum(value), 'en-US')
    .replaceAll(',', thousandSeparator);
  return preserveTrailingZeros ? appendTrailingZeros(
    convertedValue,
    countTrailingZeros(value, true)
  ) : convertedValue;
};

export const formatDecimalDisplayNumber = (value: number | string | null | undefined, options?: PrivateNumberDisplayOptions): string => {
  const { nonBreakingSpace } = options ?? {};
  if (value === '') {
    return value;
  }
  if (value === null || value === undefined) {
    return '';
  }
  if (!isValidNum(value)) {
    return trim(value.toString());
  }
  const space = options?.noThousandsSeparator ? ''
    : nonBreakingSpace ? ' ' : ' '; //https://unicode-explorer.com/c/2007
  return formatDecimal(value, space, options?.preserveTrailingZeros);
};

export const formatScientificDisplayNumber = (value: number | string | undefined | null, options?: PrivateNumberDisplayOptions): string => {
  const {
    roundScientificCoefficient,
    eNotation
  } = options ?? {};
  if (Number.isNaN(value)) {
    return 'Invalid';
  }

  if (value === null || value === undefined) {
    return '';
  }
  if (!isValidNum(value) || value === '') {
    return trim(value.toString());
  }
  const sanitizedValue = toNum(value);
  if (!Number.isFinite(sanitizedValue)) {
    return trim(value.toString());
  }
  const power = eNotation ? 'e' : '·10';
  const [coefficient, exponent] = sanitizedValue.toExponential().split('e');
  const roundedCoefficient = typeof roundScientificCoefficient === 'number'
    ? round(coefficient, roundScientificCoefficient)
    : coefficient;
  const noExponent = exponent === '+0' || exponent === '-0';
  const formattedExponent = [...exponent.replaceAll('+', '')]
    .map((c: string) => eNotation ? c : superscriptSymbols[c])
    .join('');
  return noExponent ? roundedCoefficient : `${roundedCoefficient}${power}${formattedExponent}`;
};

export const formatDisplayNumber = (
  value: number | string,
  options: PrivateNumberDisplayOptions
): string => {
  const abs = Math.abs(toNum(value));
  const formatScientific = (
    options?.scientific === 'auto'
    && options?.autoScientificBelow
    && options?.autoScientificAbove
  )
    ? (abs < options?.autoScientificBelow || abs > options?.autoScientificAbove)
    : options?.scientific;
  return formatScientific
    ? formatScientificDisplayNumber(value, options)
    : formatDecimalDisplayNumber(value, options);
};

/**
 * Displays a number with human-friendly formatting (use for non-editable display labels, text)
 *
 * Accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 *
 * @example
 * //returns '1 234.56'
 * displayNumber(1234.56)
 *
 * By default, adds thousands separators. Can be configured to display in scientific notation, and with formatted units.
 *
 * @param value
 * @param options
 * @returns formatted display number
 */
export const displayNumber = <Value extends number | string | null | undefined>(
  value: Value,
  options?: NumberDisplayOptions
): string => {
  const optionsWithDefaults: Record<string, any> = {
    scientific: options?.scientific ?? 'auto',
    eNotation: options?.eNotation ?? false,
    autoScientificBelow: options?.autoScientificBelow ?? DEFAULT_AUTO_SCIENTIFIC_BELOW,
    autoScientificAbove: options?.autoScientificAbove ?? DEFAULT_AUTO_SCIENTIFIC_ABOVE,
    withUnit: options?.withUnit ?? false,
    nonBreakingSpace: options?.nonBreakingSpace ?? false,
    roundScientificCoefficient: options?.roundScientificCoefficient
  };
  const { withUnit } = optionsWithDefaults;
  if (value === null || value === undefined) {
    return '';
  }
  const { number, unit } = parseNumber(value);
  const formattedNumber = formatDisplayNumber(number, optionsWithDefaults);
  const formattedUnit = unit ? LABELS?.[unit] : '';
  return withUnit && unit ? (
    formattedNumber === '' ? formattedUnit : `${formattedNumber} ${formattedUnit}`
  ) : formattedNumber;
};

/**
 * Displays a number with human-friendly formatting (use for non-editable display labels, text)
 *
 * Accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 *
 * @example
 * //returns '1 234.5600'
 * displayNumberToFixed('1234.5600')
 *
 * By default, adds thousands separators. Can be configured to display in scientific notation, and with formatted units.
 *
 * @param value
 * @param options
 * @returns formatted display number
 */
export const displayNumberToFixed = <Value extends string | null | undefined>(
  value: Value,
  options?: NumberDisplayOptions
): string => {
  const optionsWithDefaults: Record<string, any> = {
    scientific: options?.scientific ?? 'auto',
    eNotation: options?.eNotation ?? false,
    autoScientificBelow: options?.autoScientificBelow ?? DEFAULT_AUTO_SCIENTIFIC_BELOW,
    autoScientificAbove: options?.autoScientificAbove ?? DEFAULT_AUTO_SCIENTIFIC_ABOVE,
    withUnit: options?.withUnit ?? false,
    nonBreakingSpace: options?.nonBreakingSpace ?? false,
    roundScientificCoefficient: options?.roundScientificCoefficient
  };
  const { withUnit } = optionsWithDefaults;
  if (value === null || value === undefined) {
    return '';
  }
  const preserveTrailingZeros = true;
  const { number, unit } = parseNumber(value, preserveTrailingZeros);
  const formattedNumber = formatDisplayNumber(number, {
    ...optionsWithDefaults,
    preserveTrailingZeros: true
  });
  const formattedUnit = unit ? LABELS?.[unit] : '';
  return withUnit && unit ? (
    formattedNumber === '' ? formattedUnit : `${formattedNumber} ${formattedUnit}`
  ) : formattedNumber;
};
