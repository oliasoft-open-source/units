import {
  cleanNumStr,
  isFraction,
  numFraction,
  isNull,
  isUndefined,
  isEmptyString,
  isTrailingPeriodSeparator,
  isTrailingCommaSeparator,
  isArray,
  isObject,
  isNumeric
} from '../utils.ts';
import {isValueWithUnit, getValue, SEPARATOR} from '../units.ts';
import {
  formatDecimalDisplayNumber,
} from './display-number.ts';

const parseValue = (value: unknown): unknown => {
  const isString = typeof value === 'string';
  const hasUnit = isString && isValueWithUnit(value);
  return hasUnit ? getValue(value) : value;
};

/**
 * Checks whether a value can be converted to number type by the toNum() function
 *
 * @param value - value to be checked
 * @returns whether number can be converted by toNum() function
 *
 * @example
 * isValidNum('1 1/2') -> true
 * toNum('foobar|m') -> false
 */
export const isValidNum = (value: unknown): boolean => {
  const parsedValue = parseValue(value);
  if (
    isEmptyString(parsedValue)
    || Number.isNaN(parsedValue)
    || parsedValue === Infinity
    || parsedValue === -Infinity
  ) {
    return true;
  } else {
    if (!(
      isNull(parsedValue)
      || isUndefined(parsedValue)
      || isTrailingPeriodSeparator(parsedValue)
      || isTrailingCommaSeparator(parsedValue)
      || isArray(parsedValue)
      || isObject(parsedValue)
    )) {
      const cleanedValue = cleanNumStr(String(parsedValue));
      if (cleanedValue.includes(SEPARATOR)) {
        return false;
      }
      const number = isFraction(cleanedValue) ? numFraction(cleanedValue)
        : isNumeric(cleanedValue) ? parseFloat(cleanedValue)
          : cleanedValue;
      if (number === Infinity || number === -Infinity) {
        return true;
      }
      if (!isNumeric(number)) {
        return false;
      }
      if (!Number.isNaN(number)
      ) {
        return true;
      }
    }
  }
  return false;
};

/**
 * Checks whether a value is a valid number, in string representation with scientific notation (e.g. '1e3')
 *
 * Note - it's not possible to check whether *number* types are stored in scientific notation (all numbers are stored
 * the same way internally in floating-point formats, so there is no difference between 1000 and 1e3 internally).
 * Whether number types get displayed in scientific notation or not in the console is a browser-specific implementation
 * detail (display formatting) and not something we can check/rely on, so this function is only intended for checking
 * user-input values in string format. See https://stackoverflow.com/a/66005705/942635.
 *
 * @param value - value to be checked
 * @returns whether the value is a valid number in scientific notation
 *
 * @example
 * isValidNum('1e3') -> true
 * toNum(1000) -> false
 * toNum(1e3) -> false (we cannot check scientific notation of number types)
 */

export const isScientificStringNum = (value: unknown) => {
  if (typeof value === 'string') {
    return isValidNum(value) && value.toLowerCase().includes('e');
  }
  return false;
};

/**
 * Converts a numeric value to number type (when possible).
 * - need to know if it's possible first? Call isValidNum()
 * - accepts number types (1.234), stringified numbers ('1.234'), fractions ('1/2'), and unit numbers ('1.234|m')
 * - returns the converted number if possible, otherwise returns the input value or default value when provided
 *
 * @param value - value to be converted to number type
 * @param [fallback] - optional fallback value (returned when not possible to convert)
 * @param [minimum] - optional minimum value
 * @returns valid number after conversion, or fallback, or returns the original input
 *
 * @example
 * toNum('1.2345) -> 1.2345
 * toNum('1.2345|m') -> 1.2345
 */
export const toNum = (
  value: unknown,
  fallback?: number,
  minimum?: number
): any => {
  const fallbackResult = fallback ?? value;
  const parsedValue = parseValue(value);
  if (!isValidNum(parsedValue)) {
    return fallbackResult;
  } else {
    const cleanedValue = cleanNumStr(String(parsedValue));
    const number = isFraction(cleanedValue) ? numFraction(cleanedValue)
      : isNumeric(cleanedValue) ? parseFloat(cleanedValue)
        : cleanedValue;
    if (number === Infinity || number === -Infinity) {
      return number;
    }
    if (Number.isNaN(number) || !isNumeric(number)) {
      return fallbackResult;
    } else if (minimum && number < minimum) {
      return minimum;
    }
    return number;
  }
};

/**
 * Convert a number to a string safely, better than String(value)
 *  String(0.0000002) returns '2e-7' which is unwanted if we need to preserve formatting
 *
 * @param value
 * @returns number or string output value
 */
export const toString = (value?: unknown) => {
  if (isValidNum(value)) {
    if (typeof value === 'string') {
      return value; // already a string
    }
    if (typeof value === 'number') {
      if (Number.isNaN(value) || !Number.isFinite(value)) {
        return String(value);
      }
      /*
        Note that String(0.0000002) -> '2e-7' in JavaScript, so we need to use a safe conversion function instead
        We could improve how the scientific notation inputs are handed too, but that's a minor breaking change to the
        current unit tests, so let's consider it separately in OW-17645
      */
      return formatDecimalDisplayNumber(value, { noThousandsSeparator: true });
    }
  }
  return value;
};
