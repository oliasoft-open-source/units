import {
  toNum,
  toString,
  isValidNum,
  isScientificStringNum
} from './numbers.ts';

describe('Numbers', () => {
  it('toNum() - should return cleaned number string', () => {
    expect(toNum(1)).toEqual(1);
    expect(toNum(1.234)).toEqual(1.234);
    expect(toNum('1.234')).toEqual(1.234);
    expect(toNum('1.234|m')).toEqual(1.234);
    expect(toNum('1,234|kg/m3')).toEqual(1.234);
    //Watch out! Units not defined in ALT_UNITS will *not* convert (due to how isValueWithUnit() works):
    expect(toNum('1,234|badUnit/cm')).toEqual('1,234|badUnit/cm');
    expect(toNum('13 3/8, Intermediate, Casing')).toEqual('13 3/8, Intermediate, Casing'); // test case for OW-19280
    expect(toNum(',1')).toEqual(0.1);
    expect(toNum(null)).toEqual(null);
    expect(toNum(undefined)).toEqual(undefined);
    expect(toNum(NaN)).toBeNaN();
    expect(toNum(Infinity)).toEqual(Infinity);
    expect(toNum(-Infinity)).toEqual(-Infinity);
    expect(toNum('1/0')).toEqual(Infinity);
    expect(toNum('-1/0')).toEqual(-Infinity);
    expect(toNum('')).toEqual('');
    expect(toNum([1, 2])).toEqual([1, 2]);
    expect(toNum({a: 'abc'})).toEqual({a: 'abc'});
    expect(toNum('1.')).toEqual('1.');
    expect(toNum('1,')).toEqual('1,');
    expect(toNum('1/10')).toEqual(0.1);
    expect(toNum('1.123e4')).toEqual(11230);
    expect(toNum('1.123E4')).toEqual(11230);
    expect(toNum(1.123e4)).toEqual(11230);
    expect(toNum('1.123e+4')).toEqual(11230);
    expect(toNum('1.123e-4')).toEqual(0.0001123);
    expect(toNum('1/10|m')).toEqual(0.1);
    expect(toNum('1/10|kg/m3')).toEqual(0.1);
    // Watch out! Units not defined in ALT_UNITS will *not* convert (due to how isValueWithUnit() works)
    expect(toNum('1/10|badUnit/cm')).toEqual('1/10|badUnit/cm');
    expect(toNum('1.123e-4|m')).toEqual(0.0001123);
    expect(toNum(NaN)).toEqual(NaN);
    expect(toNum(0.05, 0.05, 0.1)).toEqual(0.1);
    expect(toNum(0.1)).toEqual(0.1);
    expect(toNum('   123\n,  56\t7   ')).toEqual(123.567);
    /*
      OW-19280: we *don't* generally support evaluation of maths expressions in input strings (except fraction strings)
      If needed in the future, we could consider a package like math-expression-evaluator
    */
    expect(toNum('1.2|-44|')).toEqual('1.2|-44|');
    expect(toNum('1.2-44')).toEqual('1.2-44');
    expect(toNum('1.2*3')).toEqual('1.2*3');
  });
  it('toNum() - fallback and minimum values', () => {
    expect(toNum('1,23', 2)).toEqual(1.23);
    expect(toNum(null, 2)).toEqual(2);
    expect(toNum(undefined, 2)).toEqual(2);
    expect(toNum(0, 2)).toEqual(0);
    expect(toNum('1,23', 2, 4)).toEqual(4);
  });
  it('OW-19695 handle + in numbers', () => {
    expect(toNum('+5')).toEqual(5);
    expect(toNum('-5')).toEqual(-5);
    expect(isValidNum('+5')).toEqual(true);
    expect(isValidNum('-5')).toEqual(true);
  });
  it('isValidNum validates if a number is possible to convert with toNum()', () => {
    expect(isValidNum(1.234)).toEqual(true);
    expect(isValidNum(-1.2345678)).toEqual(true);
    expect(isValidNum(0)).toEqual(true);
    expect(isValidNum('1.234')).toEqual(true);
    expect(isValidNum('1.234|m')).toEqual(true);
    expect(isValidNum('1,234|kg/m3')).toEqual(true);
    expect(isValidNum(NaN)).toEqual(true);
    expect(isValidNum(Infinity)).toEqual(true);
    expect(isValidNum(-Infinity)).toEqual(true);
    //Watch out! Units not defined in ALT_UNITS will *not* convert (due to how isValueWithUnit() works)
    expect(isValidNum('1,234|badUnit/cm')).toEqual(false);
    expect(isValidNum('13 3/8, Intermediate, Casing')).toEqual(false); // test case for OW-19280
    expect(isValidNum(null)).toEqual(false);
    expect(isValidNum(undefined)).toEqual(false);
    expect(isValidNum({})).toEqual(false);
    expect(isValidNum([])).toEqual(false);
    expect(isValidNum('foobar')).toEqual(false);
    expect(isValidNum('')).toEqual(true); //allow empty values
    expect(isValidNum('.')).toEqual(false); //don't allow empty decimal separator
    expect(isValidNum(',')).toEqual(false); //don't allow empty comma decimal separator
    expect(isValidNum('66.')).toEqual(false); //don't allow trailing decimal separator
    expect(isValidNum('66,')).toEqual(false); //don't allow trailing comma decimal separator
    expect(isValidNum('76')).toEqual(true);
    expect(isValidNum('81.034')).toEqual(true);
    expect(isValidNum('0')).toEqual(true);
    expect(isValidNum('-123.456')).toEqual(true);
    expect(isValidNum('1000,05')).toEqual(true);
    expect(isValidNum('2 1/2')).toEqual(true);
    expect(isValidNum('1/2')).toEqual(true);
    expect(isValidNum('1/0')).toEqual(true);
    expect(isValidNum('1.123e4')).toEqual(true);
    expect(isValidNum('1.123E4')).toEqual(true);
    expect(isValidNum(1.123e4)).toEqual(true);
    expect(isValidNum('1.123e+4')).toEqual(true);
    expect(isValidNum('1.123e-4')).toEqual(true);
    expect(isValidNum('1/10|m')).toEqual(true);
    expect(isValidNum('1/10|kg/m3')).toEqual(true);
    // Watch out! Units not defined in ALT_UNITS will *not* convert (due to how isValueWithUnit() works)
    expect(isValidNum('1/10|badUnit/cm')).toEqual(false);
    expect(isValidNum('foobar|m')).toEqual(false);
    expect(isValidNum('1.123e-4|m')).toEqual(true);
    expect(isValidNum('   123\n,  56\t7   ')).toEqual(true);
    expect(isValidNum('123|4|m')).toEqual(false);
    /*
      OW-19280: we *don't* generally support evaluation of maths expressions in input strings (except fraction strings)
      If needed in the future, we could consider a package like math-expression-evaluator
    */
    expect(isValidNum('1.2|-44|')).toEqual(false);
    expect(isValidNum('1.2-44')).toEqual(false);
    expect(isValidNum('1.2*3')).toEqual(false);
  });
  it('isScientificStringNum validates if a number is possible to convert with toNum()', () => {
    expect(isScientificStringNum(1.234)).toEqual(false);
    expect(isScientificStringNum('1.234')).toEqual(false);
    expect(isScientificStringNum('1.234|m')).toEqual(false);
    expect(isScientificStringNum('1,234|kg/m3')).toEqual(false);
    expect(isScientificStringNum(NaN)).toEqual(false);
    expect(isScientificStringNum(Infinity)).toEqual(false);
    expect(isScientificStringNum(-Infinity)).toEqual(false);
    //Watch out! Units not defined in ALT_UNITS will *not* convert (due to how isValueWithUnit() works)
    expect(isScientificStringNum('1,234|badUnit/cm')).toEqual(false);
    expect(isScientificStringNum(null)).toEqual(false);
    expect(isScientificStringNum(undefined)).toEqual(false);
    expect(isScientificStringNum({})).toEqual(false);
    expect(isScientificStringNum([])).toEqual(false);
    expect(isScientificStringNum('foobar')).toEqual(false);
    expect(isScientificStringNum('')).toEqual(false); //allow empty values
    expect(isScientificStringNum('.')).toEqual(false); //don't allow empty decimal separator
    expect(isScientificStringNum(',')).toEqual(false); //don't allow empty comma decimal separator
    expect(isScientificStringNum('66.')).toEqual(false); //don't allow trailing decimal separator
    expect(isScientificStringNum('66,')).toEqual(false); //don't allow trailing comma decimal separator
    expect(isScientificStringNum('76')).toEqual(false);
    expect(isScientificStringNum('81.034')).toEqual(false);
    expect(isScientificStringNum('1000,05')).toEqual(false);
    expect(isScientificStringNum('2 1/2')).toEqual(false);
    expect(isScientificStringNum('1.123e4')).toEqual(true);
    expect(isScientificStringNum('1.123E4')).toEqual(true);
    expect(isScientificStringNum(1.123e4)).toEqual(false); //expected (we cannot check whether number types are stored in scientific notation)
    expect(isScientificStringNum('1.123e+4')).toEqual(true);
    expect(isScientificStringNum('1.123e-4')).toEqual(true);
    expect(isScientificStringNum('1.123E4|m')).toEqual(true);
    expect(isScientificStringNum('1.123e+4|m')).toEqual(true);
    expect(isScientificStringNum('1/2e+4|m')).toEqual(true);
    expect(isScientificStringNum('1.123e-4|ft')).toEqual(true);
    expect(isScientificStringNum('1/10|m')).toEqual(false);
    expect(isScientificStringNum('1/10|kg/m3')).toEqual(false);
    // Watch out! Units not defined in ALT_UNITS will *not* convert (due to how isValueWithUnit() works)
    expect(isScientificStringNum('1/10|badUnit/cm')).toEqual(false);
    expect(isScientificStringNum('foobar|m')).toEqual(false);
    expect(isScientificStringNum('1.123e-4|m')).toEqual(true);
    expect(isScientificStringNum('   123\n,  56\t7   ')).toEqual(false);
    expect(isScientificStringNum('123|4|m')).toEqual(false);
  });
  it('toString should safely convert numbers to decimal strings', () => {
    expect(toString(1)).toEqual('1');
    expect(toString('1')).toEqual('1');
    expect(toString(999999.12345)).toEqual('999999.12345');
    expect(toString('999999.12345')).toEqual('999999.12345');
    expect(toString(0.000000000213)).toEqual('0.000000000213');
    expect(String(0.000000000213)).toEqual('2.13e-10'); // be aware String() gives scientific notation 2.13e-10
    expect(toString('0.000000000213')).toEqual('0.000000000213');
    expect(toString(0.000000000213)).toEqual('0.000000000213');
    expect(toString(2.13e-10)).toEqual('0.000000000213');
    expect(toString('2.13e-10')).toEqual('2.13e-10');
    expect(toString(5e5)).toEqual('500000');
    expect(toString('5e5')).toEqual('5e5');
    expect(toString('5,5')).toEqual('5,5');

    // edge cases:
    expect(toString(null)).toEqual(null);
    expect(toString(undefined)).toEqual(undefined);
    // up for debate what to do with NaN, +/- infinity (for now, do same as String(value))
    expect(toString(NaN)).toEqual('NaN');
    expect(toString(Infinity)).toEqual('Infinity');
    expect(toString(-Infinity)).toEqual('-Infinity');
    expect(toString('')).toEqual('');
    expect(toString([1, 2])).toEqual([1, 2]);
    expect(toString({a: 'abc'})).toEqual({a: 'abc'});
  });
});
