/* istanbul ignore file */
import {
  label,
  showAltUnitsList,
  getAltUnitsListByQuantity,
  checkAndCleanDecimalComma,
  unitFromKey,
  to,
  split,
  unum,
  toBase,
  altUnitsList,
  convertTable,
  roundNumberWithLabel,
  withUnit,
  unumWithUnit,
  validateAndClean,
  isValueWithUnit,
  getValue,
  getUnit,
  convertSamePrecision,
  convertAndGetValue,
  convertAndGetValueStrict,
  getUnitsForQuantity,
  getQuantities,
  unitFromQuantity,
  withPrettyUnitLabel,
  validateNumber,
} from './units.ts';

import {
  ALT_UNITS,
  LABELS,
  UNIT_FROM_KEY,
  KNOWN_CONVERSIONS,
  DEPRECATED_UNITS,
  UNIT_ALIASES,
  INTERMEDIATE_CONVERSIONS,
  KNOWN_UNITS,
  QUANTITIES_DESCRIPTION,
  UNITS_DESCRIPTION
} from './constants.ts';

import {
  isNumeric,
  isNonNumerical,
  isEmptyValueWithUnit,
  allNumbers,
  formatNumber,
  charCount,
  getNumberOfDigitsToShow,
  fraction,
  isFraction,
  asFraction,
  numFraction,
  cleanNumStr,
  cleanNum,
  stripLeadingZeros
} from './utils.ts';

import {
  toNum,
  toString,
  isValidNum,
  isScientificStringNum
} from './numbers/numbers.ts';

import {
  displayNumber,
  displayNumberToFixed
} from './numbers/display-number.ts';

import {
  round,
  roundToFixed,
  roundToPrecision,
  roundToDecimalPrecision,
  roundByMagnitude,
  roundByMagnitudeToFixed,
  roundByRange
} from './rounding/rounding.ts';

import {
  isCloseTo,
  isDeepCloseTo,
  isCloseToOrLessThan,
  isCloseToOrGreaterThan
} from './comparison/comparison.ts';

export {
  // Units
  label,
  showAltUnitsList,
  getAltUnitsListByQuantity,
  getUnitsForQuantity,
  getQuantities,
  checkAndCleanDecimalComma,
  unitFromKey,
  unitFromQuantity,
  to,
  split,
  unum,
  toBase,
  altUnitsList,
  convertTable,
  roundNumberWithLabel,
  withUnit,
  unumWithUnit,
  validateAndClean,
  isValueWithUnit,
  getValue,
  getUnit,
  convertSamePrecision,
  convertAndGetValue,
  convertAndGetValueStrict,
  withPrettyUnitLabel,
  validateNumber,
  //Rounding
  round,
  roundToFixed,
  roundToPrecision,
  roundToDecimalPrecision,
  roundByMagnitude,
  roundByMagnitudeToFixed,
  roundByRange,
  //Comparison
  isCloseTo,
  isDeepCloseTo,
  isCloseToOrLessThan,
  isCloseToOrGreaterThan,
  // Utils
  isNumeric,
  isNonNumerical,
  isEmptyValueWithUnit,
  allNumbers,
  formatNumber,
  displayNumber,
  displayNumberToFixed,
  charCount,
  getNumberOfDigitsToShow,
  fraction,
  isFraction,
  asFraction,
  numFraction,
  cleanNumStr,
  cleanNum,
  toNum,
  toString,
  isValidNum,
  isScientificStringNum,
  stripLeadingZeros,
  // Constants
  ALT_UNITS,
  LABELS,
  UNIT_FROM_KEY,
  KNOWN_CONVERSIONS,
  DEPRECATED_UNITS,
  UNIT_ALIASES,
  INTERMEDIATE_CONVERSIONS,
  KNOWN_UNITS,
  QUANTITIES_DESCRIPTION,
  UNITS_DESCRIPTION
};
