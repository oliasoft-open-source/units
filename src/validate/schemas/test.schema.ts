const testSchema = {
  $id: 'testValidate',
  type: 'object',
  properties: {
    name: {
      type: 'string',
      minLength: 1,
    },
  },
  required: ['name'],
};

export default testSchema;