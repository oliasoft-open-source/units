const validNumberSchema = {
  type: 'number',
  errorMessage: 'Must be a numerical value',
};

export default validNumberSchema;
