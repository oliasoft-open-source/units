import type { ErrorObject } from 'ajv';

export const transformErrors = (errors: ErrorObject[]) => {
  return errors?.map(({ message }: ErrorObject) => message);
};
