import { transformErrors } from './errors-transform.ts';
import { numberSchemaValidator, testSchemaValidator } from './ajv-validators.ts';

describe('Schema validation', () => {
  it('should successfully validate simple data', () => {
    const data = {
      name: 'Test name',
    };
    const valid = testSchemaValidator(data);
    expect(valid).toBe(true);
  });

  it('should fail to validate missing property', () => {
    const data = {
      anotherName: 'Test name',
    };
    const valid = numberSchemaValidator(data);
    const result = { valid, errors: transformErrors(numberSchemaValidator.errors) };
    expect(result.valid).toBe(false);
    expect(result.errors).toHaveLength(1);
  });

  it('should fail to validate minimum length constraint', () => {
    const data = {
      name: '',
    };
    const valid = numberSchemaValidator(data);
    const result = { valid, errors: transformErrors(numberSchemaValidator.errors) };
    expect(result.valid).toBe(false);
    expect(result.errors).toHaveLength(1);
  });
});
