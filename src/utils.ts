import Fraction from 'fraction.js';
import { displayNumber } from './numbers/display-number.ts';
import {EXP_NOTATION_RE} from './units.ts';

/* eslint-disable  @typescript-eslint/no-explicit-any */
/* eslint-disable  @typescript-eslint/explicit-module-boundary-types */
// Those disables are because a lot of arguments in methods here are "any" type
// later inside function there are a lot of checks what type of the argument it is
// and Lint is restricting usage of "any" type to much

// helper inline methods
export const isNull = (str: any): boolean => str === null;
export const isUndefined = (str: any): boolean => str === undefined;
export const isArray = (str: any): boolean => str && str.constructor === Array;
export const isObject = (str: any): boolean => str && str.constructor === Object;
export const isEmptyString = (str: any): boolean => str === '';
export const isTrailingPeriodSeparator = (str: any): boolean => str && str[str.length - 1] === '.';
export const isTrailingCommaSeparator = (str: any): boolean => str && str[str.length - 1] === ',';
export const isPercentage = (value: string | number) : boolean => typeof value === 'string' && /^\d+(\.\d+)?%$/.test(value);

/**
 * Checks if input is a string that starts with '|', e.g. '|m' or '|in'
 *
 * @param val - string or number for checking
 * @returns true if input starts with '|', e.g. '|m' or '|in'
 */
export function isEmptyValueWithUnit(val: string | number): boolean {
  return typeof val === 'string' && val.length > 0 && val.startsWith('|');
}

const trimWhiteSpace = (value: string) => value.trim().replace(/\s+/g, ' ');

/**
 * Check if provided argument is number
 * @param v - value for checking
 * @returns true if valid number
 */

/**
 * Checks if a value can be parsed as a valid fraction (uses fraction.js)
 *
 * @param value - The value to check.
 * @returns True if the value is a valid fraction, false otherwise.
 */
export const isFraction = (value: unknown): boolean => {
  if (typeof value !== 'string') {
    return false;
  }
  if (!value.includes('/')) {
    return false;
  }
  try {
    // eslint-disable-next-line no-new
    new Fraction(trimWhiteSpace(value));
    return true;
  } catch (error) {
    return (error as Error).message === 'Division by Zero';
  }
};

export function isNumeric(v: unknown): boolean {
  if (
    v === undefined
    || v === null
    || Array.isArray(v)
    || typeof v === 'object'
    || Number.isNaN(v)
    || v === 'NaN'
    || v === Infinity
    || v === -Infinity
    || v === 'Infinity'
    || v === '-Infinity'
  ) {
    return false;
  }
  if (isFraction(v)) {
    return true;
  }
  const parsed = parseFloat(v as unknown as string);
  return typeof v === 'string'
    ? !isNaN(parsed) && (EXP_NOTATION_RE.test(v) || isFinite(v as unknown as number))
    : isFinite(v as unknown as number);
}
/**
 * Check array values if all are numbers
 * @param arr - array of items
 * @returns true if all array items are numbers
 */
export function allNumbers(arr: (number | string)[]): boolean {
  return (!arr.some(val => typeof val !== 'number'));
}

/**
 * Outputs a human-friendly formatted number for UI display (with thousands separators)
 *
 * @deprecated use displayNumber instead
 * @param number - anything for converting to pretty format
 * @returns string with formatted display number
 */
export const formatNumber = (number: number | string | undefined | null): string => displayNumber(number);

/**
 * Counts all occurence of set character
 * From http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string
 * @param chr - character that will be counted in string
 * @param str - string in which the character will be counted
 * @returns number of character occurencies
 */
export function charCount(chr: string | number, str: string): number {
  const single_char = (chr + '')[0];
  let total = 0;
  let last_location = str.indexOf(single_char, 0) + 1;
  while (last_location > 0) {
    last_location = str.indexOf(single_char, last_location) + 1;
    total += 1;
  }
  return total;
}

/**
 * Calculates the number of digits to be rounded off, typically for values less than 1
 * E.g. when trying to format 1e-9 by roundNumber(), the output value will only show '0'
 * if just rounded off with 4 digits from roundNumber(val)
 * Then it is more useful to calculate the number of digits to be rounded off, and pass this in,
 * i.e. roundNumber(val, getNumberOfDigitsToShow(val))
 * which will return 0.0000000001.
 * Probably a lot of room for improvement here...
 *
 * @deprecated Use roundByMagnitude or other units package rounding functions instead
 * @param num - number for caculating digits
 * @param maxNumDigits - optional limiter of digits number, by default 20
 * @returns number of digits that will be shown for defined number
 */
export function getNumberOfDigitsToShow(num: number | string, maxNumDigits = 20): number {
  const defaultDigits = Math.min(4, maxNumDigits);
  let digits = defaultDigits;
  if (typeof num !== 'number') {
    return defaultDigits;
  }

  const numStr = String(num);

  const RegExForExponentialNumber = /-?[0-9.,]*[Ee]-?[0-9]+/;
  if (RegExForExponentialNumber.test(numStr)) {
    // e.g. 4.2e-8
    while (Math.abs(num) * (10 ** digits) < 1 && digits < maxNumDigits) {
      digits++;
    }
    // There is one digit in front of comma:
    return Math.min(digits + (defaultDigits - 1), maxNumDigits);
  }

  if (num > 1 || num < -1) {
    return defaultDigits;
  }

  for (let i = 2; i < numStr.length; i++) {
    const element = numStr[i];
    if (element !== '0') {
      return Math.min(maxNumDigits, digits + i - 2);
    }
  }
  return digits;
}

/**
 * Convert set fraction to decimal value
 * @param str - fraction to be converted
 * @returns either number in decimal format, Infinity if fraction is divided by 0 or NaN in other cases
 */
export function fraction(str: any): number | typeof Infinity | typeof NaN {
  if (str instanceof Array || str === null || str === undefined) {
    return NaN;
  }
  if (typeof str === 'string') {
    str = trimWhiteSpace(str); //whitespace causes InvalidParameter error in fraction.js
  }
  if (str === '') {
    return NaN;
  }
  let result = NaN;
  let infinite = false;
  try {
    const fractionObject = new Fraction(str);
    if (fractionObject !== undefined) {
      result = fractionObject.valueOf();
    }
  } catch (e) {
    if (e instanceof Error && e.message === 'Division by Zero') {
      infinite = true;
    }
  }
  return infinite ? Infinity : result;
}

/**
 * Converts decimal number to fractional format
 * @param str - value to be converted
 * @returns string with fractional format of set value
 */
export function asFraction(str: number | string): string {
  if (typeof str === 'string') {
    str = trimWhiteSpace(str); //whitespace causes InvalidParameter error in fraction.js
  }
  if (str === '') {
    str = '0';
  }
  return new Fraction(str as any).toFraction(true);
}

/**
 * Convert fraction string to number (return input value if conversion fails)
 * For historical reasons, numFraction returns the string value
 * unmodified if it is not able to convert to a number. This is
 * useful where user inputs are filtered through calls to
 * numFraction. For "detecting" when numFraction fails, check
 * if the return value is a string or a number. If it is a string
 * it means number conversion failed.
 *
 * @param str - fraction to be converted
 * @returns string or number with decimal format of fraction
 */
export function numFraction(str: any): any {
  if (
    str instanceof Array
    || str === null
    || str === undefined
    || str === ''
    || str === Infinity
    || str === -Infinity
    || str === 'Infinity'
    || str === '-Infinity'
    || Number.isNaN(str)
    || str === 'NaN'
  ) {
    return str;
  }
  if (typeof str === 'string') {
    str = trimWhiteSpace(str); //whitespace causes InvalidParameter error in fraction.js
  }
  let result = str;
  try {
    const fractionObject = new Fraction(str);
    if (fractionObject !== undefined) {
      result = fractionObject.valueOf();
    }
  } catch (error) {
    if ((error as Error).message === 'Division by Zero') {
      const minus = str.charAt(0) === '-';
      return minus ? -Infinity : Infinity;
    }
    console.warn('Error in numFraction() method: ', str);
  }
  return result.valueOf();
}

/**
 * Basic trimming of string values to remove leading and trailing spaces, tabs, newlines
 * @param value
 * @returns trimmed string
 */
export const trim = (value: string): string => value.trim().replace(/[\t\r\n]/g, '');

/**
 * Cleaning up and fixing provided number to correct numerical format
 * removing redundant '.' dots, ',' commas, spaces
 * @param str - string for cleaning up
 * @returns cleaned/formatted string
 */
export function cleanNumStr(str: string | number): string {
  let cleanString = trim(str + '');
  const slashCount = charCount('/', cleanString);
  const spaceCount = charCount(' ', cleanString) + charCount(' ', cleanString);
  let dotcount = charCount('.', cleanString);
  let commacount = charCount(',', cleanString);
  if (slashCount === 0 && spaceCount > 0) {
    cleanString = cleanString.replace(/\s/g, '');
  }
  if (commacount > 1) {
    cleanString = cleanString.replace(/,/g, '');
  }
  if (dotcount > 1) {
    cleanString = cleanString.replace(/\./g, '');
  }
  commacount = charCount(',', cleanString);
  dotcount   = charCount('.', cleanString);
  if ((dotcount === 1) && (commacount === 1)) {
    // One of each, make the rightmost act as decimal separator
    if (cleanString.indexOf(',') > cleanString.indexOf('.')) {
      cleanString = cleanString.replace('.', '');
      cleanString = cleanString.replace(',', '.');
    } else {
      cleanString = cleanString.replace(',', '');
    }
    if (cleanString.indexOf('.') === 0) {
      cleanString = 0 + cleanString;
    }
    return cleanString;
  }

  if (!dotcount && commacount) {
    cleanString = cleanString.replace(',', '.');
  }
  if (cleanString.indexOf('.') === 0) {
    cleanString = 0 + cleanString;
  }
  return cleanString;
}

export const stripLeadingZeros = (value: string) => {
  const isMinus = value?.[0] === '-';
  //https://stackoverflow.com/a/60509848/942635
  const matchMinus = /^-/mg;
  const matchLeadingZeros = /^(?:0+(?=[1-9])|0+(?=0))/mg;
  const cleanedValue = value
    .replace(matchMinus, '')
    .replace(matchLeadingZeros, '');
  return isMinus ? `-${cleanedValue}` : cleanedValue;
};

/**
 * Cleaning and fixing numerical string but returns it as number
 * @param str - string for cleaning up
 * @returns number after cleanup and fixing
 */
export function cleanNum(str: string | number): number {
  if (typeof str === 'number') {
    return str;
  }
  return parseFloat(cleanNumStr(str));
}

/**
 * Check if value is non-numerical
 *
 * @param value
 * @return boolean
 */
export const isNonNumerical = (value: any): boolean => !isNumeric(value);

const { abs, exp, sqrt } = Math;

/**
 * The 1D iterative Newton's method
 *
 * @param f
 * @param df
 * @param x0
 * @param tol
 * @param max_iter
 */
export function newton(
  f: (k: number) => number,
  df: (k: number) => number,
  x0: number,
  tol = 1e-8,
  max_iter = 30
): [number, boolean, number] {
  let X = x0;
  let iter = 0;
  let err = 1;
  while (err > tol && iter < max_iter) {
    const X_new = X - f(X) / df(X);
    err = abs(f(X_new) - f(X));
    if (err < tol) {
      return [X_new, true, iter];
    } else {
      X = X_new;
    }
    iter++;
  }
  return [x0, false, iter];
}

/**
 * Approximating the error function erf
 *
 * @param z
 */
export function erf(z: number) {
  const t = 1 / (1 + 0.5 * abs(z));
  // Use Horner's method:
  const ans = 1 - t * exp(-(z ** 2) - 1.26551223
    + t * (1.00002368 + t * (0.37409196 + t * (0.09678418
      + t * (-0.18628806 + t * (0.27886807 + t * (-1.13520398
        + t * (1.48851587 + t * (-0.82215223 + t * (0.17087277))))))))));
  return (z >= 0.0) ? ans : -ans;
}

/**
 * Convert from confidence interval to number of standard deviations
 *
 * @param a
 */
export function get_k_from_conf_int(a: number): number {
  const f = (k: number) => a - erf(k / sqrt(2));
  const df = (k: number) => -sqrt(2 / Math.PI) * exp(-0.5 * k ** 2);
  const [t0] = newton(f, df, 1);
  return t0;
}

/**
 * Convert from number of standard deviations to confidence interval
 *
 * @param k
 */
export function get_conf_int_from_k(k: number): number {
  return erf(k / sqrt(2));
}
