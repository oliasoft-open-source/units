import {
  ALT_UNITS,
  LABELS,
  UNIT_FROM_KEY,
  KNOWN_CONVERSIONS,
  DEPRECATED_UNITS,
  UNIT_ALIASES,
  INTERMEDIATE_CONVERSIONS,
  KNOWN_UNITS,
  SPECIAL_NUMBERS_STRING,
} from './constants.ts';
import {
  isNonNumerical,
  cleanNumStr,
  cleanNum,
  charCount,
  isNumeric,
} from './utils.ts';
import { displayNumber } from './numbers/display-number.ts';
import { toNum, toString } from './numbers/numbers.ts';
import { round } from './rounding/rounding.ts';
import type { AltUnitWithLabel, ValidateReturn } from './interfaces.ts';
import { numberSchemaValidator } from './validate/ajv-validators.ts';
import { transformErrors } from './validate/errors-transform.ts';

export const SEPARATOR = '|';
const UNIT_RE = /^(-?[0-9., /]*?(?:e[-+]?[0-9]+)?)([^0-9-., /].*)?$/;
export const EXP_NOTATION_RE = /^[-+]?[0-9]*\.?[0-9]+(?:\/[0-9]*\.?[0-9]+)?(?:[eE][-+]?[0-9]+)?$/;

/**
 * Get list of units for a given quantity
 *
 * @param quantityKey
 * @return []
 */
export function showAltUnitsList(quantityKey: string): string[] | undefined {
  return ALT_UNITS[quantityKey];
}

/**
 * Get list of units for a given quantity
 *
 * @param quantity
 * @return [] | undefined
 */
export function getUnitsForQuantity(quantity: string): string[] | undefined {
  return showAltUnitsList(quantity);
}

/**
 * Get list of all defined quantities
 * @returns []
 */
export function getQuantities(): string[] | undefined {
  return Object.keys(ALT_UNITS);
}

/**
 * Get unit label
 *
 * @param unitKey
 * @return string|undefined
 */
export function label(unitKey: string): string | undefined {
  return LABELS[unitKey];
}

/**
 * Get unit by quantity
 *
 * @param quantity
 * @return string|undefined
 */
export function unitFromKey(quantity: string): string | undefined {
  return UNIT_FROM_KEY[quantity];
}

/**
 * Get unit by quantity
 *
 * @param quantity
 * @return string|undefined
 */
export function unitFromQuantity(quantity: string): string | undefined {
  return unitFromKey(quantity);
}

/**
 * Get list of alternative units, with labels, for a given quantity.
 *
 * @param quantity
 * @return AltUnitWithLabel[]|undefined
 */
export function getAltUnitsListByQuantity(quantity: string): AltUnitWithLabel[] | undefined {
  const quantityUnitList = showAltUnitsList(quantity);
  return quantityUnitList
    ? quantityUnitList.map(unit => ({ unit, label: label(unit) }))
    : undefined;
}

/**
 * Find double dot and comma in value and replace it with decimal dot.
 * For example: 123,4 => 123.4 or 123..4 => 123.4
 *
 * @param val
 * @return string|number
 */
export function checkAndCleanDecimalComma(val: string | number): string | number {
  const RegExForMultiDots = /\.{2,}/;
  const RegexFindComma = /,/;
  if (typeof val === 'string') {
    while (RegExForMultiDots.test(val) || RegexFindComma.test(val)) {
      val = val.replace(RegExForMultiDots, '.');
      val = val.replace(RegexFindComma, '.');
    }
  }
  return val;
}

/**
 * Convert value to another unit
 *
 * @param value clean value without unit
 * @param fromUnit unit from which we are trying to convert
 * @param toUnit unit to which we are trying to convert
 * @return number|Error
 */
export function to(value: string | number, fromUnit: string, toUnit: string): number {
  value = checkAndCleanDecimalComma(value);
  if (toUnit === 'undefined') {
    console.warn('Inconsistent "to unit" - debug call to "Units.to()"');
  }
  if (fromUnit === 'undefined') {
    fromUnit = toUnit;
    console.warn('Inconsistent "from unit" - debug call to "Units.to()"');
  }
  if (fromUnit === toUnit) {
    return toNum(value);
  }

  if (value === Infinity || value === 'Infinity') {
    return Infinity;
  }
  if (value === -Infinity || value === '-Infinity') {
    return -Infinity;
  }
  if (isNonNumerical(value) && value !== '') {
    return NaN;
  }

  const conv = KNOWN_CONVERSIONS[fromUnit + '|' + toUnit];

  // If conversion is known directly, just do it.
  if (conv) {
    return conv(toNum(value));
  }

  // Do deprecated units before intermediate conversions,
  // as these does not introduce any real conversions (rouding etc):
  if (DEPRECATED_UNITS[fromUnit]) {
    console.warn(
      `Unit '${fromUnit}' is deprecated - use '${DEPRECATED_UNITS[fromUnit]}' instead.`,
    );
    return to(value, DEPRECATED_UNITS[fromUnit], toUnit);
  }
  if (DEPRECATED_UNITS[toUnit]) {
    console.warn(
      `Unit '${toUnit}' is deprecated - use '${DEPRECATED_UNITS[toUnit]}' instead.`,
    );
    return to(value, fromUnit, DEPRECATED_UNITS[toUnit]);
  }

  // Support aliases for units. This to prepear for sharing this with our customers
  if (UNIT_ALIASES[toUnit]) {
    return to(value, fromUnit, UNIT_ALIASES[toUnit]);
  }
  if (UNIT_ALIASES[fromUnit]) {
    return to(value, UNIT_ALIASES[fromUnit], toUnit);
  }

  // If conversion is not known, see if we have intermediate
  // conversions and use those:
  // E.g. "ft" to "km":
  //   We have "ft" to "m", so first do "ft" to "m", then "m" to "km"
  const int_from = INTERMEDIATE_CONVERSIONS[fromUnit];
  if (int_from) {
    const int_val = to(value, fromUnit, int_from);
    return to(int_val, int_from, toUnit);
  } else {
    // TODO: Verify that this is working, add tests!
    const int_to = INTERMEDIATE_CONVERSIONS[toUnit];
    if (toUnit && int_to && int_to !== toUnit) {
      const int_val = to(value, fromUnit, int_to);
      return to(int_val, int_to, toUnit);
    }
  }

  console.error('no conversions found', value, fromUnit, '->', toUnit);
  throw new Error(
    'No conversions found: ' + value + ' ' + fromUnit + ' -> ' + toUnit,
  );
}

/**
 * Split string into value and unit
 *
 * @param numWithUnit
 * @return string[] where first element it's actual value and second unit
 */
export function split(numWithUnit: string): string[] {
  let m;
  let vu = numWithUnit !== undefined && numWithUnit !== null ? String(numWithUnit) : ''; // ensure it is string as providing undefined or null will crash
  if (charCount(SEPARATOR, vu) > 1) {
    // Remove all SEPARATOR characters except the last one
    m = vu.split(SEPARATOR);
    vu = m.slice(0, -1).join('') + SEPARATOR + m.slice(-1);
  }
  if (vu.indexOf(SEPARATOR) >= 0) {
    m = vu.split(SEPARATOR);
  } else if (SPECIAL_NUMBERS_STRING.includes(vu)) {
    m = [vu, ''];
  } else {
    m = cleanNumStr(vu).match(UNIT_RE);
    if (m) {
      m = m.slice(1);
    }
  }
  if (!m) m = ['0', ''];
  if (m[1] == null) m[1] = '';
  return [m[0], m[1]];
}

/**
 * Get value of the number with unit string ("1|m") will return "1"
 * @param {sting} numWithUnit
 * @returns {string}
 */
export function getValue(numWithUnit: string): string {
  return split(numWithUnit)[0];
}

/**
 * Get unit of the number with unit string ("1|m") will return "m"
 * @param {sting} numWithUnit
 * @returns {string}
 */
export function getUnit(numWithUnit: string): string {
  return split(numWithUnit)[1];
}

/**
 * Convert value with unit to another unit
 *
 * @param numWithUnit value with unit
 * @param toUnit unit to which we are trying to convert
 * @param fromUnit unit from which we are trying to convert
 */
export function unum(numWithUnit: string | number, toUnit: string, fromUnit?: string): number {
  if (numWithUnit == null || numWithUnit === '') {
    return 0;
  }
  if (
    (typeof numWithUnit === 'string' && numWithUnit.startsWith('NaN'))
    || (typeof numWithUnit === 'number' && isNaN(numWithUnit))
  ) {
    return NaN;
  }

  const cleanStr = cleanNumStr(numWithUnit).replaceAll('+', '');
  const m = split(cleanStr);
  if (!m) {
    if (toUnit && fromUnit) return unum(numWithUnit + fromUnit, toUnit);
    else throw new Error('unum: invalid number: ' + numWithUnit);
  }
  if (m[0] == null) m[0] = '0';
  if (m[1]) fromUnit = m[1];
  if (!fromUnit && toUnit !== fromUnit) {
    throw new Error(`unum: unable to figure out unit: ${numWithUnit} fromUnit ${fromUnit}`);
  }

  if (toUnit === fromUnit) {
    const v = m[0] ? toNum(m[0]) : 0;
    if (v === Infinity || v === 'Infinity') {
      return Infinity;
    }
    if (v === -Infinity || v === '-Infinity') {
      return -Infinity;
    }
    if (typeof v === 'string' && EXP_NOTATION_RE.test(v)) {
      return parseFloat(v);
    }
    if (!isNumeric(v) && v !== Infinity && v !== -Infinity) {
      throw new Error('unum: invalid number: ' + v + ', ' + typeof v);
    }
    return cleanNum(v);
  }
  return to(m[0], fromUnit, toUnit);
}

/**
 * Takes user input and returns true if it was input with unit and false in every other case
 *
 * @param {String|Number} value
 * @returns {Boolean}
 */
export function isValueWithUnit(value: string | number): boolean {
  if (!value) {
    return false;
  }
  const splittedValue = String(value).split(SEPARATOR);
  return splittedValue.length === 2 && KNOWN_UNITS.includes(splittedValue[1]);
}

/**
 * Convert value to another unit
 *
 * @param numWithUnit value (with optional unit string)
 * @param toUnit unit to which we are trying to convert
 * @param fromUnit unit from which we are trying to convert
 */
export function convertAndGetValue(numWithUnit: string | number, toUnit: string, fromUnit?: string): number {
  // convertAndGetValue is an alias for "unum" (old name was non-descriptive and confusing)
  return unum(numWithUnit, toUnit, fromUnit);
}

/**
 * Convert value to another unit (preserves types and empty values)
 *
 * @param value value (with optional unit string)
 * @param toUnit unit to which we are trying to convert
 * @param fromUnit unit from which we are trying to convert
 */
export function convertAndGetValueStrict(value: string|number|null, toUnit: string, fromUnit?: string): string|number|null {
  if (value === '' || value === null) {
    return value;
  }
  if (typeof value === 'string' && isValueWithUnit(value) && getValue(value) === '') {
    return '';
  }
  const isString = typeof value === 'string';
  const result = convertAndGetValue(value, toUnit, fromUnit);
  return isString ? String(toString(result)) : result; //preserve input type
}

/**
 * Convert value to the base unit given by the quantity
 *
 * @param value
 * @param quantity
 * @return number
 */
export function toBase(value: string, quantity: string): number {
  const to_unit = unitFromKey(quantity);
  const m = split((value || '').toString());

  return unum(value, to_unit as string, m[1] || to_unit);
}

/**
 * Convert table of values to another unit
 *
 * @param toUnitRow array of units to which we are trying to convert for example: ['ft', 'ppg']
 * @param table array which represent table for example:
 * [['m', 'sg'],
   [100, 1],
   [1000, 1.25],
   [2000, 1.5],
   [3000, 1.1]]
 * @param defaultUnitRow
 * @param removeFinalUnitsRow if true first table row where we define units will be removed
 */
export function convertTable(
  toUnitRow: any,
  table: any,
  defaultUnitRow?: [],
  removeFinalUnitsRow = false,
): string | number[][] {
  // If there is no real data to work with, just return
  // the same stuff that got passed in, whatever it may
  // be.
  if (!table || !table.length || !table[0].length) return table;

  if (!toUnitRow) toUnitRow = table[0];

  // If no default unit row was given, assume the same units
  // as in unitrow.
  if (!defaultUnitRow) defaultUnitRow = toUnitRow;

  // Check if table contain units row
  const firstCell = table[0][0];
  const splittedFirstCell = firstCell && split(`${firstCell}`);
  const unitNum = splittedFirstCell && splittedFirstCell[0] && isNaN(splittedFirstCell[1] as unknown as number);
  const tableHasUnits = !unitNum && isNaN(table[0][0] as unknown as number);

  let ix = tableHasUnits ? 1 : 0;
  // Units to convert from:
  const fromunitrow = tableHasUnits ? table[0] : defaultUnitRow;
  const newTable = [toUnitRow];
  for (; ix < table.length; ix++) {
    const cols = Array(toUnitRow.length);
    const coli = table[ix];
    for (let uix = 0; uix < cols.length; uix++) {
      cols[uix] = toUnitRow[uix] && coli[uix]
        ? unum(coli[uix], toUnitRow[uix], fromunitrow[uix] as string)
        : coli[uix];
    }
    newTable.push(cols);
  }
  if (removeFinalUnitsRow) newTable.shift();
  return newTable;
}

/**
 * Rounds a unit string to N decimal places and appends formatted unit label
 *
 * @param value
 * @param n number of decimal digits
 * @return rounded value with formatted units
 */
export function roundNumberWithLabel(value: string, n = 2): string {
  return displayNumber(
    round(value, n),
    { withUnit: true }
  );
}

/**
 * Get value with unit splitted by | separator
 *
 * @param value
 * @param unit
 * @param defaultVal
 * @return string
 */
export function withUnit(value: string | number | null | undefined, unit: string | null, defaultVal = ''): string {
  if (value === null || value === '' || value === undefined) {
    value = defaultVal;
  }
  if (unit === null) {
    return String(value);
  }
  // eslint-disable-next-line prefer-const
  let [v, u] = String(value).includes(SEPARATOR) ? split(String(value)) : [value, unit];
  if (!u) {
    u = unit;
  }

  return [v, u].join(SEPARATOR);
}

/**
 * Converts to given toUnit and return the converted value with unit.
 * @see roundNumberWithLabel
 *
 * @param numWithUnit value with unit
 * @param toUnit unit to which we are trying to convert
 * @param fromUnit unit from which we are trying to convert
 * @return {string} - "number converted | unit"
 */
export function unumWithUnit(numWithUnit: string | number, toUnit: string, fromUnit?: string): string {
  return withUnit(unum(numWithUnit, toUnit, fromUnit), toUnit);
}

/**
 * Convert value with unit to another unit and display it in pretty format.
 * It will preserv the number of digits in the input or alternativly converting to the given number of digits
 *
 * @param {string|number} numWithUnit - value with unit
 * @param {string} toUnit - unit to which we are trying to convert
 * @param {number} digits - optional number of digits to round the number
 * @returns {string} - converted number including the unit (number|unit)
 */
export function convertSamePrecision(numWithUnit: string | number, toUnit: string, digits?: number): string {
  const validNumWithUnit = String(numWithUnit);
  const m = split(validNumWithUnit);

  const convertedNumber = !m[1] || m[1] == toUnit
    ? Number(m[0])
    : to(m[0], String(m[1]), toUnit);

  let prettyNumber = '0';
  let targetDigits = digits;

  if (convertedNumber !== 0) {
    if (!targetDigits) {
      if (m[1] === toUnit) {
        return validNumWithUnit;
      }
      const regx = String(m[0]).match(/^[-+.,0]*(\d*)[.,]?(\d*)/);
      if (regx) {
        targetDigits = Math.max(3, regx[1].length + regx[2].length);
      } else {
        targetDigits = 3;
      }
    }
    const lim = Math.pow(10, --targetDigits);
    const absNum = Math.abs(convertedNumber);
    if (absNum >= 1e5 * lim || absNum * 1e4 * (1 + lim) < lim) {
      prettyNumber = convertedNumber.toExponential(targetDigits); // yields trailing zero in mantissa
    } else if (absNum >= lim) {
      prettyNumber = convertedNumber.toFixed();
    } else {
      const digs = Math.floor(Math.log10(absNum));
      prettyNumber = convertedNumber.toFixed(targetDigits - digs);

      // Remove insignificant trailing zeroes
      let j = prettyNumber.length;
      if (prettyNumber[--j] == '0') {
        while (prettyNumber[--j] == '0');
        prettyNumber = prettyNumber.slice(0, j + (prettyNumber[j] == '.' ? 0 : 1));
      }
    }
  }
  return withUnit(prettyNumber, toUnit);
}

/**
 * Get list of values, with same precision as the given value, in all the units of the given quantity
 *
 * @param {string} value
 * @param {string} quantity
 * @return {[][string, string, string]}
 */
export function altUnitsList(value: string, quantity: string): (string | undefined)[][] {
  let v = value;
  if (!getUnit(value)) {
    v = withUnit(value, unitFromQuantity(quantity) ?? '');
  }
  const altUnits = ALT_UNITS[quantity] ?? [];
  return altUnits.map((unit: string) => [...split(convertSamePrecision(v, unit)), label(unit)]);
}

/**
 * Validates and cleans raw text numeric user input, typically from UnitInput
 * The previous value is for optionally determining the pre-existing unit
 * The next text is a raw input string
 * The return value is reformatted from the next text (removing invalid patterns)
 *
 * @param {String} previousValue with optional unit e.g. `25|m`
 * @param {String} nextText raw text for next value e.g. `26` (no unit)
 * @returns {String} e.g. `26|m`
 */
export function validateAndClean(previousValue: string, nextText: string): string {
  const unit = split(previousValue)[1];

  // strip all characters except numeric, '.', ',' 'E' or 'e' https://regex101.com/r/TFiYyn/1
  const stripAllExceptNumeric = /[^0-9.,-Ee]/g;
  // strip 'E' or 'e', except the first instance https://regex101.com/r/cTk4vs/1
  const stripE = /^([^(e|E)]*[eE])|[eE]/g;
  // strip invalid leading or trailing 'E' or 'e' https://regex101.com/r/UshUCw/1
  const stripInvalidLeading = /^[E|e]*|[E|e]*$/g;
  // replace all ',' with '.' https://regex101.com/r/jsYRxi/1
  const replaceComma = /,/g;
  // strip all '.', except the first instance https://regex101.com/r/7rZtyM/1
  const stripDot = /^([^.]*\.)|\./g;
  // strip all '.' that are followed by an 'e' https://regex101.com/r/zUWrG2/1
  const stripDotFollowedByE = /\.(?=e)/g;
  // strip all '-', except at beginning, or following an 'e' or 'E' https://regex101.com/r/lWVTDt/1
  const stripMinus = /-|[eE]-/g;

  // the order of these rules matters!
  const cleanedValue = nextText
    .replace(stripAllExceptNumeric, '')
    .replace(stripE, '$1')
    .replace(stripInvalidLeading, '')
    .replace(replaceComma, '.')
    .replace(stripDot, '$1')
    .replace(stripDotFollowedByE, '')
    .replace(stripMinus, (match, offset) => offset === 0 || match.toLowerCase() === 'e-' ? match : '');

  return !Number.isFinite(+cleanedValue)
    ? previousValue
    : `${cleanedValue}${unit ? '|' : ''}${unit}`;
}

/**
 * Takes string included value with units and return display it in pretty format.
 *
 * @param {String} valueWithUnits
 * @returns {String}
 */
export function withPrettyUnitLabel(valueWithUnits: string): string {
  const [val, unit] = split(valueWithUnits);
  const prettyUnit = LABELS[unit] ?? '';
  return `${val} ${prettyUnit}`;
}

export function validateNumber(value: string | number): ValidateReturn {
  let val = value;
  if (typeof value === 'string' && isValueWithUnit(value)) {
    val = getValue(value);
  }

  val = checkAndCleanDecimalComma(val);
  if (isNumeric(val)) {
    const valid = numberSchemaValidator(toNum(val));
    return { valid, errors: transformErrors(numberSchemaValidator.errors) };
  }

  const valid = numberSchemaValidator(val);
  return { valid, errors: transformErrors(numberSchemaValidator.errors) };
}
