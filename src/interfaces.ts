export interface AltUnitWithLabel {
  unit: string | never;
  label: string | undefined;
}

export interface LocaleFormat {
  thousands: string,
  decimal: string,
  useGrouping: boolean
}

export interface ValidateReturn {
  valid: boolean;
  errors: (string|undefined)[] | undefined;
}
