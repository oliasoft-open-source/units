import {getNumberOfDigitsToShow} from '../utils.ts';
import {round} from './rounding.ts';

/**
 * Previously called toPretty in WellDesign
 */
export const roundByToPretty = (num: number) => Math.abs(num) >= 10000
  ? num.toFixed()
  : Number(Number(num).toExponential(3)).toString();

/**
 * Previously called adjustDecimals in WellDesign
 */
export const roundByAdjustDecimals = (val: string|number) => {
  if (typeof val === 'number') {
    if (val >= 100) {
      return Number(round(val, 2));
    } else if (val < 100 && val >= 1) {
      return Number(round(val, 4));
    } else if (val < 1 || val > -1) {
      const valStringifiedArray = val.toString().split('').slice(2);
      const hasEpsilon = valStringifiedArray.find((val) => val === 'e');
      if (hasEpsilon) {
        const exponent = parseInt(
          valStringifiedArray[valStringifiedArray.length - 1],
          10,
        );
        return val.toFixed(exponent + 3);
      } else {
        const valDecimalCount = valStringifiedArray.findIndex(
          (val) => parseInt(val, 10) > 0,
        );
        return Number(round(val, valDecimalCount + 3));
      }
    }
  }
  return val;
};

/**
 * Using getNumberOfDigitsToShow in combination with round
 */
export const roundByGetNumberOfDigitsToShow = (value: number|string) => round(
  value,
  getNumberOfDigitsToShow(value),
);

/**
 * Copied from getNDecimals in WellDesign
 */
export const getNDecimals = (val: number, precision: number, order: number|undefined = undefined) => {
  const exp10 = (order: number|undefined): number => {
    if (order === undefined || isNaN(order)) { return NaN; }
    switch (order) {
      case 0: return 1;
      case 1: return 10;
      case 2: return 100;
      case 3: return 1e3;
      case 4: return 1e4;
      case 5: return 1e5;
      case 6: return 1e6;
      case 7: return 1e7;
      case 8: return 1e8;
      case 9: return 1e9;
      case 10: return 1e10;
      case 11: return 1e11;
      case 12: return 1e12;
      case 13: return 1e13;
      case 14: return 1e14;
      case 15: return 1e15;
      default: return 0;
    }
  };

  if (isNaN(val)) return val;
  if (val === 0
    || ((order === undefined || isNaN(order))
      && (precision === undefined || isNaN(precision)))) { return val; }

  if (order === undefined) { order = exp10(precision); }

  const sign = val / Math.abs(val);
  const absVal = Math.abs(val);

  return sign * (precision >= 0
    ? (absVal < 1 && absVal < Math.pow(10, -precision)
      ? (precision == 0 ? Math.round(absVal) : parseFloat(absVal.toPrecision(precision)))
      : Math.round(absVal * order) / order)
    : absVal);
};
