import {
  round,
  roundNumber,
  roundNumberToPrecision,
  roundNumberToFixedPrecision,
  roundNumberToDecimalPrecision,
  roundToPrecision,
  roundToDecimalPrecision,
  roundNumberByMagnitude,
  roundByMagnitude,
  roundByMagnitudeToFixed,
  roundNumberByRange,
  roundByRange,
  roundToFixed
} from './rounding.ts';

import {
  roundByToPretty,
  roundByAdjustDecimals,
  roundByGetNumberOfDigitsToShow,
  getNDecimals,
} from './deprecated-rounding.ts';

describe('Rounding', () => {
  describe('roundNumber (private)', () => {
    test('rounds values to N decimal places', () => {
      expect(roundNumber(1.234567)).toBe(1.2346);
      expect(roundNumber(1.234567, 3)).toBe(1.235);
      expect(roundNumber(0.00000000123456789)).toBe(0);
      expect(roundNumber(0.00000000123456789, 12)).toBe(1.235e-9);
      expect(roundNumber(1.23456789e-9, 12)).toBe(1.235e-9);
      expect(roundNumber(100_000.123456)).toBe(100000.1235);
      expect(roundNumber(100_000.00000000123456789)).toBe(100000);
      expect(roundNumber(-0.00049)).toBe(-0.0005);
      expect(roundNumber(-100_000.123456)).toBe(-100000.1235);
      /*
        Todo: document or handle floating-point accuracy surprises (but don't worsen performance)?
        https://medium.com/swlh/how-to-round-to-a-certain-number-of-decimal-places-in-javascript-ed74c471c1b8
      */
      //expect(roundNumber(1.005, 2)).toBe(1.01);
      expect(roundNumber(1.005, 2)).toBe(1); //not ideal
    });
  });
  describe('roundNumberToPrecision (private)', () => {
    test('rounds a number to N significant digits', () => {
      expect(roundNumberToPrecision(0.00000000000456789)).toBe(4.568e-12);
      expect(roundNumberToPrecision(0.0000456789)).toBe(0.00004568);
      expect(roundNumberToPrecision(0.0123456789)).toBe(0.01235);
      expect(roundNumberToPrecision(1)).toBe(1);
      expect(roundNumberToPrecision(10)).toBe(10);
      expect(roundNumberToPrecision(-10)).toBe(-10);
      expect(roundNumberToPrecision(10.01)).toBe(10.01);
      expect(roundNumberToPrecision(12.12345678)).toBe(12.12);
      expect(roundNumberToPrecision(123.12345678)).toBe(123.1);
      expect(roundNumberToPrecision(12_345.12345678)).toBe(12350);
      expect(roundNumberToPrecision(1.234567891010111213e21)).toBe(1.235e+21);
      //configure N showSignificantDigits
      expect(roundNumberToPrecision(0.0000456789, 2)).toBe(0.000046);
      expect(roundNumberToPrecision(12_345.12345678, 2)).toBe(12000);
    });
  });
  describe('roundNumberToFixedPrecision (private)', () => {
    test('rounds a number to N significant digits, preserving trailing decimals zeros', () => {
      expect(roundNumberToFixedPrecision(0.00045123456)).toBe('0.0004512');
      expect(roundNumberToFixedPrecision(0.0004500000)).toBe('0.0004500');
      expect(roundNumberToFixedPrecision(0.0004500001)).toBe('0.0004500');
      expect(roundNumberToFixedPrecision(4.500001e-4)).toBe('0.0004500');
      expect(roundNumberToFixedPrecision(0.00000456012)).toBe('0.000004560');
      expect(roundNumberToFixedPrecision(0.000000456012)).toBe('4.560e-7');
      expect(roundNumberToFixedPrecision(0.00000000000450000)).toBe('4.500e-12');
      expect(roundNumberToFixedPrecision(1230)).toBe('1230');
      expect(roundNumberToFixedPrecision(12321)).toBe('1.232e+4');
      expect(roundNumberToFixedPrecision(1230000)).toBe('1.230e+6');
      expect(roundNumberToFixedPrecision(123412.00000456012)).toBe('1.234e+5');
    });
  });
  describe('roundNumberToDecimalPrecision (private)', () => {
    test('can be configured to exclude the integer part', () => {
      expect(roundNumberToDecimalPrecision(12345.0012345678)).toBe(12345.001235);
      expect(roundNumberToDecimalPrecision(67891234)).toBe(67891234);
      expect(roundNumberToDecimalPrecision(1.23450012345678e+4)).toBe(12345.001235);
      expect(roundNumberToDecimalPrecision(3.456315667346e6)).toBe(3456315.6673);
      expect(roundNumberToDecimalPrecision(-12345.12345678)).toBe(-12345.1235);
      expect(roundNumberToDecimalPrecision(-3.456315667346e6)).toBe(-3456315.6673);
      expect(roundNumberToDecimalPrecision(9.2345453325e-3)).toBe(0.009235);
      expect(roundNumberToDecimalPrecision(1.23450012345678e+4)).toBe(12345.001235);
      expect(roundNumberToDecimalPrecision(1.234567891010111213e21)).toBe(1.2345678910101111e+21);
    });
  });
  describe('roundNumberByMagnitude (private)', () => {
    test('rounds a number based on its magnitude', () => {
      expect(roundNumberByMagnitude(123_456.789)).toBe(123457);
      expect(roundNumberByMagnitude(100.789)).toBe(100.8);
      expect(roundNumberByMagnitude(7.12345678)).toBe(7.123);
      expect(roundNumberByMagnitude(0.0000456789)).toBe(0.00004568);
      //test cases from deprecated roundByToPretty (show equivalence)
      expect(roundNumberByMagnitude(1.345678910111213e-21)).toBe(1.346e-21);
      expect(roundNumberByMagnitude(0.0000000000000123456789)).toBe(1.235e-14);
      expect(roundNumberByMagnitude(0.00000000123456789)).toBe(1.235e-9);
      expect(roundNumberByMagnitude(0.0000123456789)).toBe(0.00001235);
      expect(roundNumberByMagnitude(0.000123456789)).toBe(0.0001235);
      expect(roundNumberByMagnitude(0.000123756789)).toBe(0.0001238);
      expect(roundNumberByMagnitude(0.00123456789)).toBe(0.001235);
      expect(roundNumberByMagnitude(0.0123456789)).toBe(0.01235);
      expect(roundNumberByMagnitude(0.123456789)).toBe(0.1235);
      expect(roundNumberByMagnitude(1.123456789)).toBe(1.123);
      expect(roundNumberByMagnitude(10.123456789)).toBe(10.12);
      expect(roundNumberByMagnitude(100.123456789)).toBe(100.1);
      expect(roundNumberByMagnitude(1000.123456789)).toBe(1000);
      expect(roundNumberByMagnitude(10000.123456789)).toBe(10000);
      expect(roundNumberByMagnitude(9999.123456789)).toBe(9999);
      expect(roundNumberByMagnitude(1000)).toBe(1000);
      expect(roundNumberByMagnitude(1.234567891010111213e21)).toBe(1.2345678910101111e+21);
      expect(roundNumberByMagnitude(123e21)).toBe(1.23e+23);
      //examples of configurable options
      expect(roundNumberByMagnitude(0.123456789, 2)).toBe(0.12);
      expect(roundNumberByMagnitude(1.123456789, 2)).toBe(1.1);
      expect(roundNumberByMagnitude(12.123456789, 2)).toBe(12);
    });
    test('Never rounds the integer part (OW-12391)', () => {
      //show n significant digits:
      expect(roundNumberByMagnitude(1234.123456789)).toBe(1234);
      expect(roundNumberByMagnitude(1234.123456789, 6)).toBe(1234.12);
      expect(roundNumberByMagnitude(1234.123456789, 5)).toBe(1234.1);
      //expect the integer part to never be rounded:
      expect(roundNumberByMagnitude(1234.123456789, 4)).toBe(1234);
      expect(roundNumberByMagnitude(1234.123456789, 3)).toBe(1234);
      expect(roundNumberByMagnitude(1234.123456789, 2)).toBe(1234);
      expect(roundNumberByMagnitude(1234.123456789, 1)).toBe(1234);

      //show n significant digits:
      expect(roundNumberByMagnitude(12.123456789)).toBe(12.12);
      expect(roundNumberByMagnitude(12.123456789, 6)).toBe(12.1235);
      expect(roundNumberByMagnitude(12.123456789, 5)).toBe(12.123);
      expect(roundNumberByMagnitude(12.123456789, 4)).toBe(12.12);
      expect(roundNumberByMagnitude(12.123456789, 3)).toBe(12.1);
      //expect the integer part to never be rounded:
      expect(roundNumberByMagnitude(12.123456789, 2)).toBe(12);
      expect(roundNumberByMagnitude(12.123456789, 1)).toBe(12);
    });
    test('Compare old and new implementations for equivalence', () => {
      /*
        This test is just to prove "equivalence" of two implementations
          - the old one comes from the deprecated `toPretty` function
          - the new one is the replacement in `roundByMagnitude`
      */
      const n = 4;
      const oldImplementation = (value: number) => (Number(value.toExponential(n - 1).toString()));
      const newImplementation = (value: number) => Number(value.toPrecision(n));

      expect(oldImplementation(0.0012345)).toBe(0.001234);
      expect(newImplementation(0.0012345)).toBe(0.001234);

      expect(oldImplementation(0.12345)).toBe(0.1235);
      expect(newImplementation(0.12345)).toBe(0.1235);

      expect(oldImplementation(1.12345)).toBe(1.123);
      expect(newImplementation(1.12345)).toBe(1.123);

      expect(oldImplementation(12.12345)).toBe(12.12);
      expect(newImplementation(12.12345)).toBe(12.12);

      expect(oldImplementation(1212.12345)).toBe(1212);
      expect(newImplementation(1212.12345)).toBe(1212);

      //note: the full implementations have extra logic to *not* round the integer part:
      expect(oldImplementation(12126.12345)).toBe(12130);
      expect(newImplementation(12126.12345)).toBe(12130);

      expect(oldImplementation(1212678.12345)).toBe(1213000);
      expect(newImplementation(1212678.12345)).toBe(1213000);

      expect(oldImplementation(12.123456789)).toBe(12.12);
      expect(newImplementation(12.123456789)).toBe(12.12);
    });
  });
  describe('roundNumberByRange (private)', () => {
    test('shows more significant digits when the range is small and close to the value', () => {
      expect(roundNumberByRange(1.123456789, 1.2345678, 1.2345679)).toBe(1.1234568);
      expect(roundNumberByRange(1.123456789, 1.2346, 1.2347)).toBe(1.12346);
      expect(roundNumberByRange(1.123456789, 0, 10)).toBe(1.1235);
      expect(roundNumberByRange(1.123456789, -1e5, 1e5)).toBe(1.1235);
      expect(roundNumberByRange(0.123456789, 0.1, 0.2)).toBe(0.1235);
      expect(roundNumberByRange(0.0123456789, 0.01, 0.02)).toBe(0.0123);
      expect(roundNumberByRange(0.0123456789, 0.0123457, 0.0123458)).toBe(0.0123457);
      expect(roundNumberByRange(0.00123456789, 0.001, 0.002)).toBe(0.0012);
      expect(roundNumberByRange(0.00123456789, 0.00123457, 0.00123458)).toBe(0.00123457);
      expect(roundNumberByRange(0.000123456789, 0.0001, 0.0002)).toBe(0.0001);
      expect(roundNumberByRange(0.000123456789, 0.000123457, 0.000123458)).toBe(0.000123457);
      expect(roundNumberByRange(0.0000123456789, 0.00001, 0.00002)).toBe(0.00001);
      expect(roundNumberByRange(0.0000123456789, 0.0000123457, 0.0000123458)).toBe(0.0000123457);
      expect(roundNumberByRange(0.0000123456789, 0.000123457, 0.000123458)).toBe(0.000012346);
      expect(roundNumberByRange(0.0000123456789, 0.00123457, 0.00123458)).toBe(0.00001235);
      expect(roundNumberByRange(0.0000123456789, 0.0123457, 0.0123458)).toBe(0.0000123);
      expect(roundNumberByRange(0.0000123456789, 0.123457, 0.123458)).toBe(0.000012);
      expect(roundNumberByRange(0.0000123456789, 1, 2)).toBe(0);

      // can configure the minimum decimal significant digits (default 4)
      expect(roundNumberByRange(1.123456789, -1e5, 1e5, 0)).toBe(1);
      expect(roundNumberByRange(1.123456789, -1e5, 1e5, 1)).toBe(1.1);
      expect(roundNumberByRange(1.123456789, -1e5, 1e5, 2)).toBe(1.12);
      expect(roundNumberByRange(1.123456789, -1e5, 1e5, 3)).toBe(1.123);
    });
    test('handles edge-cases', () => {
      // value, max, min identical -> return original value
      expect(roundNumberByRange(1.2345678, 1.2345678, 1.2345678)).toBe(1.2345678);
      expect(roundNumberByRange(0.300123456789, 0.3, (0.1 + 0.2))).toBe(0.300123456789);
      expect(roundNumberByRange(0.3, (0.1 + 0.2), 0.3)).toBe(0.3);
      // max < min
      expect(roundNumberByRange(1.23456789, 10, 0)).toBe(1.23456789);
      expect(roundNumberByRange(0.300123456789, (0.1 + 0.2), 0.3)).toBe(0.300123456789);
    });
  });
  describe('round', () => {
    test('rounds values to N decimal places', () => {
      expect(round(1.234567)).toBe(1.2346);
      expect(round(1.234567, 3)).toBe(1.235);
    });
    test('does not append trailing 0s', () => {
      expect(round(1, 3)).toBe(1);
      expect(round(1.1, 3)).toBe(1.1);
    });
    test('handles stringified numbers', () => {
      expect(round('1.234567', 3)).toBe('1.235');
      expect(round('1.', 3)).toBe('1.');
      expect(round('1,', 3)).toBe('1,');
    });
    test('handles comma decimals', () => {
      expect(round('1,234567', 3)).toBe('1.235');
    });
    test('handles numbers with units', () => {
      expect(round('1.234567|m', 3)).toBe('1.235|m');
      expect(round('1.234567|kg/m3', 3)).toBe('1.235|kg/m3');
      //Watch out! Units not defined in ALT_UNITS will *not* round (due to how isValueWithUnit() works)
      expect(round('1.234567|badUnit/moo', 3)).toBe('1.234567|badUnit/moo');
    });
    test('handles fractions', () => {
      expect(round('1/3', 3)).toBe('0.333');
      expect(round('1/3|m', 3)).toBe('0.333|m');
    });
    test('handles scientific notation', () => {
      expect(round('1.1e-2', 3)).toBe('0.011');
      expect(round('1.1e-2|m', 3)).toBe('0.011|m');
      expect(round('1.1e-9', 12)).toBe('1.1e-9');
      expect(round('1.1e-9|m', 12)).toBe('1.1e-9|m');
    });
    test('handles bad / impossible inputs by returning input', () => {
      expect(round(null, 2)).toBe(null);
      expect(round(NaN, 2)).toBe(NaN);
      expect(round(undefined, 2)).toBe(undefined);
      expect(round(Infinity, 2)).toBe(Infinity);
      expect(round(-Infinity, 2)).toBe(-Infinity);
      expect(round('', 2)).toBe('');
      expect(round('foobar', 2)).toBe('foobar');
      expect(round('|m', 2)).toBe('|m');
    });
    test('handles small numbers OW-17645', () => {
      expect(round(0.0000002, 9)).toBe(0.0000002);
      expect(round('0.0000002', 9)).toBe('0.0000002');
      expect(round('0.0000002|m', 9)).toBe('0.0000002|m');
    });
  });
  describe('roundToPrecision', () => {
    test('rounds a number to N significant digits', () => {
      expect(roundToPrecision(0.0000456789)).toBe(0.00004568);
      expect(roundToPrecision('0.0000456789')).toBe('0.00004568');
      expect(roundToPrecision('0.0000456789|m')).toBe('0.00004568|m');
    });
    test('handles bad / impossible inputs by returning input', () => {
      expect(roundToPrecision(null)).toBe(null);
      expect(roundToPrecision(NaN)).toBe(NaN);
      expect(roundToPrecision(undefined)).toBe(undefined);
      expect(roundToPrecision(Infinity)).toBe(Infinity);
      expect(roundToPrecision(-Infinity)).toBe(-Infinity);
      expect(roundToPrecision('')).toBe('');
      expect(roundToPrecision('foobar')).toBe('foobar');
      expect(roundToPrecision('|m')).toBe('|m');
    });
    test('handles small numbers OW-17645', () => {
      expect(roundToPrecision(0.0000002)).toBe(0.0000002);
      expect(roundToPrecision('0.0000002')).toBe('0.0000002');
      expect(roundToPrecision('0.0000002|m')).toBe('0.0000002|m');
    });
  });
  describe('roundToDecimalPrecision', () => {
    test('rounds to N significant digits, not including the integer part', () => {
      expect(roundToDecimalPrecision(0.00000000000456789)).toBe(4.568e-12);
      expect(roundToDecimalPrecision(0.0000456789)).toBe(0.00004568);
      expect(roundToDecimalPrecision(0.0123456789)).toBe(0.01235);
      expect(roundToDecimalPrecision(-0.0123456789)).toBe(-0.01235);
      expect(roundToDecimalPrecision(1)).toBe(1);
      expect(roundToDecimalPrecision(10)).toBe(10);
      expect(roundToDecimalPrecision(-10)).toBe(-10);
      expect(roundToDecimalPrecision(10.01)).toBe(10.01);
      expect(roundToDecimalPrecision(12.12345678)).toBe(12.1235);
      expect(roundToDecimalPrecision(123.12345678)).toBe(123.1235);
      expect(roundToDecimalPrecision(12_345.12345678)).toBe(12345.1235);
      //configure N showSignificantDigits
      expect(roundToDecimalPrecision(0.0000456789, 2)).toBe(0.000046);
      expect(roundToDecimalPrecision(12_345.12345678, 2)).toBe(12345.12);
    });
    test('bug', () => {
      //todo: bug
      expect(roundToDecimalPrecision(12.00012345678)).toBe(12.0001235);
    });
    test('handles bad / impossible inputs by returning input', () => {
      expect(roundToDecimalPrecision(null)).toBe(null);
      expect(roundToDecimalPrecision(NaN)).toBe(NaN);
      expect(roundToDecimalPrecision(undefined)).toBe(undefined);
      expect(roundToDecimalPrecision(Infinity)).toBe(Infinity);
      expect(roundToDecimalPrecision(-Infinity)).toBe(-Infinity);
      expect(roundToDecimalPrecision('')).toBe('');
      expect(roundToDecimalPrecision('foobar')).toBe('foobar');
      expect(roundToDecimalPrecision('|m')).toBe('|m');
    });
    test('handles small numbers OW-17645', () => {
      expect(roundToDecimalPrecision(0.0000002)).toBe(0.0000002);
      expect(roundToDecimalPrecision('0.0000002')).toBe('0.0000002');
      expect(roundToDecimalPrecision('0.0000002|m')).toBe('0.0000002|m');
    });
  });
  describe('roundByMagnitude', () => {
    test('rounds a number based on its magnitude', () => {
      expect(roundByMagnitude(1.234567)).toBe(1.235);
      expect(roundByMagnitude('1.234567')).toBe('1.235');
      expect(roundByMagnitude('1.234567|m')).toBe('1.235|m');
    });
    test('handles bad / impossible inputs by returning input', () => {
      expect(roundByMagnitude(null)).toBe(null);
      expect(roundByMagnitude(NaN)).toBe(NaN);
      expect(roundByMagnitude(undefined)).toBe(undefined);
      expect(roundByMagnitude(Infinity)).toBe(Infinity);
      expect(roundByMagnitude(-Infinity)).toBe(-Infinity);
      expect(roundByMagnitude('')).toBe('');
      expect(roundByMagnitude('foobar')).toBe('foobar');
      expect(roundByMagnitude('|m')).toBe('|m');
    });
    test('handles small numbers OW-17645', () => {
      expect(roundByMagnitude(0.0000002)).toBe(0.0000002);
      expect(roundByMagnitude('0.0000002')).toBe('0.0000002');
      expect(roundByMagnitude('0.0000002|m')).toBe('0.0000002|m');
    });
  });
  describe('roundByMagnitudeToFixed', () => {
    test('test cases from Ida', () => {
      expect(roundByMagnitudeToFixed(0.131214)).toBe('0.1312');
      expect(roundByMagnitudeToFixed(0.13101)).toBe('0.1310');
      expect(roundByMagnitudeToFixed(0.000010021)).toBe('0.00001002');
      expect(roundByMagnitudeToFixed(0.000010000)).toBe('0.00001000');
      expect(roundByMagnitudeToFixed(1.1231234)).toBe('1.123');
      expect(roundByMagnitudeToFixed(1.120021)).toBe('1.120');
      expect(roundByMagnitudeToFixed(1.0001)).toBe('1.000');
      expect(roundByMagnitudeToFixed(12.12345)).toBe('12.12');
      expect(roundByMagnitudeToFixed(12.012)).toBe('12.01');
      expect(roundByMagnitudeToFixed(12.10000)).toBe('12.10');
      expect(roundByMagnitudeToFixed(123.109)).toBe('123.1');
      expect(roundByMagnitudeToFixed(123.011)).toBe('123.0');
      expect(roundByMagnitudeToFixed(1233.12)).toBe('1233');
      expect(roundByMagnitudeToFixed(1233.00)).toBe('1233');
      expect(roundByMagnitudeToFixed(1230)).toBe('1230');
    });
    test('compare with roundByMagnitude', () => {
      expect(roundByMagnitudeToFixed(123_456.789)).toBe('123457');
      expect(roundByMagnitudeToFixed(100.789)).toBe('100.8');
      expect(roundByMagnitudeToFixed(7.12345678)).toBe('7.123');
      expect(roundByMagnitudeToFixed(0.0000456789)).toBe('0.00004568');
      expect(roundByMagnitudeToFixed(1.345678910111213e-21)).toBe(String(1.346e-21));
      expect(roundByMagnitudeToFixed(0.0000000000000123456789)).toBe(String(1.235e-14));
      expect(roundByMagnitudeToFixed(0.00000000123456789)).toBe(String(1.235e-9));
      expect(roundByMagnitudeToFixed(0.0000123456789)).toBe('0.00001235');
      expect(roundByMagnitudeToFixed(0.000123456789)).toBe('0.0001235');
      expect(roundByMagnitudeToFixed(0.000123756789)).toBe('0.0001238');
      expect(roundByMagnitudeToFixed(0.00123456789)).toBe('0.001235');
      expect(roundByMagnitudeToFixed(0.0123456789)).toBe('0.01235');
      expect(roundByMagnitudeToFixed(0.123456789)).toBe('0.1235');
      expect(roundByMagnitudeToFixed(1.123456789)).toBe('1.123');
      expect(roundByMagnitudeToFixed(10.123456789)).toBe('10.12');
      expect(roundByMagnitudeToFixed(100.123456789)).toBe('100.1');
      expect(roundByMagnitudeToFixed(1000.123456789)).toBe('1000');
      expect(roundByMagnitudeToFixed(10000.123456789)).toBe('10000');
      expect(roundByMagnitudeToFixed(9999.123456789)).toBe('9999');
      expect(roundByMagnitudeToFixed(1.234567891010111213e21)).toBe(String(1.2345678910101111e+21));
      expect(roundByMagnitudeToFixed(123e21)).toBe(String(1.23e+23));
      //examples of configurable options
      expect(roundByMagnitudeToFixed(0.123456789, 2)).toBe('0.12');
      expect(roundByMagnitudeToFixed(1.123456789, 2)).toBe('1.1');
      expect(roundByMagnitudeToFixed(12.123456789, 2)).toBe('12');
      //show n significant digits:
      expect(roundByMagnitudeToFixed(1234.123456789)).toBe('1234');
      expect(roundByMagnitudeToFixed(1234.123456789, 6)).toBe('1234.12');
      expect(roundByMagnitudeToFixed(1234.123456789, 5)).toBe('1234.1');
      //expect the integer part to never be rounded:
      expect(roundByMagnitudeToFixed(1234.123456789, 4)).toBe('1234');
      expect(roundByMagnitudeToFixed(1234.123456789, 3)).toBe('1234');
      expect(roundByMagnitudeToFixed(1234.123456789, 2)).toBe('1234');
      expect(roundByMagnitudeToFixed(1234.123456789, 1)).toBe('1234');
      //show n significant digits:
      expect(roundByMagnitudeToFixed(12.123456789)).toBe('12.12');
      expect(roundByMagnitudeToFixed(12.123456789, 6)).toBe('12.1235');
      expect(roundByMagnitudeToFixed(12.123456789, 5)).toBe('12.123');
      expect(roundByMagnitudeToFixed(12.123456789, 4)).toBe('12.12');
      expect(roundByMagnitudeToFixed(12.123456789, 3)).toBe('12.1');
      //expect the integer part to never be rounded:
      expect(roundByMagnitudeToFixed(12.123456789, 2)).toBe('12');
      expect(roundByMagnitudeToFixed(12.123456789, 1)).toBe('12');
    });
    test('handles different input formats', () => {
      expect(roundByMagnitudeToFixed(1.230067)).toBe('1.230');
      expect(roundByMagnitudeToFixed('1.230067')).toBe('1.230');
      expect(roundByMagnitudeToFixed('1,230067')).toBe('1.230');
      expect(roundByMagnitudeToFixed('12345,230067')).toBe('12345');
      expect(roundByMagnitudeToFixed('1.230067|m')).toBe('1.230|m');
      expect(roundByMagnitudeToFixed(1.230067e-5)).toBe('0.00001230');
      expect(roundByMagnitudeToFixed(1.230067e+3)).toBe('1230');
      expect(roundByMagnitudeToFixed('1.230067e-5|ft')).toBe('0.00001230|ft');
      expect(roundByMagnitudeToFixed('1.230067e+3|ft')).toBe('1230|ft');
    });
    test('extra test cases (range of values)', () => {
      expect(roundByMagnitudeToFixed(12345)).toBe('12345');
      expect(roundByMagnitudeToFixed(12345.12300)).toBe('12345');
      expect(roundByMagnitudeToFixed(12345.000)).toBe('12345');
      expect(roundByMagnitudeToFixed(1200)).toBe('1200');
      expect(roundByMagnitudeToFixed(1200.0)).toBe('1200');
      expect(roundByMagnitudeToFixed(1200.0001)).toBe('1200');
      expect(roundByMagnitudeToFixed(12)).toBe('12.00');
      expect(roundByMagnitudeToFixed(12)).toBe('12.00');
      expect(roundByMagnitudeToFixed(12.0)).toBe('12.00');
      expect(roundByMagnitudeToFixed(12.00001)).toBe('12.00');
      expect(roundByMagnitudeToFixed(1.0)).toBe('1.000');
      expect(roundByMagnitudeToFixed(0.1)).toBe('0.1000');
      expect(roundByMagnitudeToFixed(0.0100001)).toBe('0.01000');
      expect(roundByMagnitudeToFixed('12345')).toBe('12345');
      expect(roundByMagnitudeToFixed('12345.12300')).toBe('12345');
      expect(roundByMagnitudeToFixed('12345.000')).toBe('12345');
      expect(roundByMagnitudeToFixed('1200')).toBe('1200');
      expect(roundByMagnitudeToFixed('1200.0')).toBe('1200');
      expect(roundByMagnitudeToFixed('1200.0001')).toBe('1200');
      expect(roundByMagnitudeToFixed('12')).toBe('12.00');
      expect(roundByMagnitudeToFixed('12')).toBe('12.00');
      expect(roundByMagnitudeToFixed('12.0')).toBe('12.00');
      expect(roundByMagnitudeToFixed('12.00001')).toBe('12.00');
      expect(roundByMagnitudeToFixed('1.0')).toBe('1.000');
      expect(roundByMagnitudeToFixed('0.1')).toBe('0.1000');
      expect(roundByMagnitudeToFixed('0.0100001')).toBe('0.01000');
      expect(roundByMagnitudeToFixed('12345|kg/s')).toBe('12345|kg/s');
      expect(roundByMagnitudeToFixed('12345.12300|kg/s')).toBe('12345|kg/s');
      expect(roundByMagnitudeToFixed('12345.000|kg/s')).toBe('12345|kg/s');
      expect(roundByMagnitudeToFixed('1200|kg/s')).toBe('1200|kg/s');
      expect(roundByMagnitudeToFixed('1200.0|kg/s')).toBe('1200|kg/s');
      expect(roundByMagnitudeToFixed('1200.0001|kg/s')).toBe('1200|kg/s');
      expect(roundByMagnitudeToFixed('12|kg/s')).toBe('12.00|kg/s');
      expect(roundByMagnitudeToFixed('12|kg/s')).toBe('12.00|kg/s');
      expect(roundByMagnitudeToFixed('12.0|kg/s')).toBe('12.00|kg/s');
      expect(roundByMagnitudeToFixed('12.00001|kg/s')).toBe('12.00|kg/s');
      expect(roundByMagnitudeToFixed('1.0|kg/s')).toBe('1.000|kg/s');
      expect(roundByMagnitudeToFixed('0.1|kg/s')).toBe('0.1000|kg/s');
      expect(roundByMagnitudeToFixed('0.0100001|kg/s')).toBe('0.01000|kg/s');
    });
    test('handles bad / impossible inputs by returning input', () => {
      expect(roundByMagnitudeToFixed(null)).toBe(null);
      expect(roundByMagnitudeToFixed(NaN)).toBe(NaN);
      expect(roundByMagnitudeToFixed(undefined)).toBe(undefined);
      expect(roundByMagnitudeToFixed(Infinity)).toBe(Infinity);
      expect(roundByMagnitudeToFixed(-Infinity)).toBe(-Infinity);
      expect(roundByMagnitudeToFixed('')).toBe('');
      expect(roundByMagnitudeToFixed('foobar')).toBe('foobar');
      expect(roundByMagnitudeToFixed('|m')).toBe('|m');
    });
  });
  describe('roundByRange', () => {
    test('rounds a number based on size in a range', () => {
      expect(roundByRange(1.23456789, 0, 10)).toBe(1.2346);
      expect(roundByRange('1.23456789', 0, 10)).toBe('1.2346');
      expect(roundByRange('1.23456789|m', 0, 10)).toBe('1.2346|m');
    });
    test('handles bad / impossible inputs by returning input', () => {
      expect(roundByRange(null, 0, 10)).toBe(null);
      expect(roundByRange(NaN, 0, 10)).toBe(NaN);
      expect(roundByRange(undefined, 0, 10)).toBe(undefined);
      expect(roundByRange(Infinity, 0, 10)).toBe(Infinity);
      expect(roundByRange(-Infinity, 0, 10)).toBe(-Infinity);
      expect(roundByRange('', 0, 10)).toBe('');
      expect(roundByRange('foobar', 0, 10)).toBe('foobar');
      expect(roundByRange('|m', 0, 10)).toBe('|m');
    });
    test('handles small numbers OW-17645', () => {
      expect(roundByRange(0.0000002, 0, 0.0000003)).toBe(0.0000002);
      expect(roundByRange('0.0000002', 0, 0.0000003)).toBe('0.0000002');
      expect(roundByRange('0.0000002|m', 0, 0.0000003)).toBe('0.0000002|m');
    });
  });
  describe('roundToFixed', () => {
    test('rounds numeric values to fixed precision', () => {
      expect(roundToFixed(1.1)).toBe('1.1000');
      expect(roundToFixed(12345678.12345678)).toBe('12345678.1235');
      expect(roundToFixed(1.12456e3)).toBe('1124.5600');
      expect(roundToFixed(0.016, 3)).toBe('0.016');
      expect(roundToFixed(0.016, 2)).toBe('0.02');
      expect(roundToFixed(0.016, 1)).toBe('0.0');
      expect(roundToFixed('1.12456e3')).toBe('1124.5600');
      expect(roundToFixed('1.12456e+3')).toBe('1124.5600');
      expect(roundToFixed('1.12456e-3')).toBe('0.0011');
      expect(roundToFixed('1.1')).toBe('1.1000');
      expect(roundToFixed('1.1|m')).toBe('1.1000|m');
    });
    test('handles bad / impossible inputs by returning input', () => {
      expect(roundToFixed(null)).toBe(null);
      expect(roundToFixed(NaN)).toBe(NaN);
      expect(roundToFixed(undefined)).toBe(undefined);
      expect(roundToFixed(Infinity)).toBe(Infinity);
      expect(roundToFixed(-Infinity)).toBe(-Infinity);
      expect(roundToFixed('')).toBe('');
      expect(roundToFixed('foobar')).toBe('foobar');
      expect(roundToFixed('|m')).toBe('|m');
    });
    test('handles small numbers OW-17645', () => {
      expect(roundToFixed(0.0000002, 9)).toBe('0.000000200');
      expect(roundToFixed('0.0000002', 9)).toBe('0.000000200');
      expect(roundToFixed('0.0000002|m', 9)).toBe('0.000000200|m');
    });
  });
});

describe('Deprecated rounding (only for comparing pre-existing implementations)', () => {
  describe('roundByToPretty', () => {
    test('rounds numbers based on the magnitude', () => {
      /*
        - rounds and preserves decimal places for small numbers
        - shows fewer decimal places for larger numbers (large numbers have no decimal places)
        - simplest implementation
       */
      expect(roundByToPretty(1.345678910111213e-21)).toBe('1.346e-21');
      expect(roundByToPretty(0.0000000000000123456789)).toBe('1.235e-14');
      expect(roundByToPretty(0.00000000123456789)).toBe('1.235e-9');
      expect(roundByToPretty(0.0000123456789)).toBe('0.00001235');
      expect(roundByToPretty(0.000123456789)).toBe('0.0001235');
      expect(roundByToPretty(0.000123756789)).toBe('0.0001238');
      expect(roundByToPretty(0.00123456789)).toBe('0.001235');
      expect(roundByToPretty(0.0123456789)).toBe('0.01235');
      expect(roundByToPretty(0.123456789)).toBe('0.1235');
      expect(roundByToPretty(1.123456789)).toBe('1.123');
      expect(roundByToPretty(10.123456789)).toBe('10.12');
      expect(roundByToPretty(100.123456789)).toBe('100.1');
      expect(roundByToPretty(1000.123456789)).toBe('1000');
      expect(roundByToPretty(10000.123456789)).toBe('10000');
      expect(roundByToPretty(9999.123456789)).toBe('9999');
      expect(roundByToPretty(1000)).toBe('1000');
      expect(roundByToPretty(1.234567891010111213e21)).toBe('1.2345678910101111e+21');
      expect(roundByToPretty(123e21)).toBe('1.23e+23');
      expect(roundByToPretty(12.123456789)).toBe('12.12');
      expect(roundByToPretty(8999.123456789)).toBe('8999');
    });
  });
  describe('roundByAdjustDecimals', () => {
    test('rounds numbers based on the magnitude', () => {
      /*
        - returns zero for very small numbers
        - inconsistent return types
        - shows fewer decimal places for larger numbers (but logic seems unclear)
      */
      expect(roundByAdjustDecimals(1.345678910111213e-21)).toBe('0.0000');
      expect(roundByAdjustDecimals(0.0000000000000123456789)).toBe('0.0000000');
      expect(roundByAdjustDecimals(0.00000000123456789)).toBe('0.000000001235');
      expect(roundByAdjustDecimals(0.0000123456789)).toBe(0.0000123);
      expect(roundByAdjustDecimals(0.000123456789)).toBe(0.000123);
      expect(roundByAdjustDecimals(0.000123756789)).toBe(0.000124);
      expect(roundByAdjustDecimals(0.00123456789)).toBe(0.00123);
      expect(roundByAdjustDecimals(0.0123456789)).toBe(0.0123);
      expect(roundByAdjustDecimals(0.123456789)).toBe(0.123);
      expect(roundByAdjustDecimals(1.123456789)).toBe(1.1235);
      expect(roundByAdjustDecimals(10.123456789)).toBe(10.1235);
      expect(roundByAdjustDecimals(100.123456789)).toBe(100.12);
      expect(roundByAdjustDecimals(1000.123456789)).toBe(1000.12);
      expect(roundByAdjustDecimals(10000.123456789)).toBe(10000.12);
      expect(roundByAdjustDecimals(1000)).toBe(1000);
      expect(roundByAdjustDecimals(1.234567891010111213e21)).toBe(1.2345678910101111e+21);
      expect(roundByAdjustDecimals(123e21)).toBe(1.23e+23);
    });
  });
  describe('roundByGetNumberOfDigitsToShow', () => {
    test('rounds numbers based on the magnitude', () => {
      /*
        - returns zero for very small numbers
        - preserves 4 decimal places even for large numbers
      */
      expect(roundByGetNumberOfDigitsToShow(1.345678910111213e-21)).toBe(0);
      expect(roundByGetNumberOfDigitsToShow(1.345678910111213e-4)).toBe(0.0001346);
      expect(roundByGetNumberOfDigitsToShow(0.0000000000000123456789)).toBe(1.235e-14);
      expect(roundByGetNumberOfDigitsToShow(0.00000000123456789)).toBe(1.235e-9);
      expect(roundByGetNumberOfDigitsToShow(0.0000123456789)).toBe(0.00001235);
      expect(roundByGetNumberOfDigitsToShow(0.000123456789)).toBe(0.0001235);
      expect(roundByGetNumberOfDigitsToShow(0.000123756789)).toBe(0.0001238);
      expect(roundByGetNumberOfDigitsToShow(0.00123456789)).toBe(0.001235);
      expect(roundByGetNumberOfDigitsToShow(0.0123456789)).toBe(0.01235);
      expect(roundByGetNumberOfDigitsToShow(0.123456789)).toBe(0.1235);
      expect(roundByGetNumberOfDigitsToShow(1.123456789)).toBe(1.1235);
      expect(roundByGetNumberOfDigitsToShow(10.123456789)).toBe(10.1235);
      expect(roundByGetNumberOfDigitsToShow(100.123456789)).toBe(100.1235);
      expect(roundByGetNumberOfDigitsToShow(1000.123456789)).toBe(1000.1235);
      expect(roundByGetNumberOfDigitsToShow(10000.123456789)).toBe(10000.1235);
      expect(roundByGetNumberOfDigitsToShow(1000)).toBe(1000);
      expect(roundByGetNumberOfDigitsToShow(1.234567891010111213e21)).toBe(1.2345678910101111e+21);
      expect(roundByGetNumberOfDigitsToShow(123e21)).toBe(1.23e+23);
    });
  });
  describe('getNDecimals', () => {
    test('rounds numbers based on the magnitude', () => {
      expect(getNDecimals(0.000000123456789, 3)).toBe(1.23e-7);
      expect(getNDecimals(0.000123456789, 3)).toBe(0.000123);
      expect(getNDecimals(10000.123456789, 3)).toBe(10000.123);
      expect(getNDecimals(10000.123456789, 7)).toBe(10000.1234568);
      expect(getNDecimals(100000000000.123456789, 3)).toBe(100000000000.123);
    });
  });
});
