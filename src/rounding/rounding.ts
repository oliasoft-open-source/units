import { parseNumber, unParseNumber } from '../parse/parse-number.ts';
import { isNumeric } from '../utils.ts';
import { toNum, isScientificStringNum } from '../numbers/numbers.ts';
import { formatDecimal } from '../numbers/display-number.ts';
import { isCloseToOrLessThan } from '../comparison/comparison.ts';

const DEFAULT_SIGNIFICANT_DIGITS = 4;

/**
 * Rounds a number to N decimal places.
 *
 * @private (see round() for the public interface)
 * @param value
 * @param [n]
 * @returns rounded number
 */
export const roundNumber = (value: number, n = DEFAULT_SIGNIFICANT_DIGITS): number => {
  const factor = 10 ** n;
  return Math.round(value * factor) / factor;
};

/**
 * Rounds a numeric value to N decimal places.
 *
 * - accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 * - returns same type as input
 *
 * @param value - the value to round
 * @param [n] - the number of decimal places to round to
 * @returns rounded value, or input value when unable to round
 *
 * @example
 * round(3.14159265) -> 3.1416
 */
export const round = <Value extends number | string | null | undefined>(
  value: Value,
  n = DEFAULT_SIGNIFICANT_DIGITS
): Value => {
  if (typeof value === 'number') {
    //need minimal overhead (performance / speed) if input is already number type
    return roundNumber(value, n) as Value;
  }
  if (value === null || Number.isNaN(value) || value === Infinity || value === -Infinity || value === undefined) {
    return value as Value;
  }
  const { number, unit, isString } = parseNumber(value);
  const result = !isNumeric(number) || (typeof number === 'string' && number.endsWith('.'))
    ? value
    : roundNumber(number, n);
  return unParseNumber({
    value: result,
    unit,
    isString,
    isScientific: isScientificStringNum(value)
  }) as Value;
};

/**
 * Rounds a number to N significant digits.
 *
 * @private (see roundToPrecision() for the public interface)
 * @param value
 * @param [n] number of significant digits
 * @returns rounded number
 */
export const roundNumberToPrecision = (
  value: number,
  n: number = DEFAULT_SIGNIFICANT_DIGITS,
): number => {
  return Number(value.toPrecision(n));
};

/**
 * Rounds a number to N significant digits, preserving trailing decimal zeros
 *
 * @private (see roundByMagnitudeToFixed() for the public interface)
 * @param value
 * @param [n] number of significant digits
 * @returns rounded number as string
 */
export const roundNumberToFixedPrecision = (
  value: number,
  n: number = DEFAULT_SIGNIFICANT_DIGITS,
): string => {
  const roundedValue = toNum(value).toPrecision(n);
  const [integerPart, decimalPart] = roundedValue.split('.');
  if (decimalPart) {
    const decimalDigits = n - integerPart.length;
    return `${integerPart}.${decimalPart.padEnd(decimalDigits, '0')}`;
  } else {
    return integerPart;
  }
};

/**
 * Rounds a number to N significant digits, excluding the integer part (only rounds decimal part)
 *
 * @private (see roundToPrecision() for the public interface)
 * @param value
 * @param [n] number of significant digits
 * @returns rounded number
 */
export const roundNumberToDecimalPrecision = (
  value: number,
  n: number = DEFAULT_SIGNIFICANT_DIGITS
): number => {
  if (Math.abs(value) > 1) {
    const [integerPart, decimalPart] = formatDecimal(value, '').split('.');
    if (!decimalPart) {
      return value;
    } else {
      const roundedDecimalPart = Number(`0.${decimalPart}`).toPrecision(n).slice(1);
      return Number(integerPart + roundedDecimalPart);
    }
  }
  return roundNumberToPrecision(value, n);
};

/**
 * Rounds a numeric value to N significant digits.
 *
 * - accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 * - returns same type as input
 * - similar to Number.toPrecision() but safer
 * - does *not* append trailing zeros (unlike `Number(4).toPrecision(4)` -> '4.000')
 *
 * @param value - the value to round
 * @param [n] - the number of significant digits
 * @returns rounded value, or input value when unable to round
 *
 * @example
 * roundToPrecision(0.0000456789) -> 0.00004568
 */
export const roundToPrecision = <Value extends number | string | null | undefined>(
  value: Value,
  n: number = DEFAULT_SIGNIFICANT_DIGITS
): Value => {
  if (typeof value === 'number') {
    //need minimal overhead (performance / speed) if input is already number type
    return roundNumberToPrecision(value, n) as Value;
  }
  if (value === null || Number.isNaN(value) || value === Infinity || value === -Infinity || value === undefined) {
    return value as Value;
  }
  const { number, unit, isString } = parseNumber(value);
  const result = !isNumeric(number) || (typeof number === 'string' && number.endsWith('.'))
    ? value
    : roundNumberToPrecision(number, n);
  return unParseNumber({
    value: result,
    unit,
    isString,
    isScientific: isScientificStringNum(value)
  }) as Value;
};

/**
 * Rounds a numeric value to N significant digits (only the decimal part)
 *
 * - accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 * - returns same type as input
 * - unlike roundToPrecision(), acts only on the decimal part, not the integer part too
 *
 * @param value - the value to round
 * @param [n]- the number of significant digits
 * @returns rounded value, or input value when unable to round
 *
 * @example
 * roundToPrecision(1234.0000456789) -> 1234.00004568
 */
export const roundToDecimalPrecision = <Value extends number | string | null | undefined >(
  value: Value,
  n: number = DEFAULT_SIGNIFICANT_DIGITS
): Value => {
  if (typeof value === 'number') {
    //need minimal overhead (performance / speed) if input is already number type
    return roundNumberToDecimalPrecision(value, n) as Value;
  }
  if (value === null || Number.isNaN(value) || value === Infinity || value === -Infinity || value === undefined) {
    return value as Value;
  }
  const { number, unit, isString } = parseNumber(value);
  const result = !isNumeric(number) || (typeof number === 'string' && number.endsWith('.'))
    ? value
    : roundNumberToDecimalPrecision(number, n);
  return unParseNumber({
    value: result,
    unit,
    isString,
    isScientific: isScientificStringNum(value)
  }) as Value;
};

/**
 * Rounds a number to an appropriate number of digits, based on its size.
 *
 * @private (see roundByMagnitude() for the public interface)
 * @param value
 * @param [n] the number of significant digits
 * @param [toFixed] to fixed digits (i.e. with trailing zeros)
 * @returns rounded number
 */
export const roundNumberByMagnitude = (
  value: number,
  n: number = DEFAULT_SIGNIFICANT_DIGITS,
  toFixed = false
): number | string => {
  const noDecimalsAbove = 10 ** n;
  const result = ((noDecimalsAbove && value > noDecimalsAbove))
    ? roundNumber(value, 0)
    : toFixed
      ? roundNumberToFixedPrecision(value, n)
      //for comparison, old implementation from toPretty (functionally equivalent to new implementation, see tests)
      //return Number(value.toExponential(n - 1).toString());
      : roundNumberToPrecision(value, n);
  return toFixed ? String(result) : result;
};

/**
 * Rounds a numeric value to an appropriate number of digits, based on its size. It rounds to N significant digits,
 * but never rounds the integer part.
 *
 * - accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 * - returns same type as input
 *
 * @param value - the value to round
 * @param [n] - the number of significant digits
 * @returns rounded value, or input value when unable to round
 *
 * @example
 * roundByMagnitude(0.000123456789) -> 0.0001235
 * roundByMagnitude(1.123456789) -> 1.123
 * roundByMagnitude(19999.123456789) -> 19999
 */
export const roundByMagnitude = <Value extends number | string | null | undefined >(
  value: Value,
  n: number = DEFAULT_SIGNIFICANT_DIGITS
): Value => {
  if (typeof value === 'number') {
    //need minimal overhead (performance / speed) if input is already number type
    return roundNumberByMagnitude(value, n) as Value;
  }
  if (value === null || Number.isNaN(value) || value === Infinity || value === -Infinity || value === undefined) {
    return value as Value;
  }
  const { number, unit, isString } = parseNumber(value);
  const result = !isNumeric(number) || (typeof number === 'string' && number.endsWith('.'))
    ? value
    : roundNumberByMagnitude(number, n);
  return unParseNumber({
    value: result,
    unit,
    isString,
    isScientific: isScientificStringNum(value)
  }) as Value;
};

/**
 * Rounds a numeric value to an appropriate number of digits, based on its size. It rounds to N significant digits,
 * but never rounds the integer part. Similar to roundByMagnitude, but adds trailing zeros.
 *
 * - accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 * - returns string type (or original value and type when not possible to convert)
 *
 * @param value - the value to round
 * @param [n] - the number of significant digits
 * @returns rounded value as a string, or input value when unable to round
 *
 * @example
 * roundByMagnitudeToFixed(0.000120016789) -> 0.0001200
 */
export const roundByMagnitudeToFixed = <Value extends number | string | null | undefined>(
  value: Value,
  n: number = DEFAULT_SIGNIFICANT_DIGITS
): Value => {
  const toFixed = true;
  if (value === null || Number.isNaN(value) || value === Infinity || value === -Infinity || value === undefined) {
    return value as Value;
  }
  if (typeof value === 'number') {
    //need minimal overhead (performance / speed) if input is already number type
    return roundNumberByMagnitude(value, n, toFixed) as Value;
  }
  const { number, unit } = parseNumber(value, true);
  const result = !isNumeric(number) || (typeof number === 'string' && number.endsWith('.'))
    ? value
    : roundNumberByMagnitude(number, n, toFixed);
  return unParseNumber({
    value: result,
    unit,
    isString: true,
    isScientific: isScientificStringNum(value)
  }) as Value;
};

/**
 * Rounds a number to an appropriate number of digits, based on its size in relation to a range.
 *
 * @private (see roundByMagnitude() for the public interface)
 * @param value
 * @param min
 * @param max
 * @param [n] the minimum number of significant digits
 * @returns rounded number
 */
export const roundNumberByRange = (
  value: number,
  min: number,
  max: number,
  n = DEFAULT_SIGNIFICANT_DIGITS
): number => {
  if (isCloseToOrLessThan(max, min)) {
    return value;
  }
  const range = max - min;
  const scale = Math.floor(Math.log10(range));
  const precision = Math.max(n, 0 - scale);
  return roundNumber(value, precision);
};

/**
 * Rounds a numeric value to an appropriate number of digits, based on its size within a range of values.
 *
 * - accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 * - returns same type as input
 *
 * @param value - the value to round
 * @param min - the min value in the range
 * @param max - the max value in the range
 * @param [n] - the minimum number of significant digits
 * @returns rounded value, or input value when unable to round
 *
 */
export const roundByRange = <Value extends number | string | null | undefined >(
  value: Value,
  min: number,
  max: number,
  n: number = DEFAULT_SIGNIFICANT_DIGITS
): Value => {
  if (typeof value === 'number') {
    //need minimal overhead (performance / speed) if input is already number type
    return roundNumberByRange(value, min, max, n) as Value;
  }
  if (value === null || Number.isNaN(value) || value === Infinity || value === -Infinity || value === undefined) {
    return value as Value;
  }
  const { number, unit, isString } = parseNumber(value);
  const result = !isNumeric(number) || (typeof number === 'string' && number.endsWith('.'))
    ? value
    : roundNumberByRange(number, min, max, n);
  return unParseNumber({
    value: result,
    unit,
    isString,
    isScientific: isScientificStringNum(value)
  }) as Value;
};

/**
 * Rounds a numeric value to N fixed decimal digits.
 *
 * - accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 * - returns same type as input
 *
 * @param value - the value to round
 * @param [n] - the number of fixed decimal digits
 * @returns rounded value, or input value when unable to round
 *
 */
export const roundToFixed = <Value extends number | string | null | undefined>(
  value: Value,
  n: number = DEFAULT_SIGNIFICANT_DIGITS
): Value => {
  if (value === null || Number.isNaN(value) || value === Infinity || value === -Infinity || value === undefined) {
    return value as Value;
  }
  if (typeof value === 'number') {
    return value.toFixed(n) as Value;
  }
  const { number, unit, isString } = parseNumber(value);
  const result = !isNumeric(number) || (typeof number === 'string' && number.endsWith('.'))
    ? value
    : number.toFixed(n);
  return unParseNumber({
    value: result,
    unit,
    isString,
    isScientific: isScientificStringNum(value)
  }) as Value;
};
