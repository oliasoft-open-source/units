import { parseNumber } from '../parse/parse-number.ts';
import { convertAndGetValue, isValueWithUnit, } from '../units.ts';
import { toNum, isValidNum } from '../numbers/numbers.ts';
import { isNumeric, isPercentage } from '../utils.ts';

export const DEFAULT_MAX_RELATIVE_DIFF = Number.EPSILON;

type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never };
type XOR<T, U> = (T | U) extends Record<string, unknown> ? (Without<T, U> & U) | (Without<U, T> & T) : T | U;

type toleranceType = XOR<{relativeDiff: number| string}, {absoluteDiff: string| number}>;

const convertNumbers = (
  firstValue: number | string,
  secondValue: number | string
): {firstNumber: number | string, secondNumber: number | string} => {
  const { number: firstNumber, unit: firstUnit } = parseNumber(firstValue);
  const { number: secondNumber, unit: secondUnit } = parseNumber(secondValue);
  const differentUnits = (firstUnit && secondUnit) && (firstUnit !== secondUnit);
  const convertedSecondNumber = differentUnits ? convertAndGetValue(secondNumber, firstUnit, secondUnit) : secondNumber;
  return {
    firstNumber,
    secondNumber: convertedSecondNumber
  };
};

const getToleranceNumber = (relativeDiff: string | number): number | null => {
  if (relativeDiff !== null && relativeDiff !== undefined) {
    if (isNumeric(relativeDiff) && typeof relativeDiff === 'number') {
      return relativeDiff;
    }
    if (isPercentage(relativeDiff)) {
      const percentageValue = toNum(relativeDiff?.toString().replace('%', ''));
      if (isNumeric(percentageValue)) {
        return percentageValue / 100;
      }
    }
  }

  return null;
};

/**
 * Determines whether two numbers are close in value with a tolerance
 * (mitigates excess JavaScript floating point precision quirks)
 */
export const isCloseTo = (
  firstValue: number | string | null,
  secondValue: number | string | null,
  options: toleranceType = {relativeDiff: DEFAULT_MAX_RELATIVE_DIFF}
): boolean => {
  const {relativeDiff, absoluteDiff} = options;
  const diff = relativeDiff ?? absoluteDiff;
  const toleranceNumber = getToleranceNumber(diff);

  if (firstValue === null || secondValue === null) {
    return false;
  }

  const hasUnitFirstValue = isValueWithUnit(firstValue);
  const hasUnitSecondValue = isValueWithUnit(secondValue);

  if ((hasUnitFirstValue && !hasUnitSecondValue) || (!hasUnitFirstValue && hasUnitSecondValue)) {
    throw new Error(`Parameters must either both have units or both not have units. Received "${firstValue}" and "${secondValue}"`);
  }

  if (toleranceNumber === null) {
    console.warn('Tolerance number is not defined!');
    return firstValue === secondValue;
  }

  if (toleranceNumber <= 0 || toleranceNumber < Number.EPSILON) {
    throw Error('Unpredictable results - toleranceNumber should be bigger than zero or less then EPSILON');
  }
  const {firstNumber, secondNumber} = convertNumbers(firstValue, secondValue);
  if (
    ((firstNumber === Infinity || firstNumber === 'Infinity') && (secondNumber === Infinity || secondNumber === 'Infinity'))
    || (firstNumber === -Infinity || firstNumber === '-Infinity') && (secondNumber === -Infinity || secondNumber === '-Infinity')
  ) {
    return true;
  }
  if (typeof firstNumber === 'number' && typeof secondNumber === 'number') {
    //1. we know they are equals
    if (firstNumber === secondNumber) return true;

    if (absoluteDiff || firstNumber === 0 || secondNumber === 0) {
      //2. checks whether they are close enough absolutely
      const diff = Math.abs(firstNumber - secondNumber);
      return isCloseTo(diff, toleranceNumber, { relativeDiff: '1%' }) || diff < toleranceNumber;
    } else {
      //3. relative checks whether they are close enough relatively
      return (2 * Math.abs((firstNumber - secondNumber) / (firstNumber + secondNumber)) < toleranceNumber);
    }
  }

  return false;
};

/**
 * Determines whether two numbers are close enough to be equal
 * or checks the firstValue is greater than the secondValue
 */
export const isCloseToOrGreaterThan = (
  firstValue: number | string | null,
  secondValue: number | string | null,
  options: toleranceType = {relativeDiff: DEFAULT_MAX_RELATIVE_DIFF}
) : boolean => {
  if (firstValue === null || secondValue === null) {
    return false;
  }
  const {firstNumber, secondNumber} = convertNumbers(firstValue, secondValue);
  if (typeof firstNumber === 'number' && typeof secondNumber === 'number') {
    return isCloseTo(firstNumber, secondNumber, options) || firstNumber > secondNumber;
  }

  return false;
};

/**
 * Determines whether two numbers are close enough to be equal
 * or checks the firstValue is less than the secondValue
 */
export const isCloseToOrLessThan = (
  firstValue: number | string | null,
  secondValue: number | string | null,
  options: toleranceType = {relativeDiff: DEFAULT_MAX_RELATIVE_DIFF}
) : boolean => {
  if (firstValue === null || secondValue === null) {
    return false;
  }
  const {firstNumber, secondNumber} = convertNumbers(firstValue, secondValue);
  if (typeof firstNumber === 'number' && typeof secondNumber === 'number') {
    return isCloseTo(firstNumber, secondNumber, options) || firstNumber < secondNumber;
  }

  return false;
};

/**
 * Determines whether two objects or arrays are deeply close equal (all nested child numbers)
 */
export const isDeepCloseTo = (a: any, b: any, options: toleranceType = {relativeDiff: DEFAULT_MAX_RELATIVE_DIFF}): boolean => {
  if (Array.isArray(a) && Array.isArray(b)) {
    if (a.length !== b.length) {
      return false;
    }
    return a.every((a, i) => isDeepCloseTo(a, b[i], options));
  }
  if (typeof a === 'object' && a !== null && typeof b === 'object' && b !== null) {
    const aKeys = Object.keys(a);
    const bKeys = Object.keys(b);
    if (aKeys.length !== bKeys.length) {
      return false;
    }
    return aKeys.every((key) => isDeepCloseTo(a[key], b[key], options));
  }
  if (
    (Number.isNaN(a) && Number.isNaN(b))
    || (a === '' && b === '')
  ) {
    return true;
  }
  if (
    (typeof a === 'number' && typeof b === 'number')
    || isValueWithUnit(a) && isValueWithUnit(b)
    || (isValidNum(a) && isValidNum(b))
  ) {
    return isCloseTo(a, b, options);
  }
  /*
    Generic strings and non-numerical properties should pass (loose checking)
    This function only compares numerical properties
  */
  return true;
};
