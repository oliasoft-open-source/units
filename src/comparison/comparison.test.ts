import {
  isCloseTo,
  isDeepCloseTo,
  isCloseToOrLessThan,
  isCloseToOrGreaterThan
} from '../index.ts';

describe('Comparison', () => {
  describe('isCloseTo', () => {
    test('Checks the values are close to each other', () => {
      //obviously they are equal:
      expect(isCloseTo(0, 0)).toBe(true);
      expect(isCloseTo(123, 123)).toBe(true);
      expect(isCloseTo(123.000123, 123.000123)).toBe(true);
      expect(isCloseTo(0.123456789, 0.123456789)).toBe(true);
      expect(isCloseTo(1e-12, 1e-12)).toBe(true);
      expect(isCloseTo('0', '0')).toBe(true);
      expect(isCloseTo('0.123456789', 0.123456789)).toBe(true);
      expect(isCloseTo(0.123456789, '0.123456789')).toBe(true);
      expect(isCloseTo(Infinity, Infinity)).toBe(true); //debatable
      expect(isCloseTo(NaN, NaN)).toBe(false);
      //close enough to be equal
      expect(isCloseTo(0.3, 0.1 + 0.2)).toBe(true);
      expect(isCloseTo(1000.1 + 1000.2, 2000.3)).toBe(true);
      expect(isCloseTo(1,  (0.3 * 3) + 0.1)).toBe(true);
      expect(isCloseTo(0, 0.1e-17)).toBe(true);
      expect(isCloseTo('0.3', 0.1 + 0.2)).toBe(true);
      expect(isCloseTo(0, '0.1e-17')).toBe(true);

      //they are not close enough to be equal
      expect(isCloseTo(1,  0.99999)).toBe(false);
      expect(isCloseTo(1.12345e-100, 1.6e-100)).toBe(false);
      expect(isCloseTo(1e-18, 1.1e-18)).toBe(false);
      expect(isCloseTo(1.9864e-17, 1.1e-17)).toBe(false);
      expect(isCloseTo(123.000000002, 123.000000003)).toBe(false);
      expect(isCloseTo(100e100,  100.0001e100)).toBe(false);
      expect(isCloseTo(0.000000002, 0.000000003)).toBe(false);
      expect(isCloseTo(-0.000000002, -0.000000003)).toBe(false);
      expect(isCloseTo(123.000000002234, 123.00000000345646)).toBe(false);
      expect(isCloseTo(0, 0.000000001)).toBe(false);
      expect(isCloseTo(123, 124)).toBe(false);
      expect(isCloseTo(123.000123, 123.000124)).toBe(false);
      expect(isCloseTo('123.000123', 123.000124)).toBe(false);
      expect(isCloseTo('', 123.000124)).toBe(false);
      expect(isCloseTo(123.000124, '')).toBe(false);
      expect(isCloseTo('', '')).toBe(false);
      expect(isCloseTo(null, 123.000124)).toBe(false);
      expect(isCloseTo(123.000124, null)).toBe(false);
    });
    test('Checks the values with given relative difference', () => {
      expect(isCloseTo(1,  0.9999901, { relativeDiff: 0.00001})).toBe(true);
      expect(isCloseTo(0.9999901, 1, { relativeDiff: 0.00001})).toBe(true);
      expect(isCloseTo(1.12345e-100, 1.125e-100, { relativeDiff: 1e-2 })).toBe(true);
      expect(isCloseTo(10, 10.1, { relativeDiff: '1%'})).toBe(true);
      expect(isCloseTo(10, 10.2,  { relativeDiff: '1%'})).toBe(false);
      expect(isCloseTo(1,  0.99998, {relativeDiff: 0.00001 })).toBe(false);
      expect(isCloseTo(1,  0.99998, {relativeDiff: 0.00001})).toBe(false);
    });
    test('Throws an error with given bad relative difference', () => {
      expect(() => isCloseTo(1, 1, { relativeDiff: 0 })).toThrow(/Unpredictable/);
      expect(() => isCloseTo(1, 1, { relativeDiff: parseFloat('-0e-10') })).toThrow(/Unpredictable/);
      expect(() => isCloseTo(1, 1, { relativeDiff: 1e-18 })).toThrow(/Unpredictable/);
    });
    test('Checks the values with given absolute difference', () => {
      expect(isCloseTo(11.3, 11, { absoluteDiff: 0.5 })).toBe(true);
      expect(isCloseTo(11, 11.34567, { absoluteDiff: 0.5 })).toBe(true);
      expect(isCloseTo(11.5, 11, { absoluteDiff: 0.5 })).toBe(true);
      expect(isCloseTo(11.7, 11, { absoluteDiff: 0.5 })).toBe(false);
      expect(isCloseTo(11.734, 11.7342, { absoluteDiff: 0.001 })).toBe(true);
      expect(isCloseTo(11.734, 11.735, { absoluteDiff: 0.001 })).toBe(true);
      expect(isCloseTo(11.734, 11.736, { absoluteDiff: 0.001 })).toBe(false);
      expect(isCloseTo(0.000001, 0.000002, { absoluteDiff: 0.000001 })).toBe(true);
      expect(isCloseTo(0.000001, 0.0000021, { absoluteDiff: 0.000001 })).toBe(false);
    });
    test('Throws an error with given bad absolute difference', () => {
      expect(() => isCloseTo(1, 1, { absoluteDiff: 0 })).toThrow(/Unpredictable/);
      expect(() => isCloseTo(1, 1, { absoluteDiff: parseFloat('-0e-10') })).toThrow(/Unpredictable/);
      expect(() => isCloseTo(1, 1, { absoluteDiff: 1e-18 })).toThrow(/Unpredictable/);
    });
    test('Checks the values with units', () => {
      //obviously they are equals:
      expect(isCloseTo('123.000123|m', '123.000123|m')).toBe(true);
      expect(isCloseTo('0.123456789|m', '0.123456789|m')).toBe(true);
      expect(isCloseTo('1e-12|m', '1e-12|m')).toBe(true);
      expect(isCloseTo('1|GPa', '1e9|Pa')).toBe(true);
      expect(isCloseTo('1|Pa', '1e-9|GPa')).toBe(true);
      //close enough to be equal
      expect(isCloseTo('0.3|m', '0.3000000000000000003|m')).toBe(true);
      expect(isCloseTo('0|m', '0.1e-17|m')).toBe(true);
    });
    test('Checks the values with different units', () => {
      expect(isCloseTo('0.3|m', '0.0003000000000000000003|km')).toBe(true);
      expect(isCloseTo('77,8355568|cm', '2.55366|ft')).toBe(true);
    });
    test('Throws an error with given units mistmatch', () => {
      expect(() => isCloseTo('0.3|sg', '0.3000000000000000003|m')).toThrow(/No conversions found/);
      expect(() => isCloseTo('77.8355568|cm', '2.55366|kg')).toThrow(/No conversions found/);
      expect(() => isCloseTo('0.3|m', '0.3')).toThrowError('Parameters must either both have units or both not have units. Received "0.3|m" and "0.3"');
      expect(() => isCloseTo('0.3', '0.3|m')).toThrowError('Parameters must either both have units or both not have units. Received "0.3" and "0.3|m"');
      expect(() => isCloseTo('0.3|m', 0.3)).toThrowError('Parameters must either both have units or both not have units. Received "0.3|m" and "0.3"');
      expect(() => isCloseTo(0.3, '0.3|m')).toThrowError('Parameters must either both have units or both not have units. Received "0.3" and "0.3|m"');
    });
  });
  describe('isDeepCloseTo deeply compares the numerical properties in objects or arrays', () => {
    test('pass on similar values', () => {
      const a = {
        a: 0,
        b: '77,8355568|cm',
        c: {
          d: 2000.3,
          e: ['0,3'],
          f: 'not a number',
        },
      };
      const b = {
        a: '0,1e-17',
        b: '2.55366|ft',
        c: {
          d: 2000.3000000000002,
          e: [0.30000000000000004],
          f: 'not a number',
        },
      };
      expect(isDeepCloseTo(a, b)).toBe(true);
    });
    test('pass on self-compare', () => {
      const a = {
        a: 1.000001,
        b: '123.000123|m',
        c: {
          d: 1.12345e-100,
          e: ['1.000004', 1.000005],
          f: 'not a number',
          g: '1,23',
          h: null,
          i: undefined,
          j: '',
          k: NaN,
          l: Infinity,
          m: -Infinity,
          n: '1.',
          o: '1,'
        },
      };
      expect(isDeepCloseTo(a, a)).toBe(true);
    });
    test('pass for simple types', () => {
      expect(isDeepCloseTo(0.3, 0.1 + 0.2)).toBe(true);
      expect(isDeepCloseTo(1000.1 + 1000.2, 2000.3)).toBe(true);
      expect(isDeepCloseTo(1,  (0.3 * 3) + 0.1)).toBe(true);
      expect(isDeepCloseTo(0, 0.1e-17)).toBe(true);
      expect(isDeepCloseTo('0.3', 0.1 + 0.2)).toBe(true);
      expect(isDeepCloseTo(0, '0.1e-17')).toBe(true);
    });
    test('pass for object of strings with slash (OW-19280)', () => {
      expect(isDeepCloseTo(
        {name: '13 3/8, Intermediate, Casing'},
        {name: '13 3/8, Intermediate, Casing'}
      )).toBe(true);
    });
    test('pass when arguments empty', () => {
      expect(isDeepCloseTo([], [])).toBe(true);
      expect(isDeepCloseTo({}, {})).toBe(true);
      expect(isDeepCloseTo(null, null)).toBe(true);
      expect(isDeepCloseTo(undefined, undefined)).toBe(true);
    });
    test('fail on values not close enough', () => {
      const a = {
        a: 1.000001,
        b: '123.000123|m',
        c: {
          d: 1.12345e-100,
          e: ['1.000004', 1.000005],
          f: 'not a number',
          g: '1,23',
          h: null,
          i: undefined,
          j: '',
          k: NaN,
          l: Infinity,
          m: -Infinity,
          n: '1.',
          o: '1,'
        },
      };
      const b = {
        a: 1.000001,
        b: '123.000123|m',
        c: {
          d: 1.6e-100,
          e: ['1.000004', 1.000005],
          f: 'not a number',
          g: '1,23',
          h: null,
          i: undefined,
          j: '',
          k: NaN,
          l: Infinity,
          m: -Infinity,
          n: '1.',
          o: '1,'
        },
      };
      expect(isDeepCloseTo(a, b)).toBe(false);
    });
    test('fail on different number of keys', () => {
      const a = {
        a: 1.000001,
        b: '123.000123|m',
        c: {
          d: 1.12345e-100,
          e: ['1.000004', 1.000005],
        },
      };
      const b = {
        a: 1.000001,
        b: '123.000123|m',
        c: {
          e: ['1.000004', 1.000005],
        },
      };
      expect(isDeepCloseTo(a, b)).toBe(false);
    });
  });
  describe('isCloseToOrLessThan', () => {
    test('Checks the values are close to each other or less than', () => {
      //equals
      expect(isCloseToOrLessThan(0.3, 0.1 + 0.2)).toBe(true);
      expect(isCloseToOrLessThan(1000.1 + 1000.2, 2000.3)).toBe(true);
      expect(isCloseToOrLessThan(1,  (0.3 * 3) + 0.1)).toBe(true);
      expect(isCloseToOrLessThan(0, 0.1e-17)).toBe(true);
      expect(isCloseToOrLessThan('0.3', 0.1 + 0.2)).toBe(true);
      expect(isCloseToOrLessThan(0, '0.1e-17')).toBe(true);

      //less than
      expect(isCloseToOrLessThan(1e-18, 1.1e-18)).toBe(true);
      expect(isCloseToOrLessThan(1.6e-103, 1.6e-102)).toBe(true);
      expect(isCloseToOrLessThan(1.0999e-17, 1.1e-17)).toBe(true);
      expect(isCloseToOrLessThan(123.000000002, 123.000000003)).toBe(true);
      expect(isCloseToOrLessThan(100e100,  100.0001e100)).toBe(true);
      expect(isCloseToOrLessThan(0.000000002, 0.000000003)).toBe(true);
      expect(isCloseToOrLessThan(123.000000002234, 123.00000000345646)).toBe(true);
      expect(isCloseToOrLessThan(0, 0.000000001)).toBe(true);
      expect(isCloseToOrLessThan(123, 124)).toBe(true);
      expect(isCloseToOrLessThan(123.000123, 123.000124)).toBe(true);

      expect(isCloseToOrLessThan(-0.000000002, -0.000000003)).toBe(false);
      expect(isCloseToOrLessThan(1,  0.99999)).toBe(false);
      expect(isCloseToOrLessThan(null, 123.000124)).toBe(false);
      expect(isCloseToOrLessThan(123.000124, null)).toBe(false);
    });
  });
  describe('isCloseToOrGreaterThan', () => {
    test('Checks the values are close to each other or greater than', () => {
      //equals
      expect(isCloseToOrGreaterThan(0.3, 0.1 + 0.2)).toBe(true);
      expect(isCloseToOrGreaterThan(1000.1 + 1000.2, 2000.3)).toBe(true);
      expect(isCloseToOrGreaterThan(1,  (0.3 * 3) + 0.1)).toBe(true);
      expect(isCloseToOrGreaterThan(0, 0.1e-17)).toBe(true);
      expect(isCloseToOrGreaterThan('0.3', 0.1 + 0.2)).toBe(true);
      expect(isCloseToOrGreaterThan(0, '0.1e-17')).toBe(true);

      //greater than
      expect(isCloseToOrGreaterThan(1.1e-18, 1e-18)).toBe(true);
      expect(isCloseToOrGreaterThan(1.6e-102, 1.6e-103)).toBe(true);
      expect(isCloseToOrGreaterThan(1.1e-17, 1.0999e-17)).toBe(true);
      expect(isCloseToOrGreaterThan(123.000000003, 123.000000002)).toBe(true);
      expect(isCloseToOrGreaterThan(100.0001e100, 100e100)).toBe(true);
      expect(isCloseToOrGreaterThan(0.000000003, 0.000000002)).toBe(true);
      expect(isCloseToOrGreaterThan(123.00000000345646, 123.000000002234)).toBe(true);
      expect(isCloseToOrGreaterThan(0.000000001, 0)).toBe(true);
      expect(isCloseToOrGreaterThan(124, 123)).toBe(true);
      expect(isCloseToOrGreaterThan(123.000124, 123.000123)).toBe(true);

      expect(isCloseToOrGreaterThan(-0.000000003, -0.000000002)).toBe(false);
      expect(isCloseToOrGreaterThan(0.99999, 1)).toBe(false);
      expect(isCloseToOrGreaterThan(null, 123.000124)).toBe(false);
      expect(isCloseToOrGreaterThan(123.000124, null)).toBe(false);
    });
  });
});