import {
  showAltUnitsList,
  label,
  getAltUnitsListByQuantity,
  unitFromKey,
  checkAndCleanDecimalComma,
  to,
  split,
  unum,
  convertTable,
  withUnit,
  unumWithUnit,
  validateAndClean,
  isValueWithUnit,
  convertSamePrecision,
  getValue,
  getUnit,
  withPrettyUnitLabel,
  getQuantities,
  roundNumberWithLabel,
  altUnitsList,
  toBase,
  getUnitsForQuantity,
  unitFromQuantity,
  convertAndGetValue,
  convertAndGetValueStrict,
  validateNumber
} from '../units.ts';
import { ALT_UNITS } from '../constants.ts';
import { round } from '../rounding/rounding.ts';

const { NumberFormat } = Intl;

describe('Units', () => {
  it('should support basic splitting of number and unit', () => {
    expect(split('12.2')).toEqual(['12.2', '']);
    expect(split(String(12.2))).toEqual(['12.2', '']);
    expect(split('12.2m')).toEqual(['12.2', 'm']);
    expect(split('-12.2m')).toEqual(['-12.2', 'm']);
    expect(split('-12,2m')).toEqual(['-12.2', 'm']);
    expect(split('-12 1/2m')).toEqual(['-12 1/2', 'm']);
    expect(split('-120,000.02m')).toEqual(['-120000.02', 'm']);
    expect(split('-120.000,02m')).toEqual(['-120000.02', 'm']);
    expect(split('-120,000.02m')).toEqual(['-120000.02', 'm']);
  });

  it('should get value from numberWithUnit string', () => {
    expect(getValue('12.2')).toEqual('12.2');
    expect(getValue('12.2m')).toEqual('12.2');
    expect(getValue('12.2|m')).toEqual('12.2');
    expect(getValue('|m')).toEqual('');
    expect(getValue('m')).toEqual('');
  });

  it('should get unit from numberWithUnit string', () => {
    expect(getUnit('12.2')).toEqual('');
    expect(getUnit('12.2m')).toEqual('m');
    expect(getUnit('12.2|m')).toEqual('m');
    expect(getUnit('|m')).toEqual('m');
    expect(getUnit('m')).toEqual('m');
  });

  it('should not split numbers with exponential notation on the e', () => {
    expect(split('-2.956215116057236e-10')).toEqual([
      '-2.956215116057236e-10',
      '',
    ]);
    expect(split('-2.956215116057236e-10|m')).toEqual([
      '-2.956215116057236e-10',
      'm',
    ]);
    expect(split('-2.956215116057236e-10kg')).toEqual([
      '-2.956215116057236e-10',
      'kg',
    ]);
    expect(split('-2.956215116057236e-10|exadonkeys')).toEqual([
      '-2.956215116057236e-10',
      'exadonkeys',
    ]);
    expect(split('-2.956215116057236e-10')).toEqual([
      '-2.956215116057236e-10',
      '',
    ]);
    // strange result e+70
    expect(split('-2.956215116057236e70')).toEqual([
      '-2.956215116057236e70',
      '',
    ]);
  });

  it('should split special numbers', () => {
    expect(split('NaN')).toEqual(['NaN', '']);
    expect(split('NaN|m')).toEqual(['NaN', 'm']);
    expect(split('Infinity')).toEqual(['Infinity', '']);
    expect(split('Infinity|m')).toEqual(['Infinity', 'm']);
    expect(split('-Infinity')).toEqual(['-Infinity', '']);
    expect(split('-Infinity|m')).toEqual(['-Infinity', 'm']);
  });

  it('should return alt units', () => {
    expect(showAltUnitsList('acceleration')).toEqual(['ft/s2', 'm/s2']);
    expect(showAltUnitsList('angles')).toEqual(['deg', 'rad']);
    expect(showAltUnitsList('dls')).toEqual(['deg/10m', 'deg/30m', 'deg/100ft']);
    expect(getUnitsForQuantity('acceleration')).toEqual(showAltUnitsList('acceleration'));
    expect(getUnitsForQuantity('angles')).toEqual(showAltUnitsList('angles'));
    expect(getUnitsForQuantity('dls')).toEqual(showAltUnitsList('dls'));
  });
  it('should return labels', () => {
    expect(label('in')).toEqual('in');
    expect(label('mm')).toEqual('mm');
    expect(label('cm')).toEqual('cm');
    expect(label('qwe123')).toEqual(undefined);
  });
  it('alt units list with labels', () => {
    expect(getAltUnitsListByQuantity('angles'))
      .toEqual([{unit: 'deg', label: '°'}, {unit: 'rad', label: 'rad'}]);
    expect(getAltUnitsListByQuantity('qwe123'))
      .toEqual(undefined);
  });
  it('should return unit from key', () => {
    expect(unitFromKey('force')).toEqual('N');
    expect(unitFromKey('qwe123')).toEqual(undefined);
    expect(unitFromQuantity('force')).toEqual(unitFromKey('force'));
    expect(unitFromQuantity('speed')).toEqual('m/s');
  });
  it('check and clean decimal comma', () => {
    expect(checkAndCleanDecimalComma(123)).toEqual(123);
    expect(checkAndCleanDecimalComma('36,6')).toEqual('36.6');
    expect(checkAndCleanDecimalComma('36..6')).toEqual('36.6');
  });
  it('isValueWithUnit should properly determinate if value is value with know unit', () => {
    expect(isValueWithUnit(123)).toEqual(false);
    expect(isValueWithUnit(-15)).toEqual(false);
    expect(isValueWithUnit('15|notOurUnit')).toEqual(false);
    expect(isValueWithUnit('15')).toEqual(false);
    expect(isValueWithUnit('15/m')).toEqual(false);
    expect(isValueWithUnit('15/MSm3')).toEqual(false);
    expect(isValueWithUnit('15|MSm3')).toEqual(true);
    expect(isValueWithUnit('15|m')).toEqual(true);
    expect(isValueWithUnit('6|mm2')).toEqual(true);
    expect(isValueWithUnit('6|mile2')).toEqual(true);
    expect(isValueWithUnit('7|km')).toEqual(true);
    expect(isValueWithUnit('7|sg')).toEqual(true);
    expect(isValueWithUnit('9|rad/ft')).toEqual(true);
    expect(isValueWithUnit('7|deg|10m')).toEqual(false);
  });

  describe('Conversions', () => {
    it('should do basic conversions', () => {
      expect(to(1, 'ft', 'm')).toEqual(0.3048);
      expect(to(1, 'm', 'ft')).toEqual(1 / 0.3048);
      expect(to(0, 'C', 'F')).toEqual(32);
      expect(to(32, 'F', 'C')).toEqual(0);
      expect(to(50, '%', 'fr')).toEqual(0.5);
      expect(to(0.125, 'fr', '%')).toEqual(12.5);
      expect(to(180, 'deg', 'rad').toFixed(4)).toEqual('3.1416');
      expect(to(1, 'rad', 'deg').toFixed(4)).toEqual('57.2958');
      expect(to('1,12', 'rad', 'deg').toFixed(4)).toEqual('64.1713');
      expect(to('1....12', 'rad', 'deg').toFixed(4)).toEqual('64.1713');
      expect(to('1,,12', 'rad', 'deg').toFixed(4)).toEqual('64.1713');
      expect(to('1,,.12', 'rad', 'deg').toFixed(4)).toEqual('64.1713');
    });
  });

  it('should convert length units', () => {
    expect(to(1, 'm', 'mm')).toEqual(1000);
    expect(to(123456, 'm', 'mm')).toEqual(123456000);
    expect(to(123.456, 'm', 'mm')).toEqual(123456);
    expect(to(1, 'm', 'cm')).toEqual(100);
    expect(to(123456, 'm', 'cm')).toEqual(12345600);
    expect(to(123.456, 'm', 'cm')).toEqual(12345.6);
    expect(to(1, 'm', 'km')).toEqual(1 / 1000);
    expect(to(123456, 'm', 'km')).toEqual(123.456);
    expect(to(123.456, 'm', 'km')).toEqual(0.123456);
    expect(to(1, 'm', 'ft')).toEqual(1 / 0.3048);
    expect(to(123456, 'm', 'ft')).toEqual(405039.3700787401);
    expect(to(123.456, 'm', 'ft')).toEqual(405.03937007874015);
    expect(to(1, 'm', 'in')).toEqual(39.37007874015748);
    expect(to(123456, 'm', 'in')).toEqual(4860472.4409448821);
    expect(to(123.456, 'm', 'in')).toEqual(4860.472440944882);
    expect(to(1, 'mm', 'm')).toEqual(1 / 1000);
    expect(to(123456, 'mm', 'm')).toEqual(123.456);
    expect(to(123.456, 'mm', 'm')).toEqual(0.123456);
    expect(to(1, 'cm', 'm')).toEqual(1 / 100);
    expect(to(123456, 'cm', 'm')).toEqual(1234.56);
    expect(to(123.456, 'cm', 'm')).toEqual(1.23456);
    expect(to(1, 'km', 'm')).toEqual(1000);
    expect(to(123456, 'km', 'm')).toEqual(123456000);
    expect(to(123.456, 'km', 'm')).toEqual(123456);
    expect(to(1, 'ft', 'm')).toEqual(0.3048);
    expect(to(123456, 'ft', 'm')).toEqual(37629.3888);
    expect(to(123.456, 'ft', 'm')).toEqual(37.6293888);
    expect(to(1, 'in', 'm')).toEqual(0.0254);
    expect(to(123456, 'in', 'm')).toEqual(3135.7824);
    expect(to(123.456, 'in', 'm')).toEqual(3.1357824);

    expect(
      (() => {
        const m_mm = to(1, 'm', 'mm');
        const mm_cm = to(m_mm, 'mm', 'cm');
        const cm_km = to(mm_cm, 'cm', 'km');
        const km_ft = to(cm_km, 'km', 'ft');
        const ft_in = to(km_ft, 'ft', 'in');
        const in_m = to(ft_in, 'in', 'm');
        return Number(in_m.toFixed(12));
      })()).toEqual(1);
  });

  it('should convert area units', () => {
    expect(to(1, 'm2', 'mm2')).toEqual(1000000);
    expect(to(123456, 'm2', 'mm2')).toEqual(123456000000);
    expect(to(123.456, 'm2', 'mm2')).toEqual(123456000);
    expect(to(1, 'm2', 'cm2')).toEqual(10000);
    expect(to(123456, 'm2', 'cm2')).toEqual(1234560000);
    expect(to(123.456, 'm2', 'cm2')).toEqual(1234560);
    expect(to(1, 'm2', 'km2')).toEqual(0.000001);
    expect(to(123456, 'm2', 'km2')).toEqual(0.123456);
    expect(to(123.456, 'm2', 'km2')).toEqual(0.000123456);
    expect(to(1, 'm2', 'in2')).toEqual(1550.0031);
    expect(to(12, 'm2', 'in2')).toEqual(18600.0372);
    expect(to(12.34, 'm2', 'in2')).toEqual(19127.038254);
    expect(to(1, 'm2', 'ft2')).toEqual(10.76391);
    expect(to(12, 'm2', 'ft2')).toEqual(129.16692);
    expect(Number(to(12.34, 'm2', 'ft2').toFixed(4))).toEqual(132.8266);
    expect(to(1, 'mm2', 'm2')).toEqual(0.000001);
    expect(to(123456, 'mm2', 'm2')).toEqual(0.123456);
    expect(to(123.456, 'mm2', 'm2')).toEqual(0.000123456);
    expect(to(1, 'cm2', 'm2')).toEqual(0.0001);
    expect(to(123456, 'cm2', 'm2')).toEqual(12.3456);
    expect(to(123.456, 'cm2', 'm2')).toEqual(0.0123456);
    expect(to(1, 'km2', 'm2')).toEqual(1000000);
    expect(to(123456, 'km2', 'm2')).toEqual(123456000000);
    expect(to(1, 'km2', 'm2')).toEqual(1000000);
    expect(
      Number(to(1, 'in2', 'm2').toFixed(8))).toEqual(
      0.00064516,
    );
    expect(
      Number(to(123456, 'in2', 'm2').toFixed(8))).toEqual(
      79.64887296,
    );
    expect(
      Number(to(123.456, 'in2', 'm2').toFixed(8))).toEqual(
      0.07964887,
    );
    expect(
      Number(to(1, 'ft2', 'm2').toFixed(8))).toEqual(
      0.09290304,
    );
    expect(
      Number(to(123, 'ft2', 'm2').toFixed(6))).toEqual(
      11.427074,
    );
    expect(
      Number(to(123.456, 'ft2', 'm2').toFixed(6))).toEqual(
      11.469438,
    );
    expect(
      Number(to(1, 'm2', 'mile2').toFixed(7))).toEqual(
      0.0000004,
    );
    expect(
      Number(to(123456, 'm2', 'mile2').toFixed(6))).toEqual(
      0.047667,
    );
    expect(
      Number(to(123.456, 'm2', 'mile2').toFixed(6))).toEqual(
      0.000048,
    );
    expect(
      Number(to(1000, 'mm2', 'in2').toFixed(6))).toEqual(
      1.550003,
    );
    expect(
      Number(to(123456, 'mm2', 'in2').toFixed(6))).toEqual(
      191.357183,
    );
    expect(
      Number(to(123.456, 'mm2', 'in2').toFixed(6))).toEqual(
      0.191357,
    );
    expect(
      Number(to(100, 'mm2', 'ft2').toFixed(8))).toEqual(
      0.00107639,
    );
    expect(
      Number(to(123456, 'mm2', 'ft2').toFixed(6))).toEqual(
      1.328869,
    );
    expect(
      Number(to(123.456, 'mm2', 'ft2').toFixed(6))).toEqual(
      0.001329,
    );
    expect(Number(to(1, 'ft2', 'in2').toFixed(4))).toEqual(144);

    // TODO multiple converters show 17777664 for this case but our func return 17777664.68816629
    // expect(to(123456, 'ft2', 'in2')).toEqual(17777664);

    expect(Number(to(123.456, 'ft2', 'in2').toFixed(3))).toEqual(17777.665);
    expect(
      Number(to(1, 'km2', 'mile2').toFixed(6))).toEqual(
      0.386102,
    );
    expect(
      Number(to(123456, 'km2', 'mile2').toFixed(2))).toEqual(
      47666.63,
    );
    expect(
      Number(to(123.456, 'km2', 'mile2').toFixed(4))).toEqual(
      47.6666,
    );

    expect(
      (() => {
        const m2_mm2 = to(1, 'm2', 'mm2');
        const mm2_in2 = to(m2_mm2, 'mm2', 'in2');
        const in2_km2 = to(mm2_in2, 'in2', 'km2');
        const km2_mile2 = to(in2_km2, 'km2', 'mile2');
        const mile2_cm2 = to(km2_mile2, 'mile2', 'cm2');
        const cm2_ft2 = to(mile2_cm2, 'cm2', 'ft2');
        const ft2_m2 = to(cm2_ft2, 'ft2', 'm2');
        return ft2_m2;
      })()).toEqual(1);
  });

  it('should convert force units', () => {
    expect(to(1, 'N', 'kN')).toEqual(0.001);
    expect(to(1, 'N', 'kgf')).toEqual(0.10197162129779283);
    expect(to(1, 'N', 'lbf')).toEqual(0.22480894309971047);
    expect(to(1, 'N', 'tonneForce')).toEqual(0.00010197162129779283);
    expect(to(1, 'kN', 'N')).toEqual(1000);
    expect(to(1, 'kgf', 'N')).toEqual(9.80665);
    expect(to(1, 'lbf', 'N')).toEqual(4.4482216152605);
    expect(to(1, 'klbf', 'lbf')).toEqual(1000);
    expect(to(1000, 'lbf', 'klbf')).toEqual(1);
    expect(to(1, 'klbf', 'kN')).toEqual(4.4482216152605);
    expect(to(1, 'kN', 'klbf')).toEqual(0.2248089430997105);

    expect(
      (() => {
        const N_kN = to(1, 'N', 'kN');
        const kN_kgf = to(N_kN, 'kN', 'kgf');
        const kgf_lbf = to(kN_kgf, 'kgf', 'lbf');
        const lbf_tonneForce = to(kgf_lbf, 'lbf', 'tonneForce');
        const tonneForce_klbf = to(
          lbf_tonneForce,
          'tonneForce',
          'klbf',
        );
        const klbf_N = to(tonneForce_klbf, 'klbf', 'N');
        return Number(klbf_N.toFixed(5));
      })()).toEqual(1);
  });

  it('should convert volume units', () => {
    expect(to(1, 'm3', 'bbl')).toEqual(6.289810770432105);
    expect(to(1, 'm3', 'ft3')).toEqual(35.31466672148859);
    expect(to(1, 'm3', 'l')).toEqual(1000);
    expect(Number(to(1, 'm3', 'Mm3').toFixed(6))).toEqual(0.000001);
    expect(to(1, 'm3', 'USGal')).toEqual(264.1720523581484);
    expect(
      Number(to(1, 'bbl', 'm3'))).toEqual(
      0.158987294928,
    );
    expect(
      Number(to(1, 'ft3', 'm3').toFixed(10))).toEqual(
      0.0283168466,
    );
    expect(Number(to(1, 'Mm3', 'm3').toFixed(6))).toEqual(1000000);
    expect(to(1, 'l', 'm3')).toEqual(0.001);
    expect(to(1, 'USGal', 'm3')).toEqual(0.003785411784);
    expect(
      Number(to(100, 'l', 'bbl').toFixed(6))).toEqual(
      0.628981,
    );
    expect(
      Number(to(100, 'USGal', 'l').toFixed(5))).toEqual(
      378.54118,
    );
    expect(
      Number(to(50, 'USGal', 'bbl').toFixed(6))).toEqual(
      1.190476,
    );
    expect(
      Number(to(10, 'ft3', 'bbl').toFixed(6))).toEqual(
      1.781076,
    );
    expect(
      Number(to(10, 'ft3', 'USGal'))).toEqual(
      74.8051948051948,
    );

    expect(
      (() => {
        const m3_bbl = to(1, 'm3', 'bbl');
        const bbl_ft3 = to(m3_bbl, 'bbl', 'ft3');
        const ft3_l = to(bbl_ft3, 'ft3', 'l');
        const l_USGal = to(ft3_l, 'l', 'USGal');
        const Usgal_M3 = to(l_USGal, 'USGal', 'm3');
        return Number(Usgal_M3.toFixed(6));
      })()).toEqual(1);
  });

  it('should convert pressure units', () => {
    expect(to(1, 'Pa', 'psi')).toEqual(0.00014503773773020924);
    expect(to(1, 'Pa', 'bar')).toEqual(0.00001);
    expect(to(1, 'Pa', 'kPa')).toEqual(0.001);
    expect(to(1, 'Pa', 'MPa')).toEqual(0.000001);
    expect(to(1, 'Pa', 'GPa')).toEqual(0.000000001);
    expect(to(1, 'psi', 'Pa')).toEqual(6894.757293168361);
    expect(to(1, 'bar', 'Pa')).toEqual(100000);
    expect(to(1, 'kPa', 'Pa')).toEqual(1000);
    expect(to(1, 'MPa', 'Pa')).toEqual(1000000);
    expect(to(1, 'GPa', 'Pa')).toEqual(1000000000);
    expect(to(1, 'ksi', 'psi')).toEqual(1000);
    expect(to(1000, 'psi', 'ksi')).toEqual(1);
    expect(to(1, 'psi', 'lbf/100ft2')).toEqual(14400);
    expect(to(14400, 'lbf/100ft2', 'psi')).toEqual(1);
    expect(to(1, 'Pa/C', 'Bar/C')).toEqual(0.00001);
    expect(to(1, 'Pa/C', 'psi/F')).toEqual(0.00008057652096122735);
    expect(to(1, 'Pa/C', 'psi/C')).toEqual(0.00014503773773020924);
    expect(to(1, 'psi/C', 'psi/F')).toEqual(0.5555555555555556);
    expect(to(1, 'psi/C', 'Pa/C')).toEqual(6894.757293168361);
    expect(to(1, 'psi/C', 'Bar/C')).toEqual(0.06894757293168362);
    expect(to(1, 'psi/F', 'psi/C')).toEqual(1.8);
    expect(to(1, 'psi/F', 'Bar/C')).toEqual(0.1241056312770305);
    expect(to(1, 'psi/F', 'Pa/C')).toEqual(12410.56312770305);
    expect(to(1, 'Bar/C', 'Pa/C')).toEqual(100000);
    expect(to(1, 'Bar/C', 'psi/F')).toEqual(8.057652096122736);
    expect(to(1, 'Bar/C', 'psi/C')).toEqual(14.503773773020923);

    expect(
      (() => {
        const Pa_psi = to(1, 'Pa', 'psi');
        const psi_bar = to(Pa_psi, 'psi', 'bar');
        const bar_kPa = to(psi_bar, 'bar', 'kPa');
        const kPa_MPa = to(bar_kPa, 'kPa', 'MPa');
        const MPa_GPa = to(kPa_MPa, 'MPa', 'GPa');
        const GPa_Pa = to(MPa_GPa, 'GPa', 'Pa');
        return GPa_Pa;
      })()).toEqual(1);
  });

  it('should convert oil density units', () => {
    expect(to(1, 'kg/m3', 'sg')).toEqual(0.001);
    expect(to(1, 'kg/m3', 'g/cm3')).toEqual(0.001);
    expect(to(1, 'kg/m3', 'lb/ft3')).toEqual(1 / 16.01846337);
    expect(to(1, 'kg/m3', 'lbm/ft3')).toEqual(1 / 16.01846337);
    expect(to(1, 'kg/m3', 'ppg')).toEqual(0.008345404265);
    expect(to(1, 'sg', 'kg/m3')).toEqual(1000);
    expect(to(1, 'g/cm3', 'kg/m3')).toEqual(1000);
    expect(to(1, 'lb/ft3', 'kg/m3')).toEqual(16.01846337);
    expect(to(1, 'lbm/ft3', 'kg/m3')).toEqual(16.01846337);
    expect(to(1, 'ppg', 'kg/m3')).toEqual(1 / 0.008345404265);

    expect(to(134, 'Pa/m', 'kPa/m')).toEqual(134 / 1000);
    expect(to(134, 'kPa/m', 'Pa/m')).toEqual(134 * 1000);
    expect(to(134, 'sg', 'kPa/m')).toEqual(134 * 9.81);

    expect(
      (() => {
        const kgM3_sg = to(1, 'kg/m3', 'sg');
        const sg_gCm3 = to(kgM3_sg, 'sg', 'g/cm3');
        const gCm3_lbFt3 = to(sg_gCm3, 'g/cm3', 'lb/ft3');
        const lbFt3_ppg = to(gCm3_lbFt3, 'lb/ft3', 'ppg');
        const ppg_kgM3 = to(lbFt3_ppg, 'ppg', 'kg/m3');
        return Number(ppg_kgM3.toFixed(9));
      })()).toEqual(1);
  });

  it('should convert gas density units', () => {
    expect(to(1, 'Gkg/m3', 'Gsg')).toEqual(1 / 1.225);
    expect(to(1, 'Gkg/m3', 'Glbm/ft3')).toEqual(1 / 16.0176516725);
    expect(to(1, 'Gkg/m3', 'Gppg')).toEqual(1 / 119.8659491193);
    expect(to(1, 'Gsg', 'Gkg/m3')).toEqual(1.225);
    expect(to(1, 'Glbm/ft3', 'Gkg/m3')).toEqual(16.0176516725);
    expect(to(1, 'Gppg', 'Gkg/m3')).toEqual(119.8659491193);

    expect(
      (() => {
        const GkgM3_Gsg = to(1, 'Gkg/m3', 'Gsg');
        const GlbFt3_Gppg = to(GkgM3_Gsg, 'Gsg', 'Gppg');
        const Gppg_GkgM3 = to(GlbFt3_Gppg, 'Gppg', 'Gkg/m3');
        return Number(Gppg_GkgM3.toFixed(9));
      })()).toEqual(1);

    expect(
      (() => {
        const GkgM3_Gsg = to(1, 'Gsg', 'Gkg/m3');
        const GlbFt3_Gppg = to(GkgM3_Gsg, 'Gkg/m3', 'Gppg');
        const Gppg_GkgM3 = to(GlbFt3_Gppg, 'Gppg', 'Gsg');
        return Gppg_GkgM3;
      })()).toEqual(1);
  });

  it('should convert specific heat capacity units', () => {
    expect(
      to(1, 'BTU/(Kg*K)', 'BTU/(lbm*degF)')).toEqual(
      1 / 4186.798188,
    );
    expect(
      to(1, 'BTU/(lbm*degF)', 'J/(kg*degC)')).toEqual(
      4186.798188,
    );
    expect(
      to(1, 'BTU/(lbm*degF)', 'BTU/(Kg*K)')).toEqual(
      4186.798188,
    );
  });

  it('should convert thermal conductivity units', () => {
    expect(
      to(1, 'BTU/(h*ft*degF)', 'W/(mK)')).toEqual(1.7295772056);
    expect(to(1, 'BTU/(h*ft*degF)', 'BTU/(h*ft*degF)')).toEqual(1);
    expect(to(1, 'BTU/(h*ft*degF)', 'W/(mK)')).toEqual(1.7295772056);
    expect(
      to(1, 'BTU/(h*ft*degF)', 'J/(s*m*degK)')).toEqual(1.7295772056);
    expect(to(1, 'BTU/(h*ft*degF)', 'BTU/(h*ft*degF)')).toEqual(1);
    expect(unum('5.97222222222222|E-06/degF', 'E-06/degC').toFixed(2)).toEqual('10.75');
  });

  it('should convert weight units', () => {
    expect(to(1, 'kg', 'kgf')).toEqual(1);
    expect(to(1, 'kg', 'lbf')).toEqual(2.20462262184877);
    expect(to(1, 'kg', 't')).toEqual(0.001);
    expect(to(1, 'kg', 'tonnes')).toEqual(0.001);
    expect(to(1, 'kg', 'mt')).toEqual(0.001);
    expect(to(1, 'kg', 'kip')).toEqual(0.00220462262184877);
    expect(to(1, 'kgf', 'kg')).toEqual(1);
    expect(to(1, 'lbf', 'kg')).toEqual(0.45359237);
    expect(to(1, 't', 'kg')).toEqual(1000);
    expect(to(1, 'tonnes', 'kg')).toEqual(1000);
    expect(to(1, 'mt', 'kg')).toEqual(1000);
    expect(to(1, 'kip', 'kg')).toEqual(453.59237);

    expect(
      (() => {
        const kg_kgf = to(1, 'kg', 'kgf');
        const kgf_lbf = to(kg_kgf, 'kgf', 'lbf');
        const lbf_t = to(kgf_lbf, 'lbf', 't');
        const t_mt = to(lbf_t, 't', 'mt');
        const mt_tonnes = to(t_mt, 'mt', 'tonnes');
        const tonnes_kip = to(mt_tonnes, 'tonnes', 'kip');
        const kip_kg = to(tonnes_kip, 'kip', 'kg');
        return Number(kip_kg.toFixed(6));
      })()).toEqual(1);
    expect(to(1, 'tonnes', 'lbf')).toEqual(1 * 1000 * 2.20462262184877);
    expect(to(1, 'lbf', 'tonnes')).toEqual(0.00045359237000000004);
    expect(to(1, 't', 'lbf')).toEqual(1 * 1000 * 2.20462262184877);
    expect(to(1, 'lbf', 't')).toEqual(0.00045359237000000004);
    expect(to(1, 'mt', 'lbf')).toEqual(2204.62262184877);
    expect(to(1, 'lbf', 'mt')).toEqual(0.0004535923700000012);
    expect(to(1, 'kgf', 'lbf')).toEqual(2.20462262184877);
    expect(to(1, 'lbf', 'kgf')).toEqual(1 / 2.2046226218487757);
    expect(to(1, 'kg', 'g')).toEqual(1 * 1000);
    expect(to(1, 'g', 'kg')).toEqual(1 / 1000);
    expect(to(1, 'kgf', 't')).toEqual(1 / 1000);
    expect(to(1, 't', 'kgf')).toEqual(1 * 1000);
    expect(to(1, 'kgf', 'tonnes')).toEqual(1 / 1000);
    expect(to(1, 'tonnes', 'kgf')).toEqual(1 * 1000);
  });

  it('should convert flow rate units', () => {
    expect(to(1, 'm3/s', 'ft3/s')).toEqual(35.31466672148859);
    expect(to(1, 'm3/s', 'lpm')).toEqual(60000);
    expect(to(1, 'm3/s', 'bpm')).toEqual(377.388646);
    expect(to(1, 'm3/s', 'm3/d')).toEqual(86400);
    expect(to(1, 'm3/s', 'bbl/d')).toEqual(543439.6505653338);
    expect(to(1, 'm3/s', 'ft3/d')).toEqual(3051187.204736614);
    expect(to(1, 'm3/s', 'gpm')).toEqual(15850.323141488905);
    expect(round(to(1, 'm3/s', 'MMSCFD'))).toEqual(round(1 / 0.32774128));
    expect(to(1, 'ft3/s', 'm3/s').toFixed(8)).toEqual('0.02831685');
    expect(to(1, 'lpm', 'm3/s')).toEqual(1 / 60000);
    expect(to(1, 'bpm', 'm3/s')).toEqual(1 / 377.388646);
    expect(
      to(1, 'm3/d', 'm3/s').toFixed(12)).toEqual(
      (1 / (24 * 60 * 60)).toFixed(12),
    );
    expect(to(1, 'bbl/d', 'm3/s')).toEqual(0.0000018401307283333333);
    expect(to(1, 'ft3/d', 'm3/s')).toEqual(0.00000032774128);
    expect(to(1, 'gpm', 'm3/s')).toEqual(0.0000630901964);
    expect(to(1, 'MMSCFD', 'm3/s').toFixed(8)).toEqual('0.32774128');
    expect(to(1, 'm3/min', 'm3/s').toFixed(8)).toEqual('0.01666667');
    expect(to(1, 'Mm3/d', 'm3/s')).toEqual(11.574074074074074);
    expect(to(1, 'm3/d', 'STB/d')).toEqual(6.289512957422141);
    expect(to(1, 'm3/s', 'MSm3/d').toFixed(8)).toEqual('0.08622754');
    expect(to(1, 'STB/d', 'ft3/d')).toEqual(5.614849187932627);
    expect(to(1000, 'bbl/d', 'MSm3/d')).toEqual(0.0001586699550179641);
    expect(to(50, 'm3/d', 'ft3/d')).toEqual(1765.7333360744294);
    expect(
      to(50, 'Sm3/d', 'ft3/d')).toEqual(
      1765.7333360744294,
    );
    expect(to(1, 'ft3/d', 'SCF/d')).toEqual(1);
    expect(to(1, 'SCF/d', 'bbl/d')).toEqual(0.17810760667903522);

    expect(
      (() => {
        const Mm3S_m3S = to(1, 'Mm3/d', 'm3/s');
        const m3S_ft3S = to(Mm3S_m3S, 'm3/s', 'ft3/s');
        const ft3S_lpm = to(m3S_ft3S, 'ft3/s', 'lpm');
        const lpm_bpm = to(ft3S_lpm, 'lpm', 'bpm');
        const bpm_m3D = to(lpm_bpm, 'bpm', 'm3/d');
        const m3D_bblD = to(bpm_m3D, 'm3/d', 'bbl/d');
        const bblD_ft3D = to(m3D_bblD, 'bbl/d', 'ft3/d');
        const ft3D_gpm = to(bblD_ft3D, 'ft3/d', 'gpm');
        const gpm_MMSCFD = to(ft3D_gpm, 'gpm', 'MMSCFD');
        const MMSCFD_m3S = to(gpm_MMSCFD, 'MMSCFD', 'm3/s');
        const Mm3s_MMSCFD = to(MMSCFD_m3S, 'm3/s', 'Mm3/d');
        return Number(Mm3s_MMSCFD.toFixed(6));
      })()).toEqual(1);
  });

  it('should convert turbulent skin units', () => {
    expect(to(1, '1/m3/d', '1/MMSCFD').toFixed(0)).toEqual('28317');
    expect(to(1, '1/MMSCFD', '1/m3/d')).toEqual(0.000035314662471284767);
  });

  it('should convert roughness units', () => {
    expect(to(1, 'm', 'microM')).toEqual(1000000);
    expect(to(1, 'microM', 'm')).toEqual(0.000001);
    expect(to(15, 'microM', 'in').toFixed(7)).toEqual('0.0005906');
  });

  it('should convert inflow productivity index units', () => {
    expect(to(1, 'm3/s/bar', 'Sm3/d/bar')).toEqual(86400);
    expect(to(1, 'm3/s/bar', 'STB/d/psi')).toEqual(37468.77736);
    expect(to(1, 'Sm3/d/bar', 'm3/s/bar')).toEqual(1 / 86400);
    expect(to(1, 'STB/d/psi', 'm3/s/bar')).toEqual(1 / 37468.77736);
    expect(round(to(1, 'Sm3/d/bar', 'STB/d/psi'), 3)).toEqual(0.434);

    expect(
      (() => {
        const m3sBar_Sm3dBar = to(1, 'm3/s/bar', 'Sm3/d/bar');
        const Sm3dbar_STBdPsi = to(
          m3sBar_Sm3dBar,
          'Sm3/d/bar',
          'STB/d/psi',
        );
        const STBdPsi_m3sBar = to(
          Sm3dbar_STBdPsi,
          'STB/d/psi',
          'm3/s/bar',
        );
        return Number(STBdPsi_m3sBar.toFixed(6));
      })()).toEqual(1);
  });

  it('should convert between MMSCFD and MSm3/d without horrible roundoff errors', () => {
    expect(
      round(to(to(1, 'MSm3/d', 'MMSCFD'), 'MMSCFD', 'MSm3/d'))).toEqual(1);
  });

  it('should convert duration units', () => {
    expect(to(1, 's', 'min')).toEqual(1 / 60);
    expect(to(1, 's', 'h')).toEqual(1 / 3600);
    expect(to(1, 's', 'd')).toEqual(1 / (24 * 3600));
    expect(to(1, 'min', 's')).toEqual(60);
    expect(to(1, 'h', 's')).toEqual(3600);
    expect(to(1, 'd', 's')).toEqual(24 * 3600);

    expect(
      (() => {
        const s_min = to(1, 's', 'min');
        const min_h = to(s_min, 'min', 'h');
        const h_day = to(min_h, 'h', 'd');
        const day_s = to(h_day, 'd', 's');
        return day_s;
      })()).toEqual(1);
  });

  it('should convert distance per time units', () => {
    expect(to(1, 'm/s', 'ft/s')).toEqual(1 / 0.3048);
    expect(to(1, 'm/s', 'ft/min')).toEqual(60 / 0.3048);
    expect(to(1, 'm/s', 'ft/h')).toEqual(3600 / 0.3048);
    expect(to(1, 'm/s', 'm/min')).toEqual(60);
    expect(to(1, 'm/s', 'm/h')).toEqual(3600);
    expect(to(1, 'ft/s', 'm/s')).toEqual(0.3048);
    expect(to(1, 'ft/min', 'm/s')).toEqual(0.3048 / 60);
    expect(to(1, 'ft/h', 'm/s')).toEqual(0.3048 / 3600);
    expect(to(1, 'm/min', 'm/s')).toEqual(1 / 60);
    expect(to(1, 'm/h', 'm/s')).toEqual(1 / 3600);
    expect(to(1, 'ft/h', 'ft/d')).toEqual(24);
    expect(to(1, 'ft/d', 'ft/h')).toEqual(1 / 24);
    expect(to(1, 'ft/h', 'm/h')).toEqual(0.3048);
    expect(to(1, 'm/h', 'ft/h')).toEqual(1 / 0.3048);
    expect(to(1, 'ft/d', 'm/h')).toEqual((1 * 0.3048) / 24);
    expect(to(1, 'm/h', 'ft/d')).toEqual((1 * 24) / 0.3048);
    expect(to(1, 'ft/d', 'm/d')).toEqual(1 * 0.3048);
    expect(to(1, 'm/d', 'ft/d')).toEqual(1 / 0.3048);
    expect(to(1, 'm/h', 'm/d')).toEqual(24);
    expect(to(1, 'm/d', 'm/h')).toEqual(1 / 24);

    expect(
      (() => {
        const ms_fts = to(1, 'm/s', 'ft/s');
        const fts_ftm = to(ms_fts, 'ft/s', 'ft/min');
        const ftm_fth = to(fts_ftm, 'ft/min', 'ft/h');
        const fth_mm = to(ftm_fth, 'ft/h', 'm/min');
        const mm_mh = to(fth_mm, 'm/min', 'm/h');
        const mh_ms = to(mm_mh, 'm/h', 'm/s');
        return Number(mh_ms.toFixed(8));
      })()).toEqual(1);
  });

  it('should convert power units', () => {
    expect(to(1, 'hp', 'W')).toEqual(745.699872);
    expect(to(1, 'W', 'hp')).toEqual(1 / 745.699872);
    expect(to(1, 'hp', 'kW').toFixed(9)).toEqual('0.745699872');
    expect(to(1, 'kW', 'hp')).toEqual(1 / 0.745699872);
    expect(to(1, 'W', 'BTU/h')).toEqual(3.4121416351331);
    expect(to(1, 'BTU/h', 'W').toFixed(4)).toEqual('0.2931');
    expect(to(1, 'kW', 'BTU/h')).toEqual(3412.1416351331);
    expect(to(1000, 'BTU/h', 'kW').toFixed(4)).toEqual('0.2931');
    expect(to(1, 'hp', 'BTU/h').toFixed(4)).toEqual('2544.4336');
    expect(to(42000, 'BTU/h', 'hp').toFixed(4)).toEqual('16.5066');
    expect(
      to(123456, 'MW', 'BTU/h').toFixed(3)).toEqual('421249357706.992');

    // Accept hydraulic horsepower (hhp) as a unit, even though it is really just horsepower?
    expect(to(42, 'hhp', 'hp')).toEqual(42);
    expect(to(42, 'hp', 'hhp')).toEqual(42);
  });

  it('should handle NaN input values by just letting them through', () => {
    expect(to(NaN, 'm', 'in')).toBeNaN();
  });

  it('unum should handle +/-Infinity and NaN', () => {
    expect(unum(Infinity, 'm', 'ft')).toBe(Infinity);
    expect(unum('1/0', 'm', 'ft')).toBe(Infinity);
    expect(unum(-Infinity, 'm', 'ft')).toBe(-Infinity);
    expect(unum('-1/0', 'm', 'ft')).toBe(-Infinity);
    expect(unum(Infinity, 'm', 'm')).toBe(Infinity);
    expect(unum('1/0', 'm', 'm')).toBe(Infinity);
    expect(unum(-Infinity, 'm', 'm')).toBe(-Infinity);
    expect(unum('-1/0', 'm', 'm')).toBe(-Infinity);
    expect(unum(NaN, 'm', 'ft')).toBeNaN();
    expect(unum('NaN', 'm', 'ft')).toBeNaN();
    expect(unum(NaN, 'm', 'm')).toBeNaN();
    expect(unum('NaN', 'm', 'm')).toBeNaN();
  });

  it('should support conversion with fractions', () => {
    expect(unum('1 1/2', 'in', 'in')).toEqual(1.5);
    expect(unum('1 1/2 in', 'in')).toEqual(1.5);
    expect(unum('-1 1/2 in', 'in')).toEqual(-1.5);
  });

  it('should support no conversion for same units', () => {
    expect(unum(2.2, 'megadonkeys', 'megadonkeys')).toEqual(2.2);
    expect(unum('2.2megadonkeys', 'megadonkeys')).toEqual(2.2);
  });

  it('should support conversion with numbers and strings', () => {
    expect(unum(2.2, 'kg/m3', 'sg')).toEqual(2200);
    expect(unum('2.2', 'kg/m3', 'sg')).toEqual(2200);
    expect(unum(2.2, 'sg', 'sg')).toEqual(2.2);
  });

  it('unum should ignore fromUnit if unit is part of string', () => {
    expect(unum('2200|kg/m3', 'sg', 'sg')).toEqual(2.2);
  });

  it('should convert temp gradients', () => {
    expect(unum('42|F/100ft', 'K/m')).toEqual(0.7655293088363954);
    expect(unum('1|K/m', 'C/100m')).toEqual(100);
  });

  it('should convert Gas Oil Ratio (GOR) units', () => {
    expect(unum('1|SCF/STB', 'Sm3/Sm3')).toEqual(0.178099173553719);
    expect(
      unum('1000|Sm3/Sm3', 'SCF/STB')).toEqual(5614.849187935035,);
  });

  it('should support unit numbers (unums)', () => {
    expect(unum('1m', 'cm')).toEqual(100);
    expect(unum('1', 'cm', 'm')).toEqual(100);
    expect(unum('10', 'in', 'm')).toEqual(393.7007874015748);
    expect(unum('10|m', 'in')).toEqual(393.7007874015748);
    expect(unum('1', 'bbl', 'm3')).toEqual(6.289810770432105);
    expect(unum('1', 'm3', 'bbl')).toEqual(0.158987294928);
    expect(unum('1', 'tonnes', 'kg')).toEqual(1 / 1000);
    expect(unum('1', 'kg', 'tonnes')).toEqual(1000);
    expect(unum('1', 'lbf', 'kg')).toEqual(2.20462262184877);
    expect(unum('1|tonnes', 'lbf')).toEqual(1 * 1000 * 2.20462262184877);
    expect(unum('1|lbf', 'tonnes')).toEqual(0.00045359237000000004);
    expect(unum('1|t', 'lbf')).toEqual(2204.62262184877);
    expect(unum('1|lbf', 't')).toEqual(0.00045359237000000004);
    expect(unum('1|mt', 'lbf')).toEqual(2204.62262184877);
    expect(unum('1|lbf', 'mt')).toEqual(0.0004535923700000012);
    expect(unum('1|kgf', 'lbf')).toEqual(1 * 2.20462262184877);
    expect(unum('1|lbf', 'kgf')).toEqual(0.45359237);
    expect(unum('1|kg', 'g')).toEqual(1 * 1000);
    expect(unum('1|g', 'kg')).toEqual(1 / 1000);
    expect(unum('1|kgf', 't')).toEqual(1 / 1000);
    expect(unum('1|t', 'kgf')).toEqual(1 * 1000);
    expect(unum('1|kgf', 'tonnes')).toEqual(1 / 1000);
    expect(unum('1|tonnes', 'kgf')).toEqual(1 * 1000);
    expect(unum('1|kg', 'kip')).toEqual(0.00220462262184877);
    expect(unum('1|kip', 'kg')).toEqual(1 * 453.59237);

    expect(unum('1', 'ft/s', 'm/s')).toEqual(1 / 0.3048);
    expect(unum('1', 'kg', 'mt')).toEqual(1000);
    expect(unum('1', 'tonnes', 'mt')).toEqual(1);
    expect(unum('1', 'mt', 'tonnes')).toEqual(1);
    expect(unum('51', 'kg', 'mt')).toEqual(51 * 1000);
    expect(unum('51', 'tonnes', 'mt')).toEqual(51);

    expect(unum('1', 'ppg', 'sg')).toEqual(8.345404265);
    expect(unum('1', 'sg', 'ppg')).toEqual(0.11982643000218995);
    expect(unum('1', 'kg/m3', 'sg')).toEqual(1000);

    expect(unum('1', 'psi', 'bar')).toEqual(14.503773773020923);
    expect(unum('1', 'bar', 'psi')).toEqual(0.06894757293168362);

    expect(unum('1', 'kg/m', 'ppf')).toEqual(0.45359237 / 0.3048);
    expect(unum('1', 'ppf', 'kg/m')).toEqual(1 / (0.45359237 / 0.3048));

    expect(unum('1', 'm3/s', 'lpm')).toEqual(1 / 60000);
    expect(unum('1', 'm3/s', 'bpm')).toEqual(0.002649788250386314);
    expect(unum('1', 'lpm', 'm3/s')).toEqual(60000);
    expect(unum('-10', 'lpm', 'bpm')).toEqual(-1589.8729502317883);
    expect(unum('1', 'bpm', 'lpm')).toEqual(0.006289810766666667);
    expect(unum('1', 'bpm', 'm3/s')).toEqual(377.388646);

    expect(unum('1', 'm2', 'mD')).toEqual(9.869233e-13);
    expect(unum('1', 'mD', 'm2')).toEqual(1013249965828.1448);
    expect(unum('1234', 'dyn/cm', 'mN/m')).toEqual(1234);
    expect(unum('1234', 'mN/m', 'dyn/cm')).toEqual(1234);
    expect(unum('1234|mN/m', 'N/m')).toEqual(1.234);
    expect(unum('1.234|N/m', 'mN/m')).toEqual(1234);
    expect(unum('1', 'N/m', 'dyn/cm')).toEqual(0.001);
    expect(unum('1', 'dyn/cm', 'N/m')).toEqual(1000);
    expect(unum('1000|mN/m', 'lbf/ft').toFixed(8)).toEqual('0.06852177');
    expect(unum('1000|dyn/cm', 'lbf/ft').toFixed(8)).toEqual('0.06852177');
    expect(
      round(unum('1|lbf/ft', 'mN/m'), 2)).toEqual(
      round(1000 / 0.06852177, 2),
    );
    expect(
      round(unum('1|lbf/ft', 'dyn/cm'), 2)).toEqual(
      round(1000 / 0.06852177, 2),
    );

    expect(unum('1800', 'h', 's')).toEqual(0.5);
    expect(unum('0.5', 's', 'h')).toEqual(1800);
    expect(unum('1', 'h', 'd')).toEqual(24);

    expect(unum('20|°N', '°S', '°S')).toEqual(20);
    expect(unum('20|°W', '°E', '°E')).toEqual(20);

    expect(unum('1', 'kg/m3', 'lb/ft3')).toEqual(16.01846337);
    expect(unum(1, 'BTU/(Kg*K)', 'BTU/(lbm*degF)')).toEqual(4186.798188);
    expect(
      unum(1, 'J/(s*m*degK)', 'BTU/(h*ft*degF)')).toEqual(
      1.7295772056,
    );
    expect(
      unum(1, 'J/(s*m*degK)', 'BTU/(h*ft*degF)')).toEqual(
      1.7295772056,
    );
    expect(unum('60|rpm', 'Hz')).toEqual(1);
    expect(unum('1|Hz', 'rpm')).toEqual(60);

    expect(round(unum('1|N', 'lbf'), 6)).toEqual(0.224809);
    expect(round(unum('1|lbf', 'N'), 6)).toEqual(4.448222);
    expect(unum('42|N', 'N')).toEqual(42);
    expect(unum('42|lbf', 'lbf')).toEqual(42);

    expect(() => unum('1', 'cm')).toThrowError(Error);
  });
  it('OW-19695 handle + in numbers', () => {
    expect(unum('+5', 'deg', 'deg')).toEqual(5);
    expect(unum('-5', 'deg', 'deg')).toEqual(-5);
    expect(to('+5', 'deg', 'deg')).toEqual(5);
    expect(to('-5', 'deg', 'deg')).toEqual(-5);
  });
  it('unum should return 0 for empty values', () => {
    expect(unum('|m', 'm', 'm')).toEqual(0);
  });
  it('unum should throw for non-numerical string inputs with no units separator (OW-19280)', () => {
    expect(() => unum('jalla', 'm')).toThrow('No conversions found:  jalla -> m');
  });
  it('to should throw for empty string inputs with invalid units (OW-19280)', () => {
    expect(() => to('', 'jalla', 'm')).toThrow('No conversions found:  jalla -> m');
  });
  it('to() and convertAndGetValue() should return 0 for empty values', () => {
    //for historical reasons, to() and convertAndGetValue() coalesce empty values to 0
    expect(to('', 'm', 'ft')).toEqual(0);
    expect(convertAndGetValue('', 'm', 'ft')).toEqual(0);
    expect(convertAndGetValue('|m', 'm', 'ft')).toEqual(0);
  });

  it('convertAndGetValueStrict() should preserve empty values', () => {
    //the strict variant preserves empty values (normally useful for user-input-loop, unsetting values etc.)
    expect(convertAndGetValueStrict('', 'm', 'ft')).toEqual('');
    expect(convertAndGetValueStrict('|m', 'm', 'ft')).toEqual('');
    expect(convertAndGetValueStrict(null, 'm', 'ft')).toEqual(null);
  });

  it('convertAndGetValueStrict() should preserve original input type', () => {
    //the strict variant preserves empty values (normally useful for user-input-loop, unsetting values etc.)
    expect(convertAndGetValueStrict(10, 'm', 'ft')).toEqual(3.048);
    expect(convertAndGetValueStrict('10', 'm', 'ft')).toEqual('3.048');
    expect(convertAndGetValueStrict('0.000000123', 'm', 'm')).toEqual('0.000000123');
    expect(convertAndGetValueStrict('10|ft', 'm')).toEqual('3.048');
  });

  it('should support intermediaries when converting', () => {
    expect(unum('1in', 'cm').toFixed(2)).toEqual('2.54');
    expect(unum('1cm', 'in').toFixed(2)).toEqual((1 / 2.54).toFixed(2));
    expect(unum('1h', 's')).toEqual(3600);
    expect(unum('1h', 'd').toFixed(4)).toEqual((1 / 24).toFixed(4));
  });

  it('should support exponential notation', () => {
    expect(
      unum('-2.956215116057236e-10', 'm3', 'm3')).toEqual(
      -2.956215116057236e-10,
    );
    expect(unum('6.289810770432105e10|bbl', 'm3')).toEqual(1e10);
    expect(unum('6.289810770432105e10bbl', 'm3')).toEqual(1e10);

    // Note that javascript uses '+' explicitly in exponential notation:
    expect(unum('6.289810770432105e+10|bbl', 'm3')).toEqual(1e10);
    expect(unum('6.289810770432105e+10bbl', 'm3')).toEqual(1e10);

    // We don't have units that starts with an 'e' at the moment, but, so just making it up but avoid conversion:
    expect(
      unum('6.289810770432105e+10|exadonkeys', 'exadonkeys')).toEqual(
      6.289810770432105e+10,
    );
    expect(
      unum('6.289810770432105e10exadonkeys', 'exadonkeys')).toEqual(
      6.289810770432105e+10,
    );
    expect(
      unum('6.289810770432105e+10', 'exadonkeys', 'exadonkeys')).toEqual(
      6.289810770432105e+10,
    );
    expect(
      unum('-6.289810770432105e-10', 'exadonkeys', 'exadonkeys')).toEqual(
      -6.289810770432105e-10,
    );

    // Should not accept garbage, even if there is an e in it:
    expect(isNaN(unum('-6.28981extrafluff57e-10|m', 'in')));
  });

  it('should handle NaN values by letting them through', () => {
    expect(unum('NaN', 'm3', 'm3')).toBeNaN();
    expect(unum('NaN', 'megadonkeys', 'megadonkeys')).toBeNaN();
    expect(unum(NaN, 'in', 'm')).toBeNaN();
    expect(unum('NaN', 'in', 'm')).toBeNaN();
    expect(unum('NaN|in', 'm')).toBeNaN();
  });

  it('should never return string values (some corner cases)', () => {
    expect(() => unum('400 -m', 'm')).toThrowError(Error);
    expect(() => unum('400 /m', 'm')).toThrowError(Error);
    expect(() => unum('400 1/m', 'm')).toThrowError(Error);
  });

  it('should return 0 if provided values are not convertible', (done) => {
    expect(unum('', '')).toEqual(0);
    done();
  });

  it('should support zero length values with units', (done) => {
    expect(split('m')).toEqual(['', 'm']);
    expect(unum('m', 'm')).toEqual(0);
    done();
  });

  it('should support explicit separator', (done) => {
    expect(unum('1|in', 'cm').toFixed(2)).toEqual('2.54');
    expect(unum('1|cm', 'in').toFixed(2)).toEqual((1 / 2.54).toFixed(2));
    expect(unum('1.2|', 'sg', 'sg').toFixed(2)).toEqual((1.2).toFixed(2));
    expect(unum('1.2|', '', '').toFixed(2)).toEqual((1.2).toFixed(2));
    done();
  });

  it('should convert compressibility', () => {
    expect(round(unum('1|1/psi', '1/Pa'))).toEqual(round(0.000145037737730209));
    expect(unum('1|1/bar', '1/Pa')).toEqual(1e-5);
    expect(unum('1|1/Pa', '1/bar')).toEqual(100000);
    expect(unum('1|1/Pa', '1/bar')).toEqual(100000);
    expect(unum('1|1/Pa', '1/kPa')).toEqual(1e3);
    expect(unum('1|1/Pa', '1/MPa')).toEqual(1e6);
    expect(unum('1|1/Pa', '1/GPa')).toEqual(1e9);
    expect(unum('1|1/GPa', '1/Pa')).toEqual(1e-9);
    expect(unum('1|1/MPa', '1/Pa')).toEqual(1e-6);
    expect(unum('1|1/kPa', '1/Pa')).toEqual(1e-3);
    expect(unum(unumWithUnit('1.23|1/kPa', '1/Pa'), '1/GPa')).toBeCloseTo(1.23e6, 3);
    expect(unum(unumWithUnit('0.172|1/Pa', '1/kPa'), '1/MPa')).toBeCloseTo(1.72e5, 3);
    expect(unum(unumWithUnit('3.172|1/MPa', '1/GPa'), '1/Pa')).toBeCloseTo(3.172e-6, 4);
  });

  it('should convert confidence Interval and Sigma', () => {
    expect(unum('1', 'CI', 'Sigma')).toEqual(0.6826894772086507);
    expect(unum('0.9545', 'Sigma', 'CI')).toEqual(2.000002405781065);
    expect(unum('1|Sigma', 'CI')).toEqual(0.6826894772086507);
    expect(unum('0.9545|CI', 'Sigma')).toEqual(2.000002405781065);
  });

  it('should convert angle gradients', () => {
    expect(unum('30|deg/30m', 'deg/m')).toEqual(1);
    expect(unum('10|deg/10m', 'deg/m')).toEqual(1);
    expect(unum('300|deg/30m', 'deg/10m')).toEqual(100);
    expect(unum('90|deg/30m', 'deg/m')).toEqual(3);
    expect(unum('1|deg/m', 'deg/30m')).toEqual(30);
    expect(unum('100|deg/100ft', 'deg/ft')).toEqual(1);
    expect(unum('1|deg/ft', 'deg/100ft')).toEqual(100);

    expect(unum('30|deg/30m', 'deg/100ft')).toEqual(0.3048 * 100);
    expect(
      round(unum('100|deg/100ft', 'deg/30m'))).toEqual(
      round((1 / 0.3048) * 30),
    );
  });

  it('unum should not lose precision when toUnit and fromUnit are the same', () => {
    expect(unum(1 + 7e-16, 'm', 'm')).toEqual(1.0000000000000007);
  });
  it('should support multiple separators', (done) => {
    expect(unum('1|ba|in', 'cm')).toBeNaN();
    expect(unum('1.2|-44|in', '', '')).toBeNaN();
    expect(unum('|-1|-44|in', '', '')).toBeNaN();
    expect(unum('|1.0|in', 'cm').toFixed(2)).toEqual('2.54');
    expect(unum('1|||in', 'cm').toFixed(2)).toEqual('2.54');
    expect(unum('1| |in', 'cm').toFixed(2)).toEqual('2.54');
    expect(unum('1||cm', 'in').toFixed(2)).toEqual((1 / 2.54).toFixed(2));
    expect(unum('1.2||', 'sg', 'sg').toFixed(2)).toEqual((1.2).toFixed(2));
    /*
      Since OW-19280 `unum()` is intentionally stricter about invalid numeric strings because the underlying `toNum()
      function is stricter too (we don't want strings like '13 3/8, Intermediate, Casing' to be converted to numbers).

      - Previously `unum('1.2|-44|')` returned `1.2` (based on output of `parseFloat(1.2-44)`)
      - Now it throws an error because toNum() checks `isNumeric(1.2-44)` before `parseFloat()`
        - `isNumeric(1.2-44)` returns false because it uses `isFinite()` to detect invalid number strings
        - `isFinite('1.2-44')` -> `false`
      - I think this test is very old (before we had "safe" input validation in NumberInput, UnitInput etc) and I don't
      think uses *should* expect to get a sensible numerical value out of `'1.2|-44'`. It seems like a very weird
      edge-case from the past that we can safely change now.
      - If we do want to support evaluation of expressions *and* have stricter checks for invalid number strings, we
      need a better way than `isFinite()`. We could look at packages like `math-expression-evaluator`. That's out of
      scope of OW-19280, and unlikely to be worth it for only this weird edge-case.
    */
    expect(() => unum('1.2|-44|', '', '')).toThrow('invalid number: 1.2-44, string');
    expect(unum('||6.289810770432105e+10||bbl', 'm3')).toEqual(1e10);
    expect(unum('||6.289810770432105e+10||', 'm3', 'bbl')).toEqual(1e10);
    done();
  });

  it('should give same result with aliased name', () => {
    expect(unum('1m', 'cm')).toEqual(convertAndGetValue('1m', 'cm'));
    expect(unum('1', 'cm', 'm')).toEqual(convertAndGetValue('1', 'cm', 'm'));
    expect(unum('10', 'in', 'm')).toEqual(convertAndGetValue('10', 'in', 'm'));
    expect(unum('10|m', 'in')).toEqual(convertAndGetValue('10|m', 'in'));
    expect(unum('1', 'bbl', 'm3')).toEqual(convertAndGetValue('1', 'bbl', 'm3'));
    expect(unum('1', 'm3', 'bbl')).toEqual(convertAndGetValue('1', 'm3', 'bbl'));
    expect(unum('1', 'tonnes', 'kg')).toEqual(convertAndGetValue('1', 'tonnes', 'kg'));
    expect(unum('1', 'kg', 'tonnes')).toEqual(convertAndGetValue('1', 'kg', 'tonnes'));
    expect(unum('1', 'lbf', 'kg')).toEqual(convertAndGetValue('1', 'lbf', 'kg'));
    expect(unum('1', 'lbf', 'tonnes')).toEqual(convertAndGetValue('1', 'lbf', 'tonnes'));
    expect(unum('1', 'ft/s', 'm/s')).toEqual(convertAndGetValue('1', 'ft/s', 'm/s'));
    expect(unum('1', 'kg', 'mt')).toEqual(convertAndGetValue('1', 'kg', 'mt'));
    expect(unum('1', 'tonnes', 'mt')).toEqual(convertAndGetValue('1', 'tonnes', 'mt'));
    expect(unum('1', 'mt', 'tonnes')).toEqual(convertAndGetValue('1', 'mt', 'tonnes'));
    expect(unum('51', 'kg', 'mt')).toEqual(convertAndGetValue('51', 'kg', 'mt'));
    expect(unum('51', 'tonnes', 'mt')).toEqual(convertAndGetValue('51', 'tonnes', 'mt'));
  });

  it('should convert wearFactor', () => {
    expect(unum('1|E-10/psi', 'E-14/pa')).toBeCloseTo(1.450377378, 6);
    expect(unum('1|E-10/psi', 'E-09/bar')).toBeCloseTo(1.450377378, 6);
    expect(unum('1|E-09/bar', 'E-10/psi')).toBeCloseTo(0.689475729, 5);
    expect(unum('1|E-14/pa', 'E-10/psi')).toBeCloseTo(0.689475729, 5);
    expect(unum('1|E-14/pa', 'E-09/bar')).toEqual(1);
    expect(unum('1|E-09/bar', 'E-14/pa')).toEqual(1);
    expect(unum('1|E-14/pa', 'E-14/pa')).toEqual(1);
    expect(unum('1|E-09/bar', 'E-09/bar')).toEqual(1);
    expect(unum('1|E-10/psi', 'E-10/psi')).toEqual(1);
    expect(unum('1|E-14/pa', '1/Pa')).toEqual(1e-14);
    expect(unum('1|E-09/bar', '1/bar')).toEqual(1e-9);
    expect(unum('1|E-10/psi', '1/psi')).toEqual(1e-10);
    expect(unum('1e-14|1/Pa', 'E-14/pa')).toEqual(1);
    expect(unum('1e-9|1/bar', 'E-09/bar')).toEqual(1);
    expect(unum('1e-10|1/psi', 'E-10/psi')).toEqual(1);
  });

  it('mixing Requirements conversion', () => {
    expect(to(10, 'm3/t', 'L/100kg')).toBe(1000);
    expect(to(1000, 'L/100kg', 'm3/t')).toBe(10);
  });

  it('entalphy conversion', () => {
    expect(to(10, 'J/kg', 'kJ/kg')).toBe(0.01);
    expect(to(3, 'kJ/kg', 'J/kg')).toBe(3000);
    expect(to(1, 'BTU/lbm', 'J/kg')).toBe(2326);
    expect(to(1, 'BTU/lbm', 'kJ/kg')).toBe(2.326);
  });

  describe('alt unit roundtrips', () => {
    const customRounding = {
      blowoutFlowRate: 2,
      blowoutGasFlowRate: 2,
      flowrate: 2,
      productionFlowRate: 2,
      productionFlowRateOil: 2,
      productionFlowRateGas: 2,
      injectionFlowRate: 2,
      gasVolume: 4,
      gor: 4,
      oilVolume: 7,
      torqueGradient: 6,
      volume: 4,
      kickToleranceVolume: 4,
      weight: 4,
    };
    Object.keys(ALT_UNITS).forEach((unitKey) => {
      const altUnits = ALT_UNITS[unitKey];
      it(`should convert whole roundtrip for '${unitKey}'`, () => {
        let val = 1;
        const { length } = altUnits;
        for (let i = 0; i < length; i++) {
          const iNext = (i + 1) % length;
          val = unum(val, altUnits[iNext], altUnits[i]);
        }

        // Will round the resulting roundtrip value by 8 digits if not overridden by customRounding:
        const digits = customRounding[unitKey] || 8;

        expect(round(val, digits)).toEqual(1);
      });
    });
  });

  describe('all to-from-permutations within a unitKey', () => {
    Object.keys(ALT_UNITS).forEach((unitKey) => {
      const altUnits = ALT_UNITS[unitKey];
      it(`should provide a conversion path between all units within '${unitKey}'`, () => {
        const { length } = altUnits;
        for (let i = 0; i < length; i++) {
          for (let j = i; j < length; j++) {
            // Note that this does not verify correctness of conversion,
            // only that there exist a conversion between them:
            expect(unum(1, altUnits[i], altUnits[j])).toBeTruthy();
            expect(unum(1, altUnits[j], altUnits[i])).toBeTruthy();
          }
        }
      });
    });
  });

  describe('Test aliases', () => {
    it('should work with aliases for units', () => {
      expect(round(unum('1', 'lb/ft', 'lbs/ft'))).toEqual(1.0);
      expect(round(unum('100', 'lbs/ft', 'kg/m'))).toEqual(148.8164);
      expect(round(unum('100', 'kg/m', 'lbs/ft'))).toEqual(67.1969);
      expect(round(unum('100', 'kg/m', 'lbm/ft'))).toEqual(67.1969);
    });
  });

  it('Intensity conversion work fine', () => {
    expect(unum('1|W/m2', 'hhp/in2')).toEqual(8.651738108419317e-7);
    expect(unum('1|W/m2', 'hhp/ft2')).toEqual(0.00012458503358387497);
    expect(unum('1|hhp/in2', 'W/m2')).toEqual(1155837.113269603);
    expect(unum('1|hhp/ft2', 'W/m2')).toEqual(8026.646309219521);
  });

  it('should support tables with units', (done) => {
    const tbl_m_sg = [
      ['m', 'sg'],
      [100, 1],
      [1000, 1.25],
      [2000, 1.5],
      [3000, 1.1],
    ];

    expect(convertTable(['m', 'sg'], tbl_m_sg)).toEqual(tbl_m_sg);
    expect(convertTable(['m', 'sg'], tbl_m_sg.slice(1))).toEqual(tbl_m_sg);

    expect(convertTable(['ft', 'sg'], tbl_m_sg)).toEqual([
      ['ft', 'sg'],
      [328.0839895013123, 1],
      [3280.839895013123, 1.25],
      [6561.679790026246, 1.5],
      [9842.51968503937, 1.1],
    ]);

    expect(convertTable(['ft', 'ppg'], tbl_m_sg)).toEqual([
      ['ft', 'ppg'],
      [328.0839895013123, 8.345404265],
      [3280.839895013123, 10.43175533125],
      [6561.679790026246, 12.5181063975],
      [9842.51968503937, 9.1799446915],
    ]);

    const tbl_3 = [
      ['cm', 'm', 'km'],
      ['1|m', '1', '0.1km'],
      [100, 1, 0.1],
      ['0.001km', '100cm', '100|m'],
    ];

    expect(convertTable(null, tbl_3)).toEqual([
      ['cm', 'm', 'km'],
      [100, 1, 0.1],
      [100, 1, 0.1],
      [100, 1, 0.1],
    ]);

    expect(convertTable(['cm', 'm', 'km'], tbl_3.slice(1))).toEqual([
      ['cm', 'm', 'km'],
      [100, 1, 0.1],
      [100, 1, 0.1],
      [100, 1, 0.1],
    ]);

    expect(
      convertTable(['cm', 'm', 'km'], tbl_3.slice(1), undefined, true)).toEqual(
      [
        [100, 1, 0.1],
        [100, 1, 0.1],
        [100, 1, 0.1],
      ],
    );

    const myExcelTable = [
      ['m', 'ft', 'in', 'kg/m3'],
      [15, 42, 35, 42],
      [16, 43, 36, 50],
    ];
    expect(
      convertTable(['cm', 'ft', 'cm', 'sg'], myExcelTable)).toEqual(
      [
        ['cm', 'ft', 'cm', 'sg'],
        [1500, 42, 88.9, 0.042],
        [1600, 43, 91.44, 0.05]
      ],
    );

    const myExcelTable2 = [
      ['m', 'ft', 'in', 'kg/m3'],
      [15, '42,', 35, 42],
      [16, 43, 36, 50],
    ];
    expect(
      convertTable(['cm', 'ft', 'cm', 'sg'], myExcelTable2)).toEqual(
      [
        ['cm', 'ft', 'cm', 'sg'],
        [1500, 42, 88.9, 0.042],
        [1600, 43, 91.44, 0.05]
      ],
    );
    done();
  });

  it('should convert to numbers with units', (done) => {
    expect(withUnit('', null)).toEqual('');
    expect(withUnit('0', null)).toEqual('0');
    expect(withUnit('', 'm')).toEqual('|m');
    expect(withUnit('0', 'm')).toEqual('0|m');
    expect(withUnit(0, 'm')).toEqual('0|m');
    expect(withUnit(undefined, 'm')).toEqual('|m');
    expect(withUnit(null, 'm')).toEqual('|m');
    expect(withUnit(1.123, 'm')).toEqual('1.123|m');
    done();
  });

  describe('UnitInput validation and cleaning', () => {
    it('should validate and clean', () => {
      expect(validateAndClean('123', '1234')).toEqual('1234');
      expect(validateAndClean('-123', '-1234')).toEqual('-1234');
      expect(validateAndClean('123-', '1234-')).toEqual('1234');
      expect(validateAndClean('.123', '.1234')).toEqual('.1234');
      expect(validateAndClean('123.', '1234.')).toEqual('1234.');
      expect(validateAndClean('1.2.3.', '1.2.3.4')).toEqual('1.234');
      expect(validateAndClean('1,2,3,', '1,2,3,4')).toEqual('1.234');
      expect(
        validateAndClean('123Foobar', '123Foobar4')).toEqual(
        '1234|Foobar',
      );
      expect(
        validateAndClean('123e15', '123e154')).toEqual(
        '123e154',
      );
      expect(validateAndClean('123|m', '1234')).toEqual('1234|m');
      expect(
        validateAndClean('123foo|m', '123foo4')).toEqual(
        '1234|m',
      );
      expect(
        validateAndClean('1.2.3|m', '1.2.3.4')).toEqual(
        '1.234|m',
      );
      expect(validateAndClean('123|', '1234')).toEqual('1234');
      expect(validateAndClean('-123m', '-1234')).toEqual('-1234|m');
      expect(validateAndClean('123m', '1234-')).toEqual('1234|m');
      expect(validateAndClean('123m', '123.')).toEqual('123.|m');
      expect(validateAndClean('123m', '1234')).toEqual('1234|m');
      expect(
        validateAndClean('123e15m', '123e154')).toEqual(
        '123e154|m',
      );
      expect(validateAndClean('1.2m', '1.23')).toEqual('1.23|m');
      expect(
        validateAndClean('1.2.3m', '1.2.3.4')).toEqual(
        '1.234|m',
      );

      expect(validateAndClean('123e|m', '123e')).toEqual('123|m'); //invalid previous value
      expect(validateAndClean('12.x..3e|m', '123e')).toEqual('123|m'); //invalid previous value
      expect(validateAndClean('123|m', '123.')).toEqual('123.|m');
      expect(validateAndClean('123.|m', '123.')).toEqual('123.|m');
      expect(validateAndClean('123.|m', '123.....')).toEqual('123.|m');

      expect(validateAndClean('2|m', 'e2')).toEqual('2|m');
      expect(validateAndClean('2|m', '2e')).toEqual('2|m');
      expect(validateAndClean('2|m', '2e3')).toEqual('2e3|m');
      expect(validateAndClean('6667', '2e3')).toEqual('2e3');

      expect(validateAndClean('2e50|m', '2e500')).toEqual('2e50|m');
      expect(validateAndClean('-2e50|m', '-2e500')).toEqual('-2e50|m');
      expect(validateAndClean('6667', '-2e500')).toEqual('6667');

      expect(validateAndClean('2|m', '-2')).toEqual('-2|m');
      expect(validateAndClean('2|m', '-2e3')).toEqual('-2e3|m');
      expect(validateAndClean('2|m', '2e-3')).toEqual('2e-3|m');
      expect(validateAndClean('2|m', '-2e-3')).toEqual('-2e-3|m');
      expect(validateAndClean('2|m', '-2e-3-4')).toEqual('-2e-34|m');
      expect(validateAndClean('2|m', '-2-3-4')).toEqual('-234|m');
    });
  });

  describe('unit conversion precision', () => {
    /**
     * Convert value to all possible units in its category and then to its initial unit,
     * determinants if lost precision  lower then allowed fallibility value for given unit type
     *
     * @param initialUnit initial unit which we start and end conversion with
     * @param unitsUsedInConversion used units for category
     * @param startPoint initial value in given unit
     * @param allowedFallibility allowed fallibility allowed for given unit category
     */
    const checkConversionPrecision = (initialUnit: string, categoryUnits: string[], startPoint = 1, allowedFallibility = 1e-8) => {
      let currentConvertedResult = startPoint;
      const unitsUsedInConversion = [initialUnit, ...categoryUnits, initialUnit]; // start unit is same as end unit
      for (const [index, conversion] of unitsUsedInConversion.entries()) {
        const fromUnit = conversion;
        const toUnit = unitsUsedInConversion[index + 1];
        if (toUnit) {
          currentConvertedResult = to(currentConvertedResult, fromUnit, toUnit);
        }
      }
      expect(Math.abs(startPoint - currentConvertedResult)).toBeLessThan(allowedFallibility);
    };
    it('should convert lenght units without loosing to much precision', () => checkConversionPrecision('m', ['mm', 'cm', 'km', 'ft', 'in']));
    it('should convert area units without loosing to much precision', () => checkConversionPrecision('m2', ['mm2', 'cm2',  'km2', 'in2', 'ft2', 'm2']));
    it('should convert force units without loosing to much precision', () => checkConversionPrecision('N',  ['kN', 'kgf', 'lbf', 'klbf', 'lbf'], 1, 1e-5));
    it('should convert volume units without loosing to much precision', () => checkConversionPrecision('m3', ['bbl', 'ft3', 'l', 'Mm3', 'USGal'], 1e-6));
    it('should convert pressure units without loosing to much precision', () => checkConversionPrecision('Pa', ['psi', 'bar', 'kPa', 'MPa', 'lbf/100ft2']));
    it('should convert oil density units without loosing to much precision', () => checkConversionPrecision('kg/m3',  ['sg', 'g/cm3', 'lb/ft3', 'ppg']));
    it('should convert gas density units without loosing to much precision', () => checkConversionPrecision('Gkg/m3',  ['Gsg', 'Glbm/ft3', 'Gppg']));
    it('should convert thermal conductivity units without loosing to much precision', () => checkConversionPrecision('BTU/(h*ft*degF)', ['W/(mK)', 'BTU/(h*ft*degF)', 'W/(mK)', 'J/(s*m*degK)']));
    it('should convert weight units without loosing to much precision', () => checkConversionPrecision('kg', ['kgf', 'lbf', 't', 'tonnes', 'mt', 'kip']));
    it('should convert  flow rate units without loosing to much precision', () => checkConversionPrecision('m3/s',  ['ft3/s', 'lpm', 'bpm', 'm3/d', 'bbl/d', 'ft3/d', 'gpm', 'MMSCFD', 'STB/d']));
    it('should convert  froughnessunits without loosing to much precision', () => checkConversionPrecision('m',  ['microM']));
    it('should convert inflow productivity index units without loosing to much precision', () => checkConversionPrecision('m3/s/bar', ['Sm3/d/bar', 'STB/d/psi']));
    it('should convert inflow productivity index units without loosing to much precision', () => checkConversionPrecision('m3/s/bar', ['Sm3/d/bar', 'STB/d/psi']));
    it('should convert distance per time units without loosing to much precision', () => checkConversionPrecision('m/s', ['ft/s', 'ft/min', 'ft/h', 'm/min', 'm/h', 'm/s']));
    it('should convert power units without loosing to much precision', () => checkConversionPrecision('hp',  ['W', 'kW', 'BTU/h', 'MW']));
    it('should convert heat capacity units without loosing to much precision', () => checkConversionPrecision('BTU/(Kg*K)',  ['BTU/(lbm*degF)']));
  });

  describe('convertSamePrecision', () => {
    it('Converts some common cases', () => {
      //expect(Units.convertSamePrecision('1|in')).toThrow(); just crashes..
      expect(convertSamePrecision('1 1/2|in', 'cm')).toEqual('3.81|cm');
      expect(convertSamePrecision('1|cm', 'cm')).toEqual('1|cm');
      expect(convertSamePrecision('1|in', 'cm', 1)).toEqual('3|cm');
      expect(convertSamePrecision('1|in', 'cm', 2)).toEqual('2.5|cm');
      expect(convertSamePrecision('1|in', 'cm', 8)).toEqual('2.54|cm');
      expect(convertSamePrecision('1|in', 'cm', 0)).toEqual('2.54|cm');
      expect(convertSamePrecision('1.01|in', 'cm')).toEqual('2.57|cm');
      expect(convertSamePrecision('-1.01|in', 'cm')).toEqual('-2.57|cm');
      expect(convertSamePrecision('1.02|in', 'cm')).toEqual('2.59|cm');
      expect(convertSamePrecision('0,102|in', 'cm')).toEqual('0.259|cm');
      expect(convertSamePrecision('.0102|in', 'cm')).toEqual('0.0259|cm');
      expect(convertSamePrecision('.00102|in', 'cm')).toEqual('0.00259|cm');
      expect(convertSamePrecision('102e-6|in', 'cm')).toEqual('0.000259|cm');
      expect(convertSamePrecision('10.2|in', 'cm')).toEqual('25.9|cm');
      expect(convertSamePrecision('102|in', 'cm')).toEqual('259|cm');
      expect(convertSamePrecision('1020|in', 'cm')).toEqual('2591|cm');
      expect(convertSamePrecision('10200|in', 'cm')).toEqual('25908|cm');
      expect(convertSamePrecision('102000|in', 'cm')).toEqual('259080|cm');
      expect(convertSamePrecision('102e0|in', 'cm')).toEqual('259|cm');
      expect(convertSamePrecision('102e1|in', 'cm')).toEqual('2591|cm');
      expect(convertSamePrecision('102e+2|in', 'cm')).toEqual('25908|cm');
      expect(convertSamePrecision('102e+3|in', 'cm')).toEqual('259080|cm');
      expect(convertSamePrecision('102e+4|in', 'cm')).toEqual('2590800|cm');
      expect(convertSamePrecision('102e+5|in', 'cm')).toEqual('2.59e+7|cm');
      expect(convertSamePrecision('102e-1|in', 'cm')).toEqual('25.9|cm');
      expect(convertSamePrecision('102e-2|in', 'cm')).toEqual('2.59|cm');
      expect(convertSamePrecision('102e-3|in', 'cm')).toEqual('0.259|cm');
      expect(convertSamePrecision('102e-4|in', 'cm')).toEqual('0.0259|cm');
      expect(convertSamePrecision('102e-5|in', 'cm')).toEqual('0.00259|cm');
      expect(convertSamePrecision('102e-6|in', 'cm')).toEqual('0.000259|cm');
      expect(convertSamePrecision('102e-7|in', 'cm')).toEqual('2.59e-5|cm');
      expect(convertSamePrecision('102e-8|in', 'cm')).toEqual('2.59e-6|cm');
      expect(convertSamePrecision('102.0e-8|in', 'cm')).toEqual('2.591e-6|cm');
      expect(convertSamePrecision(1, 'cm')).toEqual('1|cm');
      expect(convertSamePrecision('2.54', 'cm')).toEqual('2.54|cm');
      expect(convertSamePrecision('2.54', 'cm', 3)).toEqual('2.54|cm');
      expect(convertSamePrecision(2.54, 'cm', 1)).toEqual('3|cm');
      expect(convertSamePrecision(2.54, 'cm', 2)).toEqual('2.5|cm');
      expect(convertSamePrecision('1.025|in', 'm')).toEqual('0.02603|m');
      expect(convertSamePrecision('10.25|in', 'm')).toEqual('0.2603|m');
      expect(convertSamePrecision('102.5|in', 'm')).toEqual('2.603|m');
      // benchmark-cases
      expect(convertSamePrecision(1000, 'm')).toEqual('1000|m');
      expect(convertSamePrecision('10|m', 'in', 5)).toEqual('393.7|in');
      expect(convertSamePrecision('10.000|m', 'in')).toEqual('393.7|in');
      expect(convertSamePrecision('1000.0|m', 'in')).toEqual('39370|in');
    });
    it('No-converting benchmark', () => {
      for (let j = 1000; j < 3000; j += 0.1) {
        const o = convertSamePrecision(j, 'm');
        if (o !== j + '|m') throw new Error('convertSamePrecision');
      }
    });
    it('Default-digits benchmark', () => {
      for (let j = 10; j < 30; j += 0.001) {
        let outp = (j / 0.0254).toFixed(j < 25.4 ? 2 : 1);
        while (outp[outp.length - 1]  == '0') outp = outp.slice(0, -1);
        if (outp[outp.length - 1] == '.') outp = outp.slice(0, -1);
        if (convertSamePrecision(j.toFixed(3) + '|m', 'in', 5) !== outp + '|in') throw new Error('convertSamePrecision');
      }
    });
    it('Calc-digits benchmark with decimal output', () => {
      for (let j = 10; j < 30; j += 0.001) {
        let outp = (j / 0.0254).toFixed(j < 25.4 ? 2 : 1);
        while (outp[outp.length - 1]  == '0') outp = outp.slice(0, -1);
        if (outp[outp.length - 1] == '.') outp = outp.slice(0, -1);
        if (convertSamePrecision(j.toFixed(3) + '|m', 'in') !== outp + '|in') throw new Error('convertSamePrecision');
      }
    });
    it('Calc-digits benchmark with fixed output', () => {
      for (let j = 1000; j < 3000; j += 0.1) {
        const outp = (j / 0.0254).toFixed(0);
        if (convertSamePrecision(j.toFixed(1) + '|m', 'in') !== outp + '|in') throw new Error('convertSamePrecision');
      }
    });
  });

  describe('Testing unumWithUnit', () => {
    it('should convert and add unit', () => {
      expect(unumWithUnit(2.2, 'kg/m3', 'sg')).toEqual('2200|kg/m3');
      expect(unumWithUnit('2.2', 'kg/m3', 'sg')).toEqual('2200|kg/m3');
      expect(unumWithUnit(2.2, 'sg', 'sg')).toEqual('2.2|sg');
      expect(unumWithUnit(-2.2, 'sg', 'sg')).toEqual('-2.2|sg');
    });

    it('should convert and add unit to special numbers', () => {
      expect(unumWithUnit(NaN, 'm', 'ft')).toEqual('NaN|m');
      expect(unumWithUnit('NaN', 'm', 'ft')).toEqual('NaN|m');
      expect(unumWithUnit('NaN|km', 'm', 'ft')).toEqual('NaN|m');
      expect(unumWithUnit(Infinity, 'm', 'ft')).toEqual('Infinity|m');
      expect(unumWithUnit('Infinity', 'm', 'ft')).toEqual('Infinity|m');
      expect(unumWithUnit('Infinity|km', 'm', 'ft')).toEqual('Infinity|m');
      expect(unumWithUnit(-Infinity, 'm', 'ft')).toEqual('-Infinity|m');
      expect(unumWithUnit('-Infinity', 'm', 'ft')).toEqual('-Infinity|m');
      expect(unumWithUnit('-Infinity|km', 'm', 'ft')).toEqual('-Infinity|m');
    });
  });

  describe('Testing pretty print', () => {
    it('should return unit with value and display pretty ', () => {
      expect(withPrettyUnitLabel('1000|kg/m3')).toEqual('1000 kg/m³');
      expect(withPrettyUnitLabel('20|bar')).toEqual('20 Bar');
      expect(withPrettyUnitLabel('30|E-06/degC')).toEqual('30 10⁻⁶/°C');
      expect(withPrettyUnitLabel('40|ft3')).toEqual('40 ft³');
      expect(withPrettyUnitLabel('50|mm')).toEqual('50 mm');
      expect(withPrettyUnitLabel('60|cm2')).toEqual('60 cm²');
      expect(withPrettyUnitLabel('70|Mm3')).toEqual('70 Mm³');
      expect(withPrettyUnitLabel('80|in')).toEqual('80 in');
      expect(withPrettyUnitLabel('90|in2')).toEqual('90 in²');
      expect(withPrettyUnitLabel('1|tonnes')).toEqual('1 t');
    });
  });

  describe('Testing get quantities', () => {
    const quantities = getQuantities();
    it('should return list of all quantities ', () => {
      expect(quantities).toContain('length');
    });
  });

  describe('Testing roundNumberWithLabel', () => {
    it('should return return nice numbers with unit labels', () => {
      expect(roundNumberWithLabel('1000|kg/m3')).toEqual('1 000 kg/m³');
      expect(roundNumberWithLabel('1000.12345|kg/m3')).toEqual('1 000.12 kg/m³');
      expect(roundNumberWithLabel('1000.1284325|kg/m3')).toEqual('1 000.13 kg/m³');
      expect(roundNumberWithLabel('-999.999991|kg/m3', 5)).toEqual('-999.99999 kg/m³');
      expect(roundNumberWithLabel('-999.999999|kg/m3', 5)).toEqual('-1 000 kg/m³');
      expect(roundNumberWithLabel('|cm2')).toEqual('cm²');
      expect(roundNumberWithLabel('')).toEqual('');
      expect(roundNumberWithLabel('5')).toEqual('5');
    });
    describe('OW-12287 locale bug', () => {
      afterAll(() => {
        jest.clearAllMocks();
      });
      test('works with Norwegian locale', () => {
        /*
          Just to clarify, we are *not* displaying different locale number formats to different users
          We are just using Intl.NumberFormat to format the number (to one fixed format).
         */
        jest.spyOn(Intl, 'NumberFormat')
          .mockImplementation(
            (locale = 'no', options) => new NumberFormat(locale, options)
          );
        expect(roundNumberWithLabel('0.02')).toEqual('0.02');
      });
    });
  });
  describe('Testing unit list functionality', () => {
    it('should return list of all units with same precision ', () => {
      expect(altUnitsList('10', 'length')).toEqual(
        [['10', 'm', 'm'],
          ['1000', 'cm', 'cm'],
          ['32.8', 'ft', 'ft'],
          ['0.01', 'km', 'km'],
          ['394', 'in', 'in'],
          ['10000', 'mm', 'mm']]);
      expect(altUnitsList('10|m', 'length')).toEqual(
        [['10', 'm', 'm'],
          ['1000', 'cm', 'cm'],
          ['32.8', 'ft', 'ft'],
          ['0.01', 'km', 'km'],
          ['394', 'in', 'in'],
          ['10000', 'mm', 'mm']]);
      expect(altUnitsList('180', 'deg')).toEqual(
        [['180', 'deg', '°'],
          ['3.14', 'rad', 'rad']]);
    });
  });

  describe('Testing toBase', () => {
    it('should return numbers converted to the base unit', () => {
      expect(toBase('1', 'length')).toEqual(1);
      expect(toBase('1|m', 'length')).toEqual(1);
      expect(toBase('1|cm', 'length')).toEqual(0.01);
      expect(toBase('393.7007874015748|in', 'length')).toEqual(10);
      expect(toBase('|in', 'length')).toEqual(0);
      expect(toBase('6.289810770432105|bbl', 'volume')).toEqual(1);
      expect(toBase('1|tonnes', 'weight')).toEqual(1000);
    });
  });

  describe('Testing validateNumber', () => {
    const validPayload = {errors: undefined, valid: true};
    const invalidateUnitErrorPayload = {errors: ['Must be a numerical value'], valid: false};
    it('should return correct validations', () => {
      expect(validateNumber('1|m')).toEqual(validPayload);
      expect(validateNumber('1')).toEqual(validPayload);
      expect(validateNumber('0')).toEqual(validPayload);
      expect(validateNumber('3|%')).toEqual(validPayload);
      expect(validateNumber('-1.222')).toEqual(validPayload);
      expect(validateNumber('-1.222|°E')).toEqual(validPayload);
      expect(validateNumber('100.2315|Sm3/d/bar')).toEqual(validPayload);
      expect(validateNumber(0.22)).toEqual(validPayload);
      expect(validateNumber(-0.22)).toEqual(validPayload);
      expect(validateNumber(100023)).toEqual(validPayload);
      expect(validateNumber(1e3)).toEqual(validPayload);
      expect(validateNumber(12.234e-5)).toEqual(validPayload);
      expect(validateNumber('100,2')).toEqual(validPayload);
      expect(validateNumber('100,2|m')).toEqual(validPayload);
    });
    it('should return validation errors', () => {
      expect(validateNumber('abc')).toEqual(invalidateUnitErrorPayload);
      expect(validateNumber('123abc')).toEqual(invalidateUnitErrorPayload);
      expect(validateNumber('123abc|m')).toEqual(invalidateUnitErrorPayload);
    });
  });

  describe('converting GIS Units', () => {
    it('should convert GIS Location units', () => {
      expect(to(1, 'usft', 'm')).toEqual(0.30480060960121924);
      expect(to(1, 'lk', 'm')).toEqual(0.201168);
      expect(to(1, 'ftCla', 'm')).toEqual(0.3047972651151);
      expect(to(1, 'lkCla', 'm')).toEqual(0.2011057269733667);
      expect(to(1, 'ftGC', 'm')).toEqual(0.30479971018150875);
      expect(to(1, 'ydInd', 'm')).toEqual(0.9143985307444408);
      expect(to(1, 'ftSe', 'm')).toBeCloseTo(0.3047994715, 9);
      expect(to(1, 'ydSe', 'm')).toEqual(0.9143991154275526);
      expect(to(1, 'chSe', 'm')).toEqual(20.11676512155263);
      expect(to(1, 'chSe(T)', 'm')).toEqual(20.116756);
      expect(to(1, 'm', 'usft')).toBeCloseTo(3.280833333333333, 9);
      expect(to(1, 'm', 'lk')).toBeCloseTo(4.970969538, 9);
      expect(to(1, 'm', 'ftCla')).toBeCloseTo(3.280869333, 9);
      expect(to(1, 'm', 'lkCla')).toBeCloseTo(4.972508814, 9);
      expect(to(1, 'm', 'ftGC')).toBeCloseTo(3.280843015, 9);
      expect(to(1, 'm', 'ydInd')).toBeCloseTo(1.093615056, 9);
      expect(to(1, 'm', 'ftSe')).toBeCloseTo(3.2808455833, 9);
      expect(to(1, 'm', 'ydSe')).toBeCloseTo(1.093614356, 9);
      expect(to(1, 'm', 'chSe')).toBeCloseTo(0.049709782, 9);
      expect(to(1, 'm', 'chSe(T)')).toBeCloseTo(0.049709804, 9);
    });

    it('should convert m (meters) to other location units', () => {
      const mValue = 250;

      expect(to(mValue, 'm', 'usft')).toBeCloseTo(820.2083333333333, 5);
      expect(to(mValue, 'm', 'lk')).toBeCloseTo(1242.742384474668, 5);
      expect(to(mValue, 'm', 'ftCla')).toBeCloseTo(820.2173333333321, 5);
      expect(to(mValue, 'm', 'lkCla')).toBeCloseTo(1243.1272035982772, 5);
      expect(to(mValue, 'm', 'ftGC')).toBeCloseTo(820.2107536490917, 5);
      expect(to(mValue, 'm', 'ydInd')).toBeCloseTo(273.4037638888889, 5);
      expect(to(mValue, 'm', 'ftSe')).toBeCloseTo(820.2113958333334, 5);
      expect(to(mValue, 'm', 'ydSe')).toBeCloseTo(273.4035890696434, 5);
      expect(to(mValue, 'm', 'chSe')).toBeCloseTo(12.427445391414144, 5);
      expect(to(mValue, 'm', 'chSe(T)')).toBeCloseTo(12.427451026398094, 5);
    });

    it('should convert ft (feet) to other location units', () => {
      const ftValue = 500;

      expect(to(ftValue, 'ft', 'usft')).toBeCloseTo(499.99899999999997, 5);
      expect(to(ftValue, 'ft', 'lk')).toBeCloseTo(757.5757575757575, 5);
      expect(to(ftValue, 'ft', 'ftCla')).toBeCloseTo(500.0044863999993, 5);
      expect(to(ftValue, 'ft', 'lkCla')).toBeCloseTo(757.8103433135099, 5);
      expect(to(ftValue, 'ft', 'ftGC')).toBeCloseTo(500.0004754244863, 5);
      expect(to(ftValue, 'ft', 'ydInd')).toBeCloseTo(166.66693446666667, 5);
      expect(to(ftValue, 'ft', 'ftSe')).toBeCloseTo(500.00086690000006, 5);
      expect(to(ftValue, 'ft', 'ydSe')).toBeCloseTo(166.6668278968546, 5);
      expect(to(ftValue, 'ft', 'chSe')).toBeCloseTo(7.575770710606062, 5);
      expect(to(ftValue, 'ft', 'chSe(T)')).toBeCloseTo(7.575770710606062, 5);
    });

    it('should convert usft (us survey feet) to other location units', () => {
      const usftValue = 300;

      expect(to(usftValue, 'usft', 'ft')).toBeCloseTo(300.0006000012, 5);
      expect(to(usftValue, 'usft', 'lk')).toBeCloseTo(454.54636363818184, 5);
      expect(to(usftValue, 'usft', 'ftCla')).toBeCloseTo(300.0032918465833, 5);
      expect(to(usftValue, 'usft', 'lkCla')).toBeCloseTo(454.68711536233667, 5);
      expect(to(usftValue, 'usft', 'ftGC')).toBeCloseTo(300.0008852564623, 5);
      expect(to(usftValue, 'usft', 'ydInd')).toBeCloseTo(100.00036068072137, 5);
      expect(to(usftValue, 'usft', 'ftSe')).toBeCloseTo(300.0011201422403, 5);
      expect(to(usftValue, 'usft', 'ydSe')).toBeCloseTo(100.00029673870625, 5);
      expect(to(usftValue, 'usft', 'chSe')).toBeCloseTo(4.545471517306672, 5);
      expect(to(usftValue, 'usft', 'chSe(T)')).toBeCloseTo(4.545471517306672, 5);
    });

    it('should convert ftUS (us survey feet) to other location units', () => {
      const usftValue = 300;

      expect(to(usftValue, 'ftUS', 'ft')).toBeCloseTo(300.0006000012, 5);
      expect(to(usftValue, 'ftUS', 'lk')).toBeCloseTo(454.54636363818184, 5);
      expect(to(usftValue, 'ftUS', 'ftCla')).toBeCloseTo(300.0032918465833, 5);
      expect(to(usftValue, 'ftUS', 'lkCla')).toBeCloseTo(454.68711536233667, 5);
      expect(to(usftValue, 'ftUS', 'ftGC')).toBeCloseTo(300.0008852564623, 5);
      expect(to(usftValue, 'ftUS', 'ydInd')).toBeCloseTo(100.00036068072137, 5);
      expect(to(usftValue, 'ftUS', 'ftSe')).toBeCloseTo(300.0011201422403, 5);
      expect(to(usftValue, 'ftUS', 'ydSe')).toBeCloseTo(100.00029673870625, 5);
      expect(to(usftValue, 'ftUS', 'chSe')).toBeCloseTo(4.545471517306672, 5);
      expect(to(usftValue, 'ftUS', 'chSe(T)')).toBeCloseTo(4.545471517306672, 5);
    });

    it('should convert ydSe (British yard (Sears 1922)) to other location units', () => {
      const ydSeValue = 200;

      expect(to(ydSeValue, 'ydSe', 'lk')).toBeCloseTo(909.0900296543709, 5);
      expect(to(ydSeValue, 'ydSe', 'ftCla')).toBeCloseTo(600.0048032466759, 5);
      expect(to(ydSeValue, 'ydSe', 'lkCla')).toBeCloseTo(909.3715322673535, 5);
      expect(to(ydSeValue, 'ydSe', 'ftGC')).toBeCloseTo(599.9999900807164, 5);
      expect(to(ydSeValue, 'ydSe', 'ydInd')).toBeCloseTo(200.00012788365075, 5);
      expect(to(ydSeValue, 'ydSe', 'ftSe')).toBeCloseTo(600.0004598508787, 5);
      expect(to(ydSeValue, 'ydSe', 'chSe')).toBeCloseTo(9.090916058346647, 5);
      expect(to(ydSeValue, 'ydSe', 'chSe(T)')).toBeCloseTo(9.090916058346647, 5);
    });

    it('should convert ftSe (British foot (Sears 1922)) to other location units', () => {
      const ftSeValue = 200;

      expect(to(ftSeValue, 'ftSe', 'lk')).toBeCloseTo(303.0297776372745, 5);
      expect(to(ftSeValue, 'ftSe', 'ftCla')).toBeCloseTo(200.00144779748948, 5);
      expect(to(ftSeValue, 'ftSe', 'lkCla')).toBeCloseTo(303.1236117696858, 5);
      expect(to(ftSeValue, 'ftSe', 'ftGC')).toBeCloseTo(199.99984341006598, 5);
      expect(to(ftSeValue, 'ftSe', 'ydInd')).toBeCloseTo(66.66665820001467, 5);
      expect(to(ftSeValue, 'ftSe', 'chSe')).toBeCloseTo(3.0303030303030303, 5);
      expect(to(ftSeValue, 'ftSe', 'ydSe')).toBeCloseTo(66.66661557216375, 5);
      expect(to(ftSeValue, 'ftSe', 'chSe(T)')).toBeCloseTo(3.0303044043351344, 5);
    });

    it('should convert chSe (British chain (Sears 1922)) to other location units', () => {
      const chSeValue = 40;

      expect(to(chSeValue, 'chSe', 'lk')).toBeCloseTo(3999.9930648120235, 5);
      expect(to(chSeValue, 'chSe', 'ftCla')).toBeCloseTo(2640.0191109268612, 5);
      expect(to(chSeValue, 'chSe', 'lkCla')).toBeCloseTo(4001.2316753598525, 5);
      expect(to(chSeValue, 'chSe', 'ftGC')).toBeCloseTo(2639.997933012871, 5);
      expect(to(chSeValue, 'chSe', 'ydInd')).toBeCloseTo(879.9998882401936, 5);
      expect(to(chSeValue, 'chSe', 'ftSe')).toBeCloseTo(2640, 5);
      expect(to(chSeValue, 'chSe', 'ydSe')).toBeCloseTo(879.9993255525616, 5);
      expect(to(chSeValue, 'chSe', 'chSe(T)')).toBeCloseTo(40.000018137223776, 5);
    });

    it('should convert chSe(T) (British chain (Sears 1922 Truncated)) to other location units', () => {
      const chSeTValue = 40;

      expect(to(chSeTValue, 'chSe(T)', 'lk')).toBeCloseTo(3999.991251093613, 5);
      expect(to(chSeTValue, 'chSe(T)', 'ftCla')).toBeCloseTo(2640.017913861969, 5);
      expect(to(chSeTValue, 'chSe(T)', 'lkCla')).toBeCloseTo(4001.2298610798184, 5);
      expect(to(chSeTValue, 'chSe(T)', 'ftGC')).toBeCloseTo(2639.9967359575817, 5);
      expect(to(chSeTValue, 'chSe(T)', 'ydInd')).toBeCloseTo(879.9994892215021, 5);
      expect(to(chSeTValue, 'chSe(T)', 'ftSe')).toBeCloseTo(2639.9988029437736, 5);
      expect(to(chSeTValue, 'chSe(T)', 'ydSe')).toBeCloseTo(879.9989265341252, 5);
      expect(to(chSeTValue, 'chSe(T)', 'chSe')).toBeCloseTo(39.999981862784445, 5);
    });

    it('should convert ftGC (Gold Coast foot) to other location units', () => {
      const ftGCValue = 650;

      expect(to(ftGCValue, 'ftGC', 'lk')).toBeCloseTo(984.8475484072054, 5);
      expect(to(ftGCValue, 'ftGC', 'ftCla')).toBeCloseTo(650.0052142632089, 5);
      expect(to(ftGCValue, 'ftGC', 'lkCla')).toBeCloseTo(985.1525095763113, 5);
      expect(to(ftGCValue, 'ftGC', 'ydInd')).toBeCloseTo(216.66680878925416, 5);
      expect(to(ftGCValue, 'ftGC', 'ftSe')).toBeCloseTo(650.0005089176841, 5);
      expect(to(ftGCValue, 'ftGC', 'ydSe')).toBeCloseTo(216.66667024863023, 5);
      expect(to(ftGCValue, 'ftGC', 'chSe')).toBeCloseTo(9.848492559358851, 5);
      expect(to(ftGCValue, 'ftGC', 'chSe(T)')).toBeCloseTo(9.848492559358851, 5);
    });

    it('should convert ydInd (Indian yard) to other location units', () => {
      const ydIndValue = 150;

      expect(to(ydIndValue, 'ydInd', 'lk')).toBeCloseTo(681.8170862744876, 5);
      expect(to(ydIndValue, 'ydInd', 'ftCla')).toBeCloseTo(450.0033146946733, 5);
      expect(to(ydIndValue, 'ydInd', 'lkCla')).toBeCloseTo(682.028213099226, 5);
      expect(to(ydIndValue, 'ydInd', 'ftGC')).toBeCloseTo(449.99970482251194, 5);
      expect(to(ydIndValue, 'ydInd', 'ftSe')).toBeCloseTo(450.00005714990823, 5);
      expect(to(ydIndValue, 'ydInd', 'ydSe')).toBeCloseTo(149.99990408732327, 5);
      expect(to(ydIndValue, 'ydInd', 'chSe')).toBeCloseTo(6.818182684089519, 5);
      expect(to(ydIndValue, 'ydInd', 'chSe(T)')).toBeCloseTo(6.818182684089519, 5);
    });

    it('should do a round trip of location units', () => {
      const m_usft = to(1, 'm', 'usft');
      const usft_lk = to(m_usft, 'usft', 'lk');
      const lk_ftCla = to(usft_lk, 'lk', 'ftCla');
      const ftCla_lkCla = to(lk_ftCla, 'ftCla', 'lkCla');
      const lkCla_ftGC = to(ftCla_lkCla, 'lkCla', 'ftGC');
      const ftGC_ydInd = to(lkCla_ftGC, 'ftGC', 'ydInd');
      const ydInd_ftSe = to(ftGC_ydInd, 'ydInd', 'ftSe');
      const ftSe_ydSe = to(ydInd_ftSe, 'ftSe', 'ydSe');
      const ftSe_chSe = to(ftSe_ydSe, 'ydSe', 'chSe');
      const chSe_chSeT = to(ftSe_chSe, 'chSe', 'chSe(T)');
      const chSeT_m = to(chSe_chSeT, 'chSe(T)', 'm');
      expect(chSeT_m).toBeCloseTo(1, 7);
    });
  });
  describe('Converting time/stand units', () => {
    it('Should convert seconds/stand to other time/stand units', () => {
      const sStand = 94 / 27;
      expect(to(sStand, 's/stand', 'min/stand')).toBeCloseTo(0.0580246, 5);
      expect(to(sStand, 's/stand', 'h/stand')).toBeCloseTo(0.000967, 5);
      expect(to(sStand, 's/stand', 'd/stand')).toBeCloseTo(0.00004029, 5);
    });
    it('Should convert min/stand to other time/stand units', () => {
      const sStand = 30 / 8;
      expect(to(sStand, 'min/stand', 's/stand')).toBeCloseTo(225, 5);
      expect(to(sStand, 'min/stand', 'h/stand')).toBeCloseTo(0.0625, 5);
      expect(to(sStand, 'min/stand', 'd/stand')).toBeCloseTo(0.002604, 5);
    });
    it('Should convert hour/stand to other time/stand units', () => {
      const sStand = 2 / 27;
      expect(to(sStand, 'h/stand', 's/stand')).toBeCloseTo(266.666666, 5);
      expect(to(sStand, 'h/stand', 'min/stand')).toBeCloseTo(4.44444, 5);
      expect(to(sStand, 'h/stand', 'd/stand')).toBeCloseTo(0.003086, 5);
    });
    it('Should do a round trip of time/stand units', () => {
      const minStand = 130 / 40;
      const sStand = to(minStand, 'min/stand', 's/stand');
      const hStand = to(sStand, 's/stand', 'h/stand');
      const dStand = to(hStand, 'h/stand', 'd/stand');
      const convertedBackToMinStand = to(dStand, 'd/stand', 'min/stand');
      expect(convertedBackToMinStand).toBeCloseTo(minStand, 7);
    });
  });
});
