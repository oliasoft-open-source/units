import {
  isNumeric,
  isEmptyValueWithUnit,
  allNumbers,
  charCount,
  getNumberOfDigitsToShow,
  fraction,
  isFraction,
  asFraction,
  numFraction,
  cleanNumStr,
  cleanNum,
  erf,
  get_k_from_conf_int,
  get_conf_int_from_k,
  isNonNumerical,
  isPercentage,
  formatNumber,
  stripLeadingZeros
} from '../utils.ts';
import { toNum } from '../numbers/numbers.ts';
import { round, roundToPrecision } from '../rounding/rounding.ts';

describe('Utils', () => {
  it('isEmptyValueWithUnit() - should return true if its empty with unit', () => {
    expect(isEmptyValueWithUnit('|m')).toEqual(true);
    expect(isEmptyValueWithUnit(NaN)).toEqual(false);
    expect(isEmptyValueWithUnit('m')).toEqual(false);
  });
  it('isNumeric() - should return true if it is number', () => {
    expect(isNumeric(1)).toEqual(true);
    expect(isNumeric(0)).toEqual(true);
    expect(isNumeric(-1.12345)).toEqual(true);
    expect(isNumeric(1e20)).toEqual(true);
    expect(isNumeric('1e20')).toEqual(true);
    expect(isNumeric('e20')).toEqual(false);
    expect(isNumeric('1.')).toEqual(true);
    expect(isNumeric('20qwe')).toEqual(false);
    expect(isNumeric('1. 3253465346367747.2315235543€')).toEqual(false);
    expect(isNumeric('13 3/8, Intermediate, Casing')).toEqual(false); // Test case for OW-19280
    expect(isNumeric(NaN)).toEqual(false);
    expect(isNumeric('NaN')).toEqual(false);
    expect(isNumeric(undefined)).toEqual(false);
    expect(isNumeric(null)).toEqual(false);
    expect(isNumeric({})).toEqual(false);
    expect(isNumeric([])).toEqual(false);
    expect(isNumeric(Infinity)).toEqual(false);
    expect(isNumeric(-Infinity)).toEqual(false);
    expect(isNumeric('Infinity')).toEqual(false);
    expect(isNumeric('-Infinity')).toEqual(false);
    expect(isNumeric('124e-02')).toEqual(true);
    expect(isNumeric('1/2e+4')).toEqual(true);
    expect(isNumeric('1/2')).toEqual(true);
    expect(isNumeric('1/0')).toEqual(true);
    expect(isNumeric('1 1/2')).toEqual(true);
    expect(isNumeric('1.2.3')).toEqual(false);
    expect(isNumeric('123e')).toEqual(false);
    expect(isNumeric(' ')).toEqual(false);
    /*
      OW-19280: we *don't* generally support evaluation of maths expressions in input strings (except fraction strings)
      If needed in the future, we could consider a package like math-expression-evaluator
      The current implementation relies on `isFinite()` to detect invalid numerical string inputs
    */
    expect(isNumeric('1.2|-44|')).toEqual(false);
    expect(isNumeric('1.2-44')).toEqual(false);
    expect(isNumeric('1.2*3')).toEqual(false);
  });
  it('isNonNumerical() - should return true if it is not number', () => {
    /*
      These tests were back-fitted (missing from original implementation)
      isNonNumerical used to have a different implementation, but we simplified this to !isNumeric()
     */
    expect(isNonNumerical(1)).toEqual(false);
    expect(isNonNumerical(0)).toEqual(false);
    expect(isNonNumerical(-1.12345)).toEqual(false);
    expect(isNonNumerical(1e20)).toEqual(false);
    expect(isNonNumerical('1e20')).toEqual(false);
    expect(isNonNumerical('e20')).toEqual(true);
    expect(isNonNumerical('1.')).toEqual(false);
    expect(isNonNumerical('20qwe')).toEqual(true);
    expect(isNonNumerical('1. 3253465346367747.2315235543€')).toEqual(true);
    expect(isNonNumerical('13 3/8, Intermediate, Casing')).toEqual(true); // Test case for OW-19280
    expect(isNonNumerical(NaN)).toEqual(true);
    expect(isNonNumerical('NaN')).toEqual(true);
    expect(isNonNumerical(undefined)).toEqual(true);
    expect(isNonNumerical(null)).toEqual(true);
    expect(isNonNumerical({})).toEqual(true);
    expect(isNonNumerical([])).toEqual(true);
    expect(isNonNumerical(Infinity)).toEqual(true);
    expect(isNonNumerical(-Infinity)).toEqual(true);
    expect(isNonNumerical('Infinity')).toEqual(true);
    expect(isNonNumerical('-Infinity')).toEqual(true);
    expect(isNonNumerical('124e-02')).toEqual(false);
    expect(isNonNumerical('1/2')).toEqual(false);
    expect(isNonNumerical('1/0')).toEqual(false);
    expect(isNonNumerical('1 1/2')).toEqual(false);
    expect(isNonNumerical('1.2.3')).toEqual(true);
    expect(isNonNumerical('123e')).toEqual(true);
    expect(isNonNumerical('')).toEqual(true);
    expect(isNonNumerical(' ')).toEqual(true);
  });
  it('allNumbers() - should return true if all numbers in array are numeric', () => {
    expect(allNumbers([1, 2, 1.2, 5])).toEqual(true);
    expect(allNumbers([1, 1, '1'])).toEqual(false);
    expect(allNumbers(['1.1', 1, 2, 3, 1.2])).toEqual(false);
    expect(allNumbers([1, 1, 2, NaN, 1.2])).toEqual(true);
    expect(allNumbers([1, 1, 2, Infinity, 1.2])).toEqual(true);
  });
  it('charCount() - should return number of counted characters in string', () => {
    expect(charCount('1', '1.1')).toEqual(2);
    expect(charCount(1, '1.1')).toEqual(2);
    expect(charCount(0, '100000.123')).toEqual(5);
  });
  it('getNumberOfDigitsToShow() - should return number of digits for set number', () => {
    expect(getNumberOfDigitsToShow('1')).toEqual(4);
    expect(getNumberOfDigitsToShow(0.1)).toEqual(4);
    expect(getNumberOfDigitsToShow(1.1)).toEqual(4);
    expect(getNumberOfDigitsToShow(4.2e-8)).toEqual(11);
    expect(getNumberOfDigitsToShow('100000.123')).toEqual(4);
    expect(getNumberOfDigitsToShow('100000.123', 3)).toEqual(3);
    expect(getNumberOfDigitsToShow(0)).toEqual(4);
    expect(getNumberOfDigitsToShow(109e-9)).toEqual(10);
  });
  it('fraction() - should return number after converting from fraction string', () => {
    expect(fraction('1/3')).toBeCloseTo(0.333);
    expect(fraction('1/10')).toEqual(0.1);
    expect(fraction('13/0')).toEqual(Infinity);
    expect(fraction('13e2b')).toEqual(NaN);
    expect(fraction(null)).toEqual(NaN);
    expect(fraction(undefined)).toEqual(NaN);
    expect(fraction([1, 2])).toEqual(NaN);
    expect(fraction('')).toEqual(NaN);
  });
  it('isFraction() - checks if a value can be parsed as a fraction', () => {
    expect(isFraction('1/4')).toBe(true);
    expect(isFraction('-1/3')).toBe(true);
    expect(isFraction('2  1/3')).toBe(true);
    expect(isFraction(' 12    1/5 ')).toBe(true);
    expect(isFraction('1/0')).toBe(true);
    expect(isFraction(' 12    1/5/4 ')).toBe(false);
    expect(isFraction('12.12 / 4')).toBe(false);
    expect(isFraction('0.5')).toBe(false);
    expect(isFraction('0,5')).toBe(false);
    expect(isFraction('13 3/8, Intermediate, Casing')).toBe(false);
    expect(isFraction('1/ ')).toBe(false);
    expect(isFraction('/2')).toBe(false);
    expect(isFraction(null)).toBe(false);
    expect(isFraction(undefined)).toBe(false);
    expect(isFraction({})).toBe(false);
    expect(isFraction(NaN)).toBe(false);
    expect(isFraction(Infinity)).toBe(false);
    expect(isFraction(0)).toBe(false);
    expect(isFraction(123)).toBe(false);
    expect(isFraction(-123.1234)).toBe(false);
    expect(isFraction(1 / 2)).toBe(false); // 1 / 2 (number type) converts to 0.5 before function call (expected)
  });
  it('asFraction() - should return string in fractional format', () => {
    expect(asFraction('')).toEqual('0');
    expect(asFraction('0.1')).toEqual('1/10');
  });
  it('numFraction() - should return number(decimal) format of fraction', () => {
    expect(numFraction('')).toEqual('');
    expect(numFraction('1/10')).toEqual(0.1);
    expect(numFraction('1/0')).toEqual(Infinity);
    expect(numFraction('-1/0')).toEqual(-Infinity);
    expect(numFraction(null)).toEqual(null);
    expect(numFraction(undefined)).toEqual(undefined);
    expect(numFraction(Infinity)).toEqual(Infinity);
    expect(numFraction(-Infinity)).toEqual(-Infinity);
    expect(numFraction(NaN)).toBeNaN();
    expect(numFraction([1, 2])).toEqual([1, 2]);
  });
  it('cleanNumStr() - should return cleaned number string', () => {
    expect(cleanNumStr('.1')).toEqual('0.1');
    expect(cleanNumStr(',1')).toEqual('0.1');
    expect(cleanNumStr('1000,000.1')).toEqual('1000000.1');
    expect(cleanNumStr('1000.000,1')).toEqual('1000000.1');
    expect(cleanNumStr('.,1')).toEqual('0.1');
    expect(cleanNumStr('.,.,1')).toEqual('1');
    expect(cleanNumStr('1000,000')).toEqual('1000.000');
    expect(cleanNumStr('1000,000,000')).toEqual('1000000000');
    expect(cleanNumStr('1000,000,000.1')).toEqual('1000000000.1');
    expect(cleanNumStr('1000,000,000.1.1')).toEqual('100000000011');
    expect(cleanNumStr('1000.000.000')).toEqual('1000000000');
    expect(cleanNumStr('1/10')).toEqual('1/10');
    expect(cleanNumStr('1 10')).toEqual('110');
  });
  it('OW-19695 handle + in numbers', () => {
    /*
      - cleanNumStr() should preserve `+` characters to not interfere with typing valid values in inputs
    */
    expect(cleanNumStr('+5')).toEqual('+5');
    expect(cleanNumStr('-5')).toEqual('-5');
  });
  it('cleanNumStr() - should strip tabs, EOL, trailing and leading spaces', () => {
    expect(cleanNumStr('     1000.000         ')).toEqual('1000.000');
    expect(cleanNumStr('     10   00.0 0 0         ')).toEqual('1000.000');
    expect(cleanNumStr('1000.000\t')).toEqual('1000.000');
    expect(cleanNumStr('100\n0.000')).toEqual('1000.000');
    expect(cleanNumStr('100\n0.00\n0')).toEqual('1000.000');
    expect(cleanNumStr('100\r0.000')).toEqual('1000.000');
    expect(cleanNumStr('100\r\n0.000')).toEqual('1000.000');
  });
  it('cleanNum() - should return cleaned number string', () => {
    expect(cleanNum(1)).toEqual(1);
    expect(cleanNum(',1')).toEqual(0.1);
  });
  it('stripLeadingZeros() - should numbers without leading zeros', () => {
    expect(stripLeadingZeros('')).toEqual('');
    expect(stripLeadingZeros('0')).toEqual('0');
    expect(stripLeadingZeros('00')).toEqual('0');
    expect(stripLeadingZeros('00.00')).toEqual('0.00');
    expect(stripLeadingZeros('00,00')).toEqual('0,00');
    expect(stripLeadingZeros('000000123')).toEqual('123');
    expect(stripLeadingZeros('000.123')).toEqual('0.123');
    expect(stripLeadingZeros('00123.12300010000')).toEqual('123.12300010000');
    expect(stripLeadingZeros('1.2e-4')).toEqual('1.2e-4');
    expect(stripLeadingZeros('0001.2e-4')).toEqual('1.2e-4');
    expect(stripLeadingZeros('-000000.0001')).toEqual('-0.0001');
    expect(stripLeadingZeros('00BAD12.45')).toEqual('0BAD12.45');
    expect(stripLeadingZeros('BAD_VALUE')).toEqual('BAD_VALUE');
  });
  it('toNum() - should return cleaned number string', () => {
    expect(toNum(1)).toEqual(1);
    expect(toNum(',1')).toEqual(0.1);
    expect(toNum(null)).toEqual(null);
    expect(toNum(undefined)).toEqual(undefined);
    expect(toNum('')).toEqual('');
    expect(toNum([1, 2])).toEqual([1, 2]);
    expect(toNum({a: 'abc'})).toEqual({a: 'abc'});
    expect(toNum('1.')).toEqual('1.');
    expect(toNum('1,')).toEqual('1,');
    expect(toNum('1/10')).toEqual(0.1);
    expect(toNum(NaN)).toEqual(NaN);
    expect(toNum(0.05, 0.05, 0.1)).toEqual(0.1);
    expect(toNum(0.1)).toEqual(0.1);
  });
  it('should support sigma / conf. interval conversion', () => {
    expect(round(erf(0.1))).toEqual(0.1125);
    expect(round(erf(-0.1))).toEqual(-0.1125);
    expect(round(get_k_from_conf_int(0.97))).toEqual(2.1701);
    expect(round(get_conf_int_from_k(3))).toEqual(0.9973);
    expect(round(get_conf_int_from_k(2))).toEqual(0.9545);
    expect(round(get_conf_int_from_k(1))).toEqual(0.6827);
    expect(round(get_k_from_conf_int(0.6827))).toEqual(1);
  });
  it('isPercentage() - should decided whether the value is a percentage with prosent', () => {
    expect(isPercentage('20%')).toBe(true);
    expect(isPercentage('2.7%')).toBe(true);
    expect(isPercentage('2,7%')).toBe(false);
    expect(isPercentage('d%')).toBe(false);
    expect(isPercentage('4%7')).toBe(false);
    expect(isPercentage('')).toBe(false);
    expect(isPercentage('%')).toBe(false);
    expect(isPercentage(20)).toBe(false);
    expect(isPercentage('20')).toBe(false);
  });

  it('formatNumber() (deprecated, should use displayNumber instead) - should display numbers for human-friendly labels', () => {
    expect(formatNumber(1234.56)).toBe('1 234.56');
    expect(formatNumber('1234.56')).toBe('1 234.56');
    expect(formatNumber('1234.56|m')).toBe('1 234.56');
    expect(formatNumber(null)).toBe('');
    expect(formatNumber(undefined)).toBe('');
  });
  it('formatNumber() (deprecated, should use displayNumber instead) - displays in scientific notation when appropriate', () => {
    // By default >1e7 or <1e-4 displays in scientific notation
    expect(formatNumber(9500.123)).toBe('9 500.123');
    expect(formatNumber(10500.123)).toBe('10 500.123');
    expect(formatNumber(10500.12345678)).toBe('10 500.12345678');
    expect(formatNumber(11_050_000.12345678)).toBe('1.105000012345678·10⁷');
    expect(formatNumber(0.0004)).toBe('0.0004');
    expect(formatNumber(0.00004)).toBe('4·10⁻⁵');
    expect(formatNumber(0.0000123456)).toBe('1.23456·10⁻⁵');

    //can round the coefficient by composing with round:
    expect(formatNumber(roundToPrecision(11_050_000.12345678))).toBe('1.105·10⁷');

    //can configure the mix/max thresholds for showing scientific notation
    expect(
      formatNumber(1230000.123)
    ).toBe('1 230 000.123');
    expect(
      formatNumber(12300000.123)
    ).toBe('1.2300000123·10⁷');
    expect(
      formatNumber(1.124e-9)
    ).toBe('1.124·10⁻⁹');

    //can configure to _only_ show (all values) in scientific notation
    expect(formatNumber('10500.12345678')).toBe(
      '10 500.12345678'
    );
  });
  describe('Test garbage values inputs', () => {
    const values = [
      '-6.28981extrafluff57e-10',
      '130 53#0/6sdsadsa0',
      '13223ddddaaa23.324',
      '123.32asdasds4',
      '12131$#$@23.32asdasds4',
    ];
    values.forEach((value) => {
      it('should check the value and return NaN if value is garbage (non numerical)', () => {
        expect(isNonNumerical(value)).toBeTruthy();
      });
    });
  });
});
