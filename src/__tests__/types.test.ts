import {
  round,
  roundToFixed,
  roundToPrecision,
  roundToDecimalPrecision,
  roundByMagnitude,
  roundByMagnitudeToFixed,
} from '../rounding/rounding.ts';
import {
  displayNumber,
  displayNumberToFixed,
} from '../numbers/display-number.ts';
import {
  toNum
} from '../numbers/numbers.ts';
import {
  to,
  unum,
  convertAndGetValue,
  checkAndCleanDecimalComma,
  roundNumberWithLabel,
  toBase,
  validateAndClean,
  getValue,
  convertSamePrecision,
  withPrettyUnitLabel
} from '../units.ts';
import {
  cleanNumStr,
  cleanNum,
  fraction,
  asFraction
} from '../utils.ts';

describe('Type assertions', () => {
  it('return types', () => {
    expect(typeof getValue('123|m')).toBe('string');
    expect(typeof to(123, 'm', 'ft')).toBe('number');
    expect(typeof to('123', 'm', 'ft')).toBe('number');
    expect(typeof toBase('123|ft', 'length')).toBe('number');
    expect(typeof unum(123, 'm', 'ft')).toBe('number');
    expect(typeof unum('123', 'm', 'ft')).toBe('number');
    expect(typeof convertAndGetValue(123, 'm', 'ft')).toBe('number');
    expect(typeof convertAndGetValue('123', 'm', 'ft')).toBe('number');
    expect(typeof convertSamePrecision('1|in', 'cm')).toBe('string');
    expect(typeof convertSamePrecision(123, 'cm')).toBe('string');

    expect(typeof checkAndCleanDecimalComma(123.234)).toBe('number');
    expect(typeof checkAndCleanDecimalComma('123,45')).toBe('string');
    expect(typeof validateAndClean('123|m', '1234')).toBe('string');

    expect(typeof toNum(123)).toBe('number');
    expect(typeof toNum('123')).toBe('number');
    expect(typeof toNum('123|m')).toBe('number');

    expect(typeof cleanNum(123)).toBe('number');
    expect(typeof cleanNum('123')).toBe('number');

    expect(typeof cleanNumStr(123)).toBe('string');
    expect(typeof cleanNumStr('123')).toBe('string');

    //rounding
    expect(typeof round(123)).toEqual('number');
    expect(typeof round('123')).toEqual('string');
    expect(typeof roundToFixed(123)).toEqual('string'); //always string
    expect(typeof roundToPrecision('123')).toEqual('string');
    expect(typeof roundToDecimalPrecision(123)).toEqual('number');
    expect(typeof roundToDecimalPrecision('123')).toEqual('string');
    expect(typeof roundByMagnitude(123)).toEqual('number');
    expect(typeof roundByMagnitude('123')).toEqual('string');
    expect(typeof roundByMagnitudeToFixed(123)).toEqual('string'); //always string
    expect(typeof roundByMagnitudeToFixed('123')).toEqual('string');
    expect(typeof roundNumberWithLabel('123|m')).toEqual('string'); //only accepts string

    //fraction
    expect(typeof fraction(1 / 3)).toEqual('number');
    expect(typeof fraction('1/3')).toEqual('number');
    expect(typeof asFraction(1 / 3)).toEqual('string');
    expect(typeof asFraction('1/3')).toEqual('string');

    //display
    expect(typeof displayNumber('1|in')).toBe('string');
    expect(typeof displayNumber(123)).toBe('string');
    expect(typeof displayNumberToFixed('1|in')).toBe('string');
    expect(typeof withPrettyUnitLabel('123|m')).toBe('string');
  });
});
