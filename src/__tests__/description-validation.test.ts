import { ALT_UNITS, LABELS, QUANTITIES_DESCRIPTION, UNITS_DESCRIPTION } from '../constants.ts';

const quantitiesKeys: string[] = Object.keys(ALT_UNITS);
const allUnits: string[] = Object.values(ALT_UNITS).flat() as string[];

describe('Validation of Constants for Quantities, Units, Descriptions, and Labels', () => {
  describe('Quantities Descriptions', () => {
    quantitiesKeys.forEach(quantity => {
      it(`should have a description for ${quantity}`, () => {
        expect(QUANTITIES_DESCRIPTION[quantity]).toBeDefined();
        expect(QUANTITIES_DESCRIPTION[quantity]).not.toBe('');
      });
    });
  });

  describe('Units Descriptions and Labels', () => {
    allUnits.forEach(unit => {
      it(`should have a description ${unit}`, () => {
        expect(UNITS_DESCRIPTION[unit]).toBeDefined();
        expect(UNITS_DESCRIPTION[unit]).not.toBe('');
      });

      it(`should have a label for ${unit}`, () => {
        expect(LABELS[unit]).toBeDefined();
        expect(LABELS[unit]).not.toBe('');
      });
    });
  });
});
