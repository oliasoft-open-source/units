import {
  to,
} from '../units.ts';
import { ALT_UNITS } from '../constants.ts';
import { isCloseTo } from '../comparison/comparison.ts';

/*
  "Smoke test" for two-way reconversion of unit pairs (A->B->A)

  Checks reconversions of all relevant unit pairs in this repo to make sure the delta is not unreasonable (i.e. within
  the limits of normal floating-point noise)

  The purpose of this test is to catch "obvious mistakes" when any new units are added to the repo (it does not
  check any specific unit in detail - devs can add detailed tests for specific units when needed)
*/

/*
  Test value for checking two-way reconversion of units (A->B->A)
  There is no particular meaning behind the value chosen (just a few significant digits)
  Note: some unit pairs (e.g. CI/Sigma) have implicit rules on allowed value size (less than 1)
*/
const TEST_VALUE = 0.314159; //worth trying different input values for reconversion?

/*
  Maximum diff allowed two-way reconversion of units (A->B->A)
  Needs to be small enough to catch formula mistakes when adding new units, but large enough for floating-point noise
*/
const MAX_DIFF_THRESHOLD = 1e-12;

const getCombinations = (units: string[]) => {
  const combinations = [];
  for (let i = 0; i < units.length; i++) {
    for (let j = 0; j < units.length; j++) {
      if (i !== j) {
        combinations.push([units[i], units[j]]);
      }
    }
  }
  return combinations;
};

const failedConversions: Set<{details: string, delta: number}> = new Set();

describe('All compatible units should support two-way reconversion', () => {
  it('Values should be very close when doing two-way reconversion (within limits of floating-point tolerance)', () => {
    Object.entries(ALT_UNITS).forEach(([_quantity, units]) => {
      const combinations = getCombinations(units);
      combinations.forEach(([fromUnit, toUnit]) => {
        const converted = to(TEST_VALUE, fromUnit, toUnit);
        const convertedBack = to(converted, toUnit, fromUnit);
        const reconversionIsCloseEqual = isCloseTo(
          convertedBack,
          TEST_VALUE,
          {absoluteDiff: MAX_DIFF_THRESHOLD}
        );
        if (!reconversionIsCloseEqual) {
          const delta = Math.abs(convertedBack - TEST_VALUE);
          const convertedLabel = `to(${TEST_VALUE}, ${fromUnit}, ${toUnit})`;
          const details = `to((${convertedLabel}), ${toUnit}, ${fromUnit})`;
          failedConversions.add({details, delta});
        }
      });
    });
    const results = Array.from(failedConversions);
    const sortedResults = results.sort((a, b) => b.delta - a.delta);
    if (sortedResults.length) {
      console.error('Two-way conversions of some unit functions have large deltas (reconversion should produce same value)');
      const combinedResults = sortedResults
        .map(({details, delta}) => `${details} -> Delta: ${delta}`);
      const flattenedResults = combinedResults.join('\n');
      console.log(flattenedResults);
    }
    expect(sortedResults.length).toBe(0);
  });
});
