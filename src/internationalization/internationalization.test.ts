import {
  convertNumberToLocale,
  convertArrayToLocale,
  convertNumberFromLocale,
  convertArrayFromLocale
} from './internationalization.ts';
import {
  getLocaleFormatFromString,
  getLocaleFormat,
} from './utils.ts';

const { NumberFormat } = Intl;

jest.spyOn(Intl, 'NumberFormat')
  .mockImplementation(
    (locale = 'no', options) => new NumberFormat(locale, options)
  );

describe('Internationalization', () => {
  it('should convert machine numbers to locale numbers with a given country code (US)', () => {
    const machineNumbers = [123456.789, 987654, 0.123456];
    const localeNumbersUS = ['123,456.789', '987,654', '0.123456'];
    const fallbackLocale = ['123\xa0456,789', '987\xa0654', '0,123456'];

    //thousand operator
    expect(convertNumberToLocale(123456, 'en')).toEqual('123,456');

    //decimal operator
    expect(convertNumberToLocale(0.156, 'en')).toEqual('0.156');

    //thousand and decimal operators
    expect(convertNumberToLocale(123456.789, 'en')).toEqual('123,456.789');

    //long decimal digits
    expect(convertNumberToLocale(0.123456789, 'en')).toEqual('0.123456789');

    //converting array
    expect(convertArrayToLocale(machineNumbers, 'en')).toEqual(localeNumbersUS);

    //fallback to default browser localisation setting
    expect(convertNumberToLocale(123456.789, 'fail')).toEqual('123\xa0456,789');
    expect(convertArrayToLocale(machineNumbers, 'fail')).toEqual(fallbackLocale);
  });

  it('should convert machine numbers to locale numbers with default browser settings (norwegian)', () => {
    const machineNumbers = [123456.789, 987654, 0.123456];
    const localeNumbers = ['123\xa0456,789', '987\xa0654', '0,123456'];

    //thousand operator
    expect(convertNumberToLocale(123456)).toEqual('123\xa0456');

    //decimal operator
    expect(convertNumberToLocale(0.156)).toEqual('0,156');

    //thousand and decimal operators
    expect(convertNumberToLocale(123456.789)).toEqual('123\xa0456,789');

    //long decimal digits
    expect(convertNumberToLocale(0.123456789)).toEqual('0,123456789');

    //converting array
    expect(convertArrayToLocale(machineNumbers)).toEqual(localeNumbers);
  });

  it('should convert locale numbers to machine numbers with a given country code (US)', () => {
    const localeNumbers = ['123,456.789', '987,654.321', '-0.12345'];
    const machineNumbers = [123456.789, 987654.321, -0.12345];

    //thousand operator
    expect(convertNumberFromLocale('123,456', 'en')).toEqual(123456);

    //decimal operator
    expect(convertNumberFromLocale('0.156', 'en')).toEqual(0.156);

    //thousand and decimal operators
    expect(convertNumberFromLocale('123,456.789', 'en')).toEqual(123456.789);

    //long decimal digits
    expect(convertNumberFromLocale('0.123456789', 'en')).toEqual(0.123456789);

    //converting array
    expect(convertArrayFromLocale(localeNumbers, 'en')).toEqual(machineNumbers);
  });

  it('should convert locale numbers to machine numbers with default browser settings (norwegian)', () => {
    const localeNumbers = ['123 456,789', '987 654,321', '0,12345'];
    const machineNumbers = [123456.789, 987654.321, 0.12345];

    //thousand operator
    expect(convertNumberFromLocale('123 456')).toEqual(123456);

    //decimal operator
    expect(convertNumberFromLocale('0.156')).toEqual(0.156);

    //thousand and decimal operators
    expect(convertNumberFromLocale('123,456.789')).toEqual(123456.789);

    //long decimal digits
    expect(convertNumberFromLocale('0.123456789')).toEqual(0.123456789);

    //converting array
    expect(convertArrayFromLocale(localeNumbers)).toEqual(machineNumbers);
  });

  it.skip('should return the locale format for a given country code', () => {
    expect(getLocaleFormat('nb')).toMatchObject(
      {decimal: ',', thousands: ' ', useGrouping: true});
    expect(getLocaleFormat('no-NOB')).toMatchObject(
      {decimal: ',', thousands: ' ',  useGrouping: true});
    expect(getLocaleFormat('en')).toMatchObject(
      {decimal: '.', thousands: ',',  useGrouping: true});
    expect(getLocaleFormat('it')).toMatchObject(
      {decimal: ',', thousands: '.',  useGrouping: true});
    expect(getLocaleFormat('xxxxxx')).toMatchObject(
      {thousands: ',', decimal: '.',  useGrouping: true});
  });

  it('should return the locale format for a given number string', () => {
    expect(getLocaleFormatFromString('324,322')).toEqual(
      {thousands: ',', decimal: ',', useGrouping: true});
    expect(getLocaleFormatFromString('324.1224,322')).toEqual(
      {thousands: '.', decimal: ',', useGrouping: true});
    expect(getLocaleFormatFromString('324 322, 133')).toEqual(
      {thousands: ' ', decimal: ',', useGrouping: true});
    expect(getLocaleFormatFromString('3.322,1')).toEqual(
      {thousands: '.', decimal: ',', useGrouping: true});
    expect(getLocaleFormatFromString('-.3221')).toEqual(
      {thousands: ',', decimal: '.', useGrouping: true});
    expect(getLocaleFormatFromString('-1244,3221')).toEqual(
      {thousands: ',', decimal: ',', useGrouping: true});
    expect(getLocaleFormatFromString('-24 4343.3221')).toEqual(
      {thousands: ' ', decimal: '.', useGrouping: true});
  });
});
