import type { LocaleFormat } from '../interfaces.ts';
/**
    * Find out if the given locale is supported.
    * @param countryCode
    * @return {boolean}
  */
export function isLocaleSupported(countryCode:string): boolean {
  const locales = [countryCode];
  const options = { localeMatcher: 'lookup' };
  if (Intl.NumberFormat.supportedLocalesOf(locales, options as any).length > 0) {
    return true;
  }
  return false;
}

/**
    * Get the number format for the given country.
    * @param countryCode
    * @return {object}
  */
export function getLocaleFormat(countryCode: string): LocaleFormat {
  // Table heavely modified from https://stackoverflow.com/questions/5314237/is-there-a-functionality-in-javascript-to-convert-values-into-specific-locale-fo
  const formatLUT: Record<string, any> = {
    bg: { decimal: ',', thousands: ' ' },      //Bulgarian
    ca: { decimal: ',', thousands: '.' },      //Catalan
    cs: { decimal: ',', thousands: ' ' },      //Czech
    da: { decimal: ',', thousands: '.' },      //Danish
    de: { decimal: ',', thousands: '.' },      //German - Germany
    el: { decimal: ',', thousands: '.' },      //Greek
    en: { decimal: '.', thousands: ',' },      //English - United States
    es: { decimal: ',', thousands: '.' },      //Spanish - International
    fi: { decimal: ',', thousands: ' ' },      //Finnish
    fr: { decimal: ',', thousands: ' ' },      //French - France
    ssr: { decimal: '.', thousands: '\'' },    //French - Swiss
    hu: { decimal: ',', thousands: ' ' },      //Hungarian
    it: { decimal: ',', thousands: '.' },      //Italian - Italy
    slf: { decimal: '.', thousands: '\'' },    //Italian - Swiss
    ja: { decimal: '.', thousands: '\'' },     //Japanese
    nl: { decimal: ',', thousands: '.' },      //Dutch - Netherlands
    no: { decimal: ',', thousands: ' ' },      //Norwegian
    nb: { decimal: ',', thousands: ' ' },      //Norwegian Bokmål
    nn: { decimal: ',', thousands: ' ' },      //Norwegian Nynorsk
    pl: { decimal: ',', thousands: ' ' },      //Polish
    pt: { decimal: ',', thousands: '.' },      //Portuguese - Standard
    ro: { decimal: ',', thousands: '.' },      //Romanian
    ru: { decimal: ',', thousands: ' ' },      //Russian
    hr: { decimal: ',', thousands: '.' },      //Croatian
    sr: { decimal: ',', thousands: '.' },      //Serbian - Latin
    sv: { decimal: ',', thousands: '.' },      //Swedish
    tr: { decimal: ',', thousands: '.' },      //Turkish
    id: { decimal: '.', thousands: ',' },      //Indonesian
    uk: { decimal: ',', thousands: ' ' },      //Ukranian
    be: { decimal: ',', thousands: ' ' },      //Belausian
    sl: { decimal: ',', thousands: '.' },      //Slovenian
    et: { decimal: '.', thousands: ' ' },      //Estonian
    lv: { decimal: ',', thousands: ' ' },      //Latvian
    lt: { decimal: ',', thousands: '.' },      //Lithuanian
    af: { decimal: '.', thousands: ',' },      //Afrikaans
    fo: { decimal: ',', thousands: '.' }       //Faeroese
  };
    // See https://en.wikipedia.org/wiki/Decimal_mark for definition
  const defaultFormat = {
    thousands: ',',   // thousands separator
    decimal: '.',     // decimal mark
    useGrouping: true // group numbers?
  };
    // Strip country sub-codes
  const pos = countryCode.search('-');
  if (pos > 0) {
    countryCode = countryCode.substr(0, pos);
  }
  const format = formatLUT[countryCode];
  if (format) {
    try {
      const numberFormat = new Intl.NumberFormat(countryCode);
      const usedOptions = numberFormat.resolvedOptions();
      format.useGrouping = usedOptions.useGrouping;
    } catch (error) {
      format.useGrouping = true;
    }
    return format;
  }
  return defaultFormat;
}

/**
    * Given a string representing a number in a given locale, try to guess the format.
    * @param number
    * @return {object}
  */
export function getLocaleFormatFromString(number: string): LocaleFormat {
  const format = {
    thousands: ',',   // thousands separator
    decimal: '.',     // decimal mark
    useGrouping: true // 50-50 chance of beeing correct
  };
  number.trim();
  let thousandsPos = number.indexOf(' ');
  if (thousandsPos > 0) {
    format.thousands = ' ';
  }
  const decimalPos = number.indexOf('.');
  thousandsPos = number.indexOf(',');
  if (decimalPos < thousandsPos) {
    format.decimal = ',';
    if (decimalPos > 0) { format.thousands = '.'; }
  }
  return format;
}

/**
    * Convert a string on the given number format to a JavaScript floating number.
    * Based on https://github.com/AndreasPizsa/parse-decimal-number/blob/master/dist/parse-decimal-number.js
    * @param number
    * @return {number}
  */
export function convertStringToNumber(value: string | number, { thousands = ',', decimal = '.', useGrouping = true }: LocaleFormat): number | string {
  if (typeof (value) === 'number') { return value; }
  const groupMinSize = useGrouping ? 3 : 1;
  const pattern =  new RegExp('^\\s*([+-]?(?:(?:\\d{1,3}(?:\\' + thousands + '\\d{' + groupMinSize + ',3})+)|\\d*))(?:\\' + decimal + '(\\d*))?\\s*$');

  const result = value.match(pattern);
  if (!((result != null) && result.length === 3)) {
    return value;
  }
  const integerPart = (result?.[1] || '').replace(new RegExp('\\' + thousands, 'g'), '');
  const fractionPart = result[2];
  const number = parseFloat(integerPart + '.' + fractionPart);
  return number;
}