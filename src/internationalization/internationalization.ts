import type { LocaleFormat } from '../interfaces.ts';
import {getLocaleFormat, getLocaleFormatFromString, convertStringToNumber } from './utils.ts';
// All countryCodes are given with BCP 47 language tag using primary names from ISO 639 (http://www.iana.org/assignments/language-subtag-registry/language-subtag-registry)
// See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl#Locale_identification_and_negotiation

const options = {maximumFractionDigits: 20};

// --- Convert to locale number ---

/**
    * Convert the given number to the browser's default or the given format.
    * Returns the converted number
  */
export function convertNumberToLocale(number: number, countryCode?: string): string {
  let numberFormat;
  try {
    if (countryCode) {
      numberFormat = new Intl.NumberFormat(countryCode, options);
    } else {
      numberFormat = new Intl.NumberFormat(undefined, options);
    }
  } catch {
    numberFormat = new Intl.NumberFormat(undefined, options);
  }

  return numberFormat.format(number);
}

/**
    * Convert numbers in the given array to the browser's default format.
    *  Returns converted array.
  */
export function convertArrayToLocale(numbers:Array<number>, countryCode?: string): Array<string> {
  let numberFormat;
  try {
    if (countryCode) {
      numberFormat = new Intl.NumberFormat(countryCode, options);
    } else {
      numberFormat = new Intl.NumberFormat(undefined, options);
    }
  } catch {
    numberFormat = new Intl.NumberFormat(undefined, options);
  }

  return numbers.map(numberFormat.format);
}

// --- Convert to (Javascript) machine number ---

/**
    * Convert the number(string) to machine number.
    * Returns the converted number.
  */
export function convertNumberFromLocale(number: string, countryCode?: string): number|string {
  let localeFormat: LocaleFormat;
  if (countryCode) {
    localeFormat = getLocaleFormat(countryCode);
  } else {
    localeFormat = getLocaleFormatFromString(number);
  }
  return convertStringToNumber(number, localeFormat);
}

/**
    * Convert the array of numbers(string) to an array of (JavaScript) numbers.
    * Returns the converted array.
    * @param numbers
    * @param countryCode
    * @return {Number[]}
  */
export function convertArrayFromLocale(numbers: Array<string>, countryCode?: string): Array<number|string> {
  let localeFormat: LocaleFormat;
  if (countryCode) {
    localeFormat = getLocaleFormat(countryCode);
  } else {
    let index = numbers?.findIndex(n => n.includes(',') || n.includes('.'));
    if (index < 0) { index = 0; }
    localeFormat = getLocaleFormatFromString(numbers[index]);
  }
  return numbers.map(n => convertStringToNumber(n, localeFormat));
}