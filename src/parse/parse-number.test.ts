import {
  parseNumber,
  unParseNumber,
  countTrailingZeros,
  hasTrailingZeros,
} from './parse-number.ts';

describe('Parse', () => {
  describe('parse', () => {
    test('parses a generic numeric input into number, unit and type properties', () => {
      expect(parseNumber(1.2)).toStrictEqual({number: 1.2, unit: null, isString: false});
      expect(parseNumber('1.2')).toStrictEqual({number: 1.2, unit: null, isString: true});
      expect(parseNumber('1,2')).toStrictEqual({number: 1.2, unit: null, isString: true});
      expect(parseNumber(',2')).toStrictEqual({number: 0.2, unit: null, isString: true});
      expect(parseNumber('1.2|m')).toStrictEqual({number: 1.2, unit: 'm', isString: true});
      expect(parseNumber('1e3|m')).toStrictEqual({number: 1000, unit: 'm', isString: true});
      expect(parseNumber('1/2|m')).toStrictEqual({number: 0.5, unit: 'm', isString: true});
      expect(parseNumber('1/2|kg/m3')).toStrictEqual({number: 0.5, unit: 'kg/m3', isString: true});
      //Watch out! Units not defined in ALT_UNITS will *not* round (due to how isValueWithUnit() works)
      expect(parseNumber('1/2|3badUnit/m3')).toStrictEqual({number: '1/2|3badUnit/m3', unit: null, isString: true});
      expect(parseNumber('foobar')).toStrictEqual({number: 'foobar', unit: null, isString: true});
    });
    test('handles small decimal values OW-17645', () => {
      expect(parseNumber(0.0000002)).toStrictEqual({number: 0.0000002, unit: null, isString: false});
      expect(parseNumber('0.0000002')).toStrictEqual({number: 0.0000002, unit: null, isString: true});
      expect(parseNumber('0.0000002|m')).toStrictEqual({number: 0.0000002, unit: 'm', isString: true});
    });
    test('it preserves trailing decimal zeros for string inputs (optional flag)', () => {
      expect(parseNumber('1234.1234000', true)).toStrictEqual({number: '1234.1234000', unit: null, isString: true});
      expect(parseNumber('1234.1234000|m', true)).toStrictEqual({number: '1234.1234000', unit: 'm', isString: true});
      expect(parseNumber('1.2300e-2', true)).toStrictEqual({number: '1.2300e-2', unit: null, isString: true});
      expect(parseNumber('1.2300e-2|m', true)).toStrictEqual({number: '1.2300e-2', unit: 'm', isString: true});
    });
  });
  describe('unParse', () => {
    test('converts value, unit and type back to a generic numeric value', () => {
      expect(unParseNumber({value: 1.2})).toBe(1.2);
      expect(unParseNumber({value: 1.2, isString: true})).toBe('1.2');
      expect(unParseNumber({value: 1.2, unit: 'm', isString: true})).toBe('1.2|m');
      expect(unParseNumber({value: '1/2|3badUnit/m3', unit: null, isString: true})).toBe('1/2|3badUnit/m3');
    });
    test('handles small decimal values OW-17645', () => {
      expect(unParseNumber({value: 0.0000002})).toBe(0.0000002);
      expect(unParseNumber({value: 0.0000002, isString: true})).toBe('0.0000002');
      expect(unParseNumber({value: 0.0000002, unit: 'm', isString: true})).toBe('0.0000002|m');
      expect(unParseNumber({value: '0.0000002'})).toBe('0.0000002');
      expect(unParseNumber({value: '0.0000002', isString: true})).toBe('0.0000002');
      expect(unParseNumber({value: '0.0000002', unit: 'm', isString: true})).toBe('0.0000002|m');
    });
  });
  describe('countTrailingZeros', () => {
    test('counts trailing zeros', () => {
      expect(countTrailingZeros('123.12300')).toBe(2);
      expect(countTrailingZeros('10203.12300')).toBe(2);
      expect(countTrailingZeros('123000')).toBe(3);
      expect(countTrailingZeros('123000.0')).toBe(4);
      expect(countTrailingZeros('123000', true)).toBe(0);
      expect(countTrailingZeros('123000.0', true)).toBe(1);
      expect(countTrailingZeros('123.1230000')).toBe(4);
      expect(countTrailingZeros('123.123000|m')).toBe(3);
      expect(countTrailingZeros('10203.123000|m')).toBe(3);
      expect(countTrailingZeros('123.123000e3')).toBe(3);
      expect(countTrailingZeros('123.123000e+4')).toBe(3);
      expect(countTrailingZeros('123,123000e+4')).toBe(3);
      expect(countTrailingZeros('10203.10023000e-4')).toBe(3);
      expect(countTrailingZeros('123.123000e-40')).toBe(3);
      expect(countTrailingZeros('123000.00')).toBe(5);
      expect(countTrailingZeros('123010.00')).toBe(3);
      expect(countTrailingZeros('0.0|ft')).toBe(2);
      expect(countTrailingZeros('123000000')).toBe(6);
    });
    test('has trailing decimal zeros', () => {
      expect(hasTrailingZeros('123.123')).toBe(false);
      expect(hasTrailingZeros('1234')).toBe(false);
      expect(hasTrailingZeros('123000')).toBe(true);
      expect(hasTrailingZeros('123.12300')).toBe(true);
      expect(hasTrailingZeros('123.1230000')).toBe(true);
      expect(hasTrailingZeros('123.123000|m')).toBe(true);
      expect(hasTrailingZeros('123.123000e3')).toBe(true);
      expect(hasTrailingZeros('123.123000e+4')).toBe(true);
      expect(hasTrailingZeros('123,123000e+4')).toBe(true);
      expect(hasTrailingZeros('10203.10023000e-4')).toBe(true);
      expect(hasTrailingZeros('123.123000e-40')).toBe(true);
      expect(hasTrailingZeros('123000.00')).toBe(true);
      expect(hasTrailingZeros('123010.00')).toBe(true);
      expect(hasTrailingZeros('0.0|ft')).toBe(true);
      expect(hasTrailingZeros('123000000')).toBe(true);
    });
  });
});
