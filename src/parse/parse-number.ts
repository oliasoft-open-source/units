import { getUnit, getValue, isValueWithUnit, withUnit } from '../units.ts';
import { toNum, toString } from '../numbers/numbers.ts';
import { cleanNumStr } from '../utils.ts';

export const countTrailingZeros = (value: number|string, decimalPartOnly = false) => {
  /*
    See unit test and
      - https://regex101.com/r/89tGjt/1
      - https://regex101.com/r/55lEWZ/1
      Matches trailing zeros followed by |, e, E or end of string
   */
  const condition = decimalPartOnly
    ? /0+((?=[|eE])|$)/
    : /(0+|0+\.0+)((?=[|eE])|$)/;
  if (typeof value === 'string' && (decimalPartOnly ? value.includes('.') || value.includes(',') : true)) {
    return value?.match(condition)?.[0]
      ?.replaceAll(/[.,]/g, '')
      ?.length ?? 0;
  }
  return 0;
};

export const hasTrailingZeros = (value: number|string) => {
  return countTrailingZeros(value) > 0;
};

/**
 * Internal function to parse the value, unit, and type from a generic numeric input
 *
 * Accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 *
 * @param value
 * @returns object with number, unit, and type
 */
export const parseNumber = (value: number|string, preserveTrailingZeros = false)
  : {number:any; unit:string|null; isString: boolean} => {
  const isString = typeof value === 'string';
  const hasUnit = isString && isValueWithUnit(value);
  const unit = hasUnit ? getUnit(value) : null;
  const cleaned = cleanNumStr(hasUnit ? getValue(value) : value);
  const number = preserveTrailingZeros && hasTrailingZeros(value) ? cleaned : toNum(cleaned);
  return {
    number,
    unit,
    isString,
  };
};

/**
 * Convert a number to a string safely, better than String(value)
 *  String(0.0000002) returns '2e-7' which is unwanted if we need to preserve formatting
 *
 * @param value
 * @param [isScientific] whether to preserve scientific notation
 * @returns number or string output value
 */
const safeStringifyNumber = (value: number|string, isScientific?: boolean): string => {
  /*
    Note that String(0.0000002) -> '2e-7' in JavaScript, so we need to use a safe conversion function instead
    We could improve how the scientific notation inputs are handed too, but that's a minor breaking change to the
    current unit tests, so let's consider it separately in OW-17645
  */
  return isScientific
    ? String(value) // up for debate if we should use formatScientificDisplayNumber here ...
    : String(toString(value));
};

/**
 * Internal function to unParse a value, unit, and type back to an output value
 *
 * Accepts number types (1.234), stringified numbers ('1.234'), and unit numbers ('1.234|m')
 *
 * @param args
 * @param args.value
 * @param args.unit
 * @param args.isString
 * @param args.isScientific
 * @returns number or string output value
 */
export const unParseNumber = ({
  value,
  unit,
  isString,
  isScientific
} : {
  value: number|string,
  unit?: string|null,
  isString?: boolean,
  isScientific?: boolean,
}): number|string => {
  const convertedValue = (typeof value === 'number' && isString)
    ? safeStringifyNumber(value, isScientific) : value;
  if (unit) {
    return withUnit(convertedValue, unit);
  }
  return convertedValue;
};
