import eslintPlugin from '@typescript-eslint/eslint-plugin';
import oliasoftConfig from '@oliasoft-open-source/eslint-config-oliasoft';

const cleanedOliasoftConfig = {
  ...oliasoftConfig,
  languageOptions: {
    ...oliasoftConfig.languageOptions,
    globals: {
      ...(oliasoftConfig.languageOptions?.globals || {}),
      console: "readonly",
      document: "readonly",
      process: "readonly",
      window: "readonly",
      module: "readonly",
      __dirname: "readonly",
      setTimeout: "readonly",
      setInterval: "readonly"
    }
  }
};

export default [
  cleanedOliasoftConfig,
  eslintPlugin.configs.recommended,
  {
    plugins: {
      '@typescript-eslint': eslintPlugin
    },
    languageOptions: {
      parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module'
      }
    },
    settings: {
      'import/resolver': {
        typescript: {},
        node: {
          extensions: ['.js', '.ts']
        }
      }
    },
    rules: {
      '@typescript-eslint/no-non-null-assertion': 'off',
      'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
      'import/no-unresolved': 'off',
      'semi': 'off',
      '@typescript-eslint/semi': ['error'],
      'quotes': ['error', 'single', { allowTemplateLiterals: true }]
    },
    ignores: ['src/validate/ajv-validators.ts']
  }
];
